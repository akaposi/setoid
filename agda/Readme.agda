{-# OPTIONS --without-K --prop #-}

module Readme where

-- Formalisation accompanying the paper "Setoid type theory --- a
-- syntactic translation". Tested with Agda 2.6.0.

-- TODO: universe levels in Π

import lib             -- standard library

----------------------------------------------------------------------
-- a setoid model supporting the coeR rule
----------------------------------------------------------------------

import ModelR.Decl     -- declaration
import ModelR.Core     -- substitution calculus
import ModelR.Iden     -- Martin-Löf's identity type
import ModelR.Func     -- function space
import ModelR.Sigma    -- Sigma types
import ModelR.Bool     -- booleans
import ModelR.Props    -- universe of propositions
import ModelR.SetoidTT -- the extra rules of setoid type theory

----------------------------------------------------------------------
-- a setoid model supporting extra things
----------------------------------------------------------------------

-- a difference from the paper is that contexts are not indexed by the
-- universe level but are in the highest universe

import Model.Decl      -- declaration
import Model.Core      -- substitution calculus
import Model.Props0    -- universe of propositions
import Model.Iden      -- Martin-Löf's identity type
-- import Model.ComputId
import Model.Func      -- function space
import Model.Sets0     -- universe of small sets
import Model.Sets1     -- universe of large sets
import Model.Nat       -- natural numbers
import Model.Stream    -- streams
import Model.Trunc     -- propostional truncation
import Model.Quotient  -- quotient types
-- import Model.List   -- lists (unfinished)

----------------------------------------------------------------------
-- a shallow embedding of setoid type theory equality in Agda
----------------------------------------------------------------------

import Shallow
