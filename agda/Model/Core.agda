{-# OPTIONS --without-K --prop #-}

module Model.Core where

open import lib
open import Agda.Primitive

open import Model.Decl

infixl 5 _,_
infixl 7 _[_]T
infixl 5 _,s_
infix  6 _∘_
infixl 8 _[_]t

-- definitions

• : Con
• = record
  { ∣_∣C   = Lift ⊤
  ; _C_~_  = λ _ _ → LiftP ⊤p
  }

_,_ : (Γ : Con){l : level} → Ty Γ l → Con
_,_ Γ {l0} A = record
  { ∣_∣C   = Σ ∣ Γ ∣C ∣ A ∣T_
  ; _C_~_  = λ { (γ ,Σ α)(γ' ,Σ α') → Σp (Γ C γ ~ γ') (A T_⊢ α ~ α') }
  ; refC   = λ { (γ ,Σ α) → refC Γ γ ,p refT A α }
  ; symC   = λ { (p ,p r) → symC Γ p ,p symT A r }
  ; transC = λ { (p ,p r)(p' ,p r') → transC Γ p p' ,p transT A r r' }
  }
_,_ Γ {l1} A = record
  { ∣_∣C   = Σ ∣ Γ ∣C ∣ A ∣T_
  ; _C_~_  = λ { (γ ,Σ α)(γ' ,Σ α') → Σp (Γ C γ ~ γ') (A T_⊢ α ~ α') }
  ; refC   = λ { (γ ,Σ α) → refC Γ γ ,p refT A α }
  ; symC   = λ { (p ,p r) → symC Γ p ,p symT A r }
  ; transC = λ { (p ,p r)(p' ,p r') → transC Γ p p' ,p transT A r r' }
  }

_[_]T : ∀{Γ Δ l} → Ty Δ l → Tms Γ Δ → Ty Γ l
A [ σ ]T = record
  { ∣_∣T_   = λ γ → ∣ A ∣T (∣ σ ∣s γ)
  ; _T_⊢_~_ = λ p α α' → A T ~s σ p ⊢ α ~ α'
  ; refT    = refT A
  ; symT    = symT A
  ; transT  = transT A
  ; coeT    = λ p → coeT A (~s σ p)
  ; cohT    = λ p → cohT A (~s σ p)
  }

id : ∀{Γ} → Tms Γ Γ
id = record
  { ∣_∣s = λ γ → γ
  ; ~s   = λ p → p
  }

_∘_ : ∀{Γ Θ Δ} → Tms Θ Δ → Tms Γ Θ → Tms Γ Δ
σ ∘ ν = record
  { ∣_∣s = λ γ → ∣ σ ∣s (∣ ν ∣s γ)
  ; ~s   = λ p → ~s σ (~s ν p)
  }

ε : ∀{Γ} → Tms Γ •
ε = record
  { ∣_∣s = λ _ → lift tt
  }

_,s_ : ∀{Γ Δ}(σ : Tms Γ Δ){l}{A : Ty Δ l} → Tm Γ (A [ σ ]T) → Tms Γ (Δ , A)
_,s_ σ {l0}  t = record { ∣_∣s = λ γ → ∣ σ ∣s γ ,Σ ∣ t ∣t γ ; ~s = λ p → ~s σ p ,p ~t t p }
_,s_ σ {l1} t = record { ∣_∣s = λ γ → ∣ σ ∣s γ ,Σ ∣ t ∣t γ ; ~s = λ p → ~s σ p ,p ~t t p }

π₁ : ∀{Γ Δ l}{A : Ty Δ l} → Tms Γ (Δ , A) →  Tms Γ Δ
π₁ {l = l0}  σ = record { ∣_∣s = λ γ → proj₁ (∣ σ ∣s γ) ; ~s = λ p → proj₁p (~s σ p) }
π₁ {l = l1} σ = record { ∣_∣s = λ γ → proj₁ (∣ σ ∣s γ) ; ~s = λ p → proj₁p (~s σ p) }

_[_]t : ∀{Γ Δ l}{A : Ty Δ l} → Tm Δ A → (σ : Tms Γ Δ) → Tm Γ (A [ σ ]T)
t [ σ ]t = record { ∣_∣t = λ γ → ∣ t ∣t (∣ σ ∣s γ) ; ~t = λ p → ~t t (~s σ p) }

π₂ : ∀{Γ Δ l}{A : Ty Δ l}(σ : Tms Γ (Δ , A)) → Tm Γ (A [ π₁ {A = A} σ ]T)
π₂ {l = l0}  σ = record { ∣_∣t = λ γ → proj₂ (∣ σ ∣s γ) ; ~t = λ p → proj₂p (~s σ p) }
π₂ {l = l1} σ = record { ∣_∣t = λ γ → proj₂ (∣ σ ∣s γ) ; ~t = λ p → proj₂p (~s σ p) }

[id]T : ∀{Γ l}{A : Ty Γ l}  → A [ id ]T ≡ A
[][]T : ∀{Γ Δ Σ l}{A : Ty Σ l}{σ : Tms Γ Δ}{δ : Tms Δ Σ} → (A [ δ ]T [ σ ]T) ≡ (A [ δ ∘ σ ]T)
idl   : ∀{Γ Δ}{δ : Tms Γ Δ} → (id ∘ δ) ≡ δ
idr   : ∀{Γ Δ}{δ : Tms Γ Δ} → (δ ∘ id) ≡ δ
ass   : ∀{Γ Δ Σ Ω}{σ : Tms Σ Ω}{δ : Tms Δ Σ}{ν : Tms Γ Δ} → ((σ ∘ δ) ∘ ν) ≡ (σ ∘ (δ ∘ ν))
,∘0   : ∀{Γ Δ Σ}{δ : Tms Γ Δ}{σ : Tms Σ Γ}{A : Ty Δ l0} {a : Tm Γ (A [ δ ]T)} → ((_,s_ δ {A = A} a) ∘ σ) ≡ (_,s_ (δ ∘ σ) {A = A} (a [ σ ]t))
,∘1   : ∀{Γ Δ Σ}{δ : Tms Γ Δ}{σ : Tms Σ Γ}{A : Ty Δ l1}{a : Tm Γ (A [ δ ]T)} → ((_,s_ δ {A = A} a) ∘ σ) ≡ (_,s_ (δ ∘ σ) {A = A} (a [ σ ]t))
π₁β0  : ∀{Γ Δ}{A : Ty Δ l0} {δ : Tms Γ Δ}{a : Tm Γ (A [ δ ]T)} → (π₁ {A = A}(_,s_ δ {A = A} a)) ≡ δ
π₁β1  : ∀{Γ Δ}{A : Ty Δ l1}{δ : Tms Γ Δ}{a : Tm Γ (A [ δ ]T)} → (π₁ {A = A}(_,s_ δ {A = A} a)) ≡ δ
πη0   : ∀{Γ Δ}{A : Ty Δ l0} {δ : Tms Γ (Δ , A)} → (_,s_ (π₁ {A = A} δ) {A = A} (π₂ {A = A} δ)) ≡ δ
πη1   : ∀{Γ Δ}{A : Ty Δ l1}{δ : Tms Γ (Δ , A)} → (_,s_ (π₁ {A = A} δ) {A = A} (π₂ {A = A} δ)) ≡ δ
εη    : ∀{Γ}{σ : Tms Γ •} → σ ≡ ε
π₂β0  : ∀{Γ Δ}{A : Ty Δ l0} {δ : Tms Γ Δ}{a : Tm Γ (A [ δ ]T)} → π₂ {A = A}(_,s_ δ {A = A} a) ≡ a
π₂β1  : ∀{Γ Δ}{A : Ty Δ l1}{δ : Tms Γ Δ}{a : Tm Γ (A [ δ ]T)} → π₂ {A = A}(_,s_ δ {A = A} a) ≡ a

[id]T = refl
[][]T = refl
idl   = refl
idr   = refl
ass   = refl
,∘0   = refl
,∘1   = refl
π₁β0  = refl
π₁β1  = refl
πη0   = refl
πη1   = refl
εη    = refl
π₂β0  = refl
π₂β1  = refl

wk : ∀{Γ l}{A : Ty Γ l} → Tms (Γ , A) Γ
wk {A = A} = π₁ {A = A} id

vz : ∀{Γ l}{A : Ty Γ l} → Tm (Γ , A) (A [ wk {A = A} ]T)
vz {A = A} = π₂ {A = A} id

vs : ∀{Γ l c}{A : Ty Γ l}{B : Ty Γ c} → Tm Γ A → Tm (Γ , B) (A [ wk {A = B} ]T) 
vs {B = B} x = x [ wk {A = B} ]t

<_> : ∀{Γ l}{A : Ty Γ l} → Tm Γ A → Tms Γ (Γ , A)
<_> {A = A} = _,s_ id {A = A}

infix 4 <_>

_^_ : {Γ Δ : Con}(σ : Tms Γ Δ){l : level}(A : Ty Δ l) → Tms (Γ , A [ σ ]T) (Δ , A)
σ ^ A = _,s_ (σ ∘ wk {A = A [ σ ]T}) {A = A} (vz {A = A [ σ ]T})

infixl 5 _^_

wk' : ∀{Γ l}(A : Ty Γ l) → Tms (Γ , A) Γ
wk' A = wk {A = A}

wkT : ∀{Γ l l'}(A : Ty Γ l) → Ty Γ l' → Ty (Γ , A) l'
wkT A B = B [ wk' A ]T

vz' : ∀{Γ l}(A : Ty Γ l) → Tm (Γ , A) (wkT A A)
vz' A = vz {A = A}

vs' : ∀{Γ l c}(A : Ty Γ l)(B : Ty Γ c) → Tm Γ A → Tm (Γ , B) (wkT B A)
vs' A B x = vs {A = A}{B} x

<_∣_> : ∀{Γ l}(A : Ty Γ l) → Tm Γ A → Tms Γ (Γ , A)
<_∣_> A t = <_> {A = A} t
