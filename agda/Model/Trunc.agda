{-# OPTIONS --without-K --prop #-}

module Model.Trunc where

open import lib
open import Agda.Primitive

open import Model.Decl
open import Model.Core

Tr : {Γ : Con}{b : level} → Ty Γ b → Ty Γ b
Tr {Γ}{b} A = record
  { ∣_∣T_ = ∣ A ∣T_
  ; _T_⊢_~_ = λ _ _ _ → LiftP ⊤p
  ; coeT = coeT A
  }

embed : {Γ : Con}{b : level}{A : Ty Γ b} → Tm Γ A → Tm Γ (Tr A)
embed t = record { ∣_∣t = ∣ t ∣t }

open import Model.Iden

treq : {Γ : Con}{b : level}{A : Ty Γ b}(t u : Tm Γ (Tr A)) → Tm Γ (Id t u)
treq t u = record {}

recTrt0 : {Γ : Con}{A : Ty Γ l0}(C : Ty Γ l0)
  (membed : Tm (Γ , A) (C [ wk {A = A} ]T))
  (mtreq  : Tm (Γ , C , C [ wk {A = C} ]T)
               (Id {A = C [ wk {A = C} ]T [ wk {A = C [ wk {A = C} ]T} ]T} (vz {A = C} [ wk {Γ , C}{A = C [ wk {A = C} ]T} ]t) (vz {A = C [ wk {A = C} ]T})))
  (u : Tm Γ (Tr A)) →
  Tm Γ C
recTrt0 {Γ} = λ C membed mtreq u → record
  { ∣_∣t = λ γ → ∣ membed ∣t (γ ,Σ ∣ u ∣t γ)
  ; ~t = λ {γ}{γ'} γ~ → transT C (unliftp (∣ mtreq ∣t (γ ,Σ ∣ membed ∣t (γ ,Σ ∣ u ∣t γ) ,Σ coeT C (symC Γ γ~) (∣ membed ∣t (γ' ,Σ ∣ u ∣t γ')))))
                                 (symT C (cohT C (symC Γ γ~) (∣ membed ∣t (γ' ,Σ ∣ u ∣t γ'))))
  }

recTrt1 : {Γ : Con}{A : Ty Γ l0} (C : Ty Γ l1)(membed : Tm (Γ , A) (C [ wk {A = A} ]T))(mtreq : Tm (Γ , C , C [ wk {A = C} ]T) (Id {A = C [ wk {A = C} ]T [ wk {A = C [ wk {A = C} ]T} ]T} (vz {A = C} [ wk {Γ , C}{A = C [ wk {A = C} ]T} ]t) (vz {A = C [ wk {A = C} ]T}))) (u : Tm Γ (Tr A)) → Tm Γ C
recTrf0 : {Γ : Con}{A : Ty Γ l1}(C : Ty Γ l0) (membed : Tm (Γ , A) (C [ wk {A = A} ]T))(mtreq : Tm (Γ , C , C [ wk {A = C} ]T) (Id {A = C [ wk {A = C} ]T [ wk {A = C [ wk {A = C} ]T} ]T} (vz {A = C} [ wk {Γ , C}{A = C [ wk {A = C} ]T} ]t) (vz {A = C [ wk {A = C} ]T}))) (u : Tm Γ (Tr A)) → Tm Γ C
recTrf1 : {Γ : Con}{A : Ty Γ l1}(C : Ty Γ l1)(membed : Tm (Γ , A) (C [ wk {A = A} ]T))(mtreq : Tm (Γ , C , C [ wk {A = C} ]T) (Id {A = C [ wk {A = C} ]T [ wk {A = C [ wk {A = C} ]T} ]T} (vz {A = C} [ wk {Γ , C}{A = C [ wk {A = C} ]T} ]t) (vz {A = C [ wk {A = C} ]T}))) (u : Tm Γ (Tr A)) → Tm Γ C
recTrt1 {Γ}{A} = λ C membed mtreq u → record { ∣_∣t = λ γ → ∣ membed ∣t (γ ,Σ ∣ u ∣t γ) ; ~t = λ {γ}{γ'} γ~ → transT C (unliftp (∣ mtreq ∣t (γ ,Σ ∣ membed ∣t (γ ,Σ ∣ u ∣t γ) ,Σ coeT C (symC Γ γ~) (∣ membed ∣t (γ' ,Σ ∣ u ∣t γ'))))) (symT C (cohT C (symC Γ γ~) (∣ membed ∣t (γ' ,Σ ∣ u ∣t γ')))) }
recTrf0 {Γ}{A} = λ C membed mtreq u → record { ∣_∣t = λ γ → ∣ membed ∣t (γ ,Σ ∣ u ∣t γ) ; ~t = λ {γ}{γ'} γ~ → transT C (unliftp (∣ mtreq ∣t (γ ,Σ ∣ membed ∣t (γ ,Σ ∣ u ∣t γ) ,Σ coeT C (symC Γ γ~) (∣ membed ∣t (γ' ,Σ ∣ u ∣t γ'))))) (symT C (cohT C (symC Γ γ~) (∣ membed ∣t (γ' ,Σ ∣ u ∣t γ')))) }
recTrf1 {Γ}{A} = λ C membed mtreq u → record { ∣_∣t = λ γ → ∣ membed ∣t (γ ,Σ ∣ u ∣t γ) ; ~t = λ {γ}{γ'} γ~ → transT C (unliftp (∣ mtreq ∣t (γ ,Σ ∣ membed ∣t (γ ,Σ ∣ u ∣t γ) ,Σ coeT C (symC Γ γ~) (∣ membed ∣t (γ' ,Σ ∣ u ∣t γ'))))) (symT C (cohT C (symC Γ γ~) (∣ membed ∣t (γ' ,Σ ∣ u ∣t γ')))) }

recTrt0β : {Γ : Con}{A : Ty Γ l0}{C : Ty Γ l0}
  {membed : Tm (Γ , A) (C [ wk {A = A} ]T)}
  {mtreq  : Tm (Γ , C , C [ wk {A = C} ]T)
               (Id {A = C [ wk {A = C} ]T [ wk {A = C [ wk {A = C} ]T} ]T} (vz {A = C} [ wk {Γ , C}{A = C [ wk {A = C} ]T} ]t) (vz {A = C [ wk {A = C} ]T}))}
  {t : Tm Γ A} →
  recTrt0 {Γ}{A} C membed mtreq (embed t) ≡ membed [ < t > ]t
recTrt0β = refl

recTrt1β : {Γ : Con}{A : Ty Γ l0} {C : Ty Γ l1}{membed : Tm (Γ , A) (C [ wk {A = A} ]T)}{mtreq : Tm (Γ , C , C [ wk {A = C} ]T)(Id {A = C [ wk {A = C} ]T [ wk {A = C [ wk {A = C} ]T} ]T} (vz {A = C} [ wk {Γ , C}{A = C [ wk {A = C} ]T} ]t) (vz {A = C [ wk {A = C} ]T}))} {t : Tm Γ A} → recTrt1 {Γ}{A} C membed mtreq (embed t) ≡ membed [ < t > ]t
recTrf0β : {Γ : Con}{A : Ty Γ l1}{C : Ty Γ l0} {membed : Tm (Γ , A) (C [ wk {A = A} ]T)}{mtreq : Tm (Γ , C , C [ wk {A = C} ]T)(Id {A = C [ wk {A = C} ]T [ wk {A = C [ wk {A = C} ]T} ]T} (vz {A = C} [ wk {Γ , C}{A = C [ wk {A = C} ]T} ]t) (vz {A = C [ wk {A = C} ]T}))} {t : Tm Γ A} → recTrf0 {Γ}{A} C membed mtreq (embed t) ≡ membed [ < t > ]t
recTrf1β : {Γ : Con}{A : Ty Γ l1}{C : Ty Γ l1}{membed : Tm (Γ , A) (C [ wk {A = A} ]T)}{mtreq : Tm (Γ , C , C [ wk {A = C} ]T)(Id {A = C [ wk {A = C} ]T [ wk {A = C [ wk {A = C} ]T} ]T} (vz {A = C} [ wk {Γ , C}{A = C [ wk {A = C} ]T} ]t) (vz {A = C [ wk {A = C} ]T}))} {t : Tm Γ A} → recTrf1 {Γ}{A} C membed mtreq (embed t) ≡ membed [ < t > ]t
recTrt1β = refl
recTrf0β = refl
recTrf1β = refl

-- substitution laws

Tr[] : {Γ : Con}{b : level}{A : Ty Γ b}{Θ : Con}{σ : Tms Θ Γ} → Tr A [ σ ]T ≡ Tr (A [ σ ]T)
Tr[] = refl

embed[] : {Γ : Con}{b : level}{A : Ty Γ b}{t : Tm Γ A}{Θ : Con}{σ : Tms Θ Γ} →
  embed t [ σ ]t ≡ embed (t [ σ ]t)
embed[] = refl

treq[] : {Γ : Con}{b : level}{A : Ty Γ b}{t u : Tm Γ (Tr A)}{Θ : Con}{σ : Tms Θ Γ} →
  treq {A = A} t u [ σ ]t ≡ treq {A = A [ σ ]T} (t [ σ ]t) (u [ σ ]t)
treq[] = refl

recTrt0[] : {Γ : Con}{A : Ty Γ l0}{C : Ty Γ l0}{membed : Tm (Γ , A) (C [ wk {A = A} ]T)}{mtreq : Tm (Γ , C , C [ wk {A = C} ]T) (Id {A = C [ wk {A = C} ]T [ wk {A = C [ wk {A = C} ]T} ]T} (vz {A = C} [ wk {Γ , C}{A = C [ wk {A = C} ]T} ]t) (vz {A = C [ wk {A = C} ]T}))}{u : Tm Γ (Tr A)}{Θ : Con}{σ : Tms Θ Γ} →
  recTrt0 {Γ}{A} C membed mtreq u [ σ ]t ≡ recTrt0 {Θ}{A [ σ ]T}(C [ σ ]T) (membed [ σ ^ A ]t) (mtreq [ σ ^ C ^ C [ wk {A = C} ]T ]t) (u [ σ ]t)
recTrt0[] = refl
