{-# OPTIONS --without-K --prop #-}

module Model.Sets1 where

open import lib
open import Agda.Primitive

open import Model.Decl
open import Model.Core
open import Model.Props0
open import Model.Sets0

-- an inductive-recursive universe of sets

data U₁ : Set₁
_~U₁_ : U₁ → U₁ → Prop₁
refU₁ : (a : U₁) → a ~U₁ a
symU₁ : {a a' : U₁}(a~ : a ~U₁ a') → a' ~U₁ a
transU₁ : {a a' a'' : U₁}(a~ : a ~U₁ a')(a~' : a' ~U₁ a'') → a ~U₁ a''

El₁ : U₁ → Set₁
_⊢_~El₁_ : {a a' : U₁}(a~ : a ~U₁ a') → El₁ a → El₁ a' → Prop₁
-- refEl₁ defined separately below
symEl₁ : {a a' : U₁}{a~ : a ~U₁ a'}{x : El₁ a}{x' : El₁ a'}(x~ : a~ ⊢ x ~El₁ x') → symU₁ a~ ⊢ x' ~El₁ x
transEl₁ : {a a' a'' : U₁}{a~ : a ~U₁ a'}{a~' : a' ~U₁ a''}{x : El₁ a}{x' : El₁ a'}{x'' : El₁ a''}
  (x~ : a~ ⊢ x ~El₁ x')(x~' : a~' ⊢ x' ~El₁ x'') → transU₁ a~ a~' ⊢ x ~El₁ x''
coeEl₁ : {a a' : U₁}(a~ : a ~U₁ a') → El₁ a → El₁ a'
cohEl₁ : {a a' : U₁}(a~ : a ~U₁ a')(x : El₁ a) → a~ ⊢ x ~El₁ coeEl₁ a~ x

data U₁ where
  ⊤s : U₁
  ⊥s : U₁
  Σs : (a : U₁)(b : El₁ a → U₁)(refb : {x x' : El₁ a}(x~ : refU₁ a ⊢ x ~El₁ x') → b x ~U₁ b x') → U₁
  Πs : (a : U₁)(b : El₁ a → U₁)(refb : {x x' : El₁ a}(x~ : refU₁ a ⊢ x ~El₁ x') → b x ~U₁ b x') → U₁
  ι : U → U₁
  u : U₁

⊤s ~U₁ ⊤s = ⊤p'
⊥s ~U₁ ⊥s = ⊤p'
Σs a b refb ~U₁ Σs a' b' refb' = Σp (a ~U₁ a') λ a~ → {x : El₁ a}{x' : El₁ a'}(x~ : a~ ⊢ x ~El₁ x') → b x ~U₁ b' x'
Πs a b refb ~U₁ Πs a' b' refb' = Σp (a ~U₁ a') λ a~ → {x : El₁ a}{x' : El₁ a'}(x~ : a~ ⊢ x ~El₁ x') → b x ~U₁ b' x'
ι a ~U₁ ι b = a ~U b
u ~U₁ u = ⊤p'
_ ~U₁ _ = ⊥p'

refU₁ ⊤s = ttp'
refU₁ ⊥s = ttp'
refU₁ (Σs a b refb) = refU₁ a ,p λ x~ → refb x~
refU₁ (Πs a b refb) = refU₁ a ,p λ x~ → refb x~
refU₁ (ι a) = refU a
refU₁ u = ttp'

symU₁ {⊤s} {⊤s} a~ = ttp'
symU₁ {⊤s} {⊥s} ()
symU₁ {⊤s} {Σs a' b' refb'} ()
symU₁ {⊤s} {Πs a' b' refb'} ()
symU₁ {⊥s} {⊤s} ()
symU₁ {⊥s} {⊥s} a~ = ttp'
symU₁ {⊥s} {Σs a' b' refb'} ()
symU₁ {⊥s} {Πs a' b' refb'} ()
symU₁ {Σs a b refb} {⊤s} ()
symU₁ {Σs a b refb} {⊥s} ()
symU₁ {Σs a b refb} {Σs a' b' refb'}(a~ ,p b~) = symU₁ {a} a~ ,p λ x~ → symU₁ {b _} (b~ (symEl₁ x~))
symU₁ {Σs a b refb} {Πs a' b' refb'} ()
symU₁ {Πs a b refb} {⊤s} ()
symU₁ {Πs a b refb} {⊥s} ()
symU₁ {Πs a b refb} {Σs a' b' refb'} ()
symU₁ {Πs a b refb} {Πs a' b' refb'}(a~ ,p b~) = symU₁ {a} a~ ,p λ x~ → symU₁ {b _} (b~ (symEl₁ x~))
symU₁ {ι a} {⊤s} ()
symU₁ {ι a} {⊥s} ()
symU₁ {ι a} {Σs b b₁ refb} ()
symU₁ {ι a} {Πs b b₁ refb} ()
symU₁ {ι a} {ι x₁} p = symU p
symU₁ {u} {⊤s} ()                                  
symU₁ {u} {⊥s} ()                                  
symU₁ {u} {Σs b b₁ refb} ()                        
symU₁ {u} {Πs b b₁ refb} ()                        
symU₁ {u} {ι x₁} ()
symU₁ {u} {u} a~ = ttp'

transU₁ {⊤s} {⊤s} {⊤s} a~ a~' = ttp'
transU₁ {⊤s} {⊤s} {⊥s} a~ ()
transU₁ {⊤s} {⊤s} {Σs a'' b'' refb''} a~ ()
transU₁ {⊤s} {⊤s} {Πs a'' b'' refb''} a~ ()
transU₁ {⊤s} {⊥s} {_} ()
transU₁ {⊤s} {Σs a' b refb'} {_} ()
transU₁ {⊤s} {Πs a' b refb'} {_} ()
transU₁ {⊥s} {⊤s} {_} ()
transU₁ {⊥s} {⊥s} {⊤s} a~ ()
transU₁ {⊥s} {⊥s} {⊥s} a~ a~' = ttp'
transU₁ {⊥s} {⊥s} {Σs a'' b'' refb''} a~ ()
transU₁ {⊥s} {⊥s} {Πs a'' b'' refb''} a~ ()
transU₁ {⊥s} {Σs a' b' refb'} {_} ()
transU₁ {⊥s} {Πs a' b' refb'} {_} ()
transU₁ {Σs a b refb} {⊤s} {_} ()
transU₁ {Σs a b refb} {⊥s} {_} ()
transU₁ {Σs a b refb} {Σs a' b' refb'} {⊤s} a~ ()
transU₁ {Σs a b refb} {Σs a' b' refb'} {⊥s} a~ ()
transU₁ {Σs a b refb} {Σs a' b' refb'} {Σs a'' b'' refb''} (a~ ,p b~) (a~' ,p b~') =
  transU₁ {a} a~ a~' ,p λ {x}{x''} x~ → transU₁ {b _} (b~ (cohEl₁ a~ x)) (transU₁ {b' _} (refb' (transEl₁ (symEl₁ (cohEl₁ a~ x)) (transEl₁ {a} x~ (cohEl₁ (symU₁ {a'} a~') x'')) )) (b~' (symEl₁ (cohEl₁ (symU₁ {a'} a~') x''))))
transU₁ {Σs a b refb} {Σs a' b' refb'} {Πs a'' b'' refb''} a~ ()
transU₁ {Σs a b refb} {Πs a' b' refb'} {_} ()
transU₁ {Πs a b refb} {⊤s} {_} ()
transU₁ {Πs a b refb} {⊥s} {_} ()
transU₁ {Πs a b refb} {Σs a' b' refb'} {_} ()
transU₁ {Πs a b refb} {Πs a' b' refb'} {⊤s} a~ ()
transU₁ {Πs a b refb} {Πs a' b' refb'} {⊥s} a~ ()
transU₁ {Πs a b refb} {Πs a' b' refb'} {Σs a'' b'' refb''} a~ ()
transU₁ {Πs a b refb} {Πs a' b' refb'} {Πs a'' b'' refb''} (a~ ,p b~) (a~' ,p b~') =
  transU₁ {a} a~ a~' ,p λ {x}{x''} x~ → transU₁ {b _} (b~ (cohEl₁ a~ x)) (transU₁ {b' _} (refb' (transEl₁ (symEl₁ (cohEl₁ a~ x)) (transEl₁ {a} x~ (cohEl₁ (symU₁ {a'} a~') x'')) )) (b~' (symEl₁ (cohEl₁ (symU₁ {a'} a~') x''))))
transU₁ {ι a} {⊤s} {c} ()
transU₁ {ι a} {⊥s} {c} ()
transU₁ {ι a} {Σs b b₁ refb} {c} ()
transU₁ {ι a} {Πs b b₁ refb} {c} ()
transU₁ {ι a} {ι b} {⊤s} x ()
transU₁ {ι a} {ι b} {⊥s} x ()
transU₁ {ι a} {ι b} {Σs c b₁ refb} x ()
transU₁ {ι a} {ι b} {Πs c b₁ refb} x ()
transU₁ {ι a} {ι b} {ι c} p q = transU p q
transU₁ {u} {⊤s} ()
transU₁ {u} {⊥s} ()
transU₁ {u} {Σs a'' b refb} ()
transU₁ {u} {Πs a'' b refb} ()
transU₁ {u} {ι x} ()
transU₁ {u} {u} {⊤s} _ ()
transU₁ {u} {u} {⊥s} _ ()
transU₁ {u} {u} {Σs a'' b refb} _ ()
transU₁ {u} {u} {Πs a'' b refb} _ ()
transU₁ {u} {u} {ι x} _ ()
transU₁ {u} {u} {u} _ _ = ttp'


El₁ ⊤s = Lift ⊤
El₁ ⊥s = Lift ⊥
El₁ (Σs a b refb) = Σ (El₁ a) λ x → El₁ (b x)
El₁ (Πs a b refb) = Σsp ((x : El₁ a) → El₁ (b x)) λ f → {x x' : El₁ a}(x~ : refU₁ a ⊢ x ~El₁ x') → refb x~ ⊢ f x ~El₁ f x'
El₁ (ι a) = Lift (El a)
El₁ u = U

_⊢_~El₁_ {⊤s} {⊤s} a~ x x' = ⊤p'
_⊢_~El₁_ {⊤s} {⊥s} () x x'
_⊢_~El₁_ {⊤s} {Σs a' b' refb'} () x x'
_⊢_~El₁_ {⊤s} {Πs a' b' refb'} () x x'
_⊢_~El₁_ {⊤s} {ι a} () x x'
_⊢_~El₁_ {⊥s} {⊤s} () x x'
_⊢_~El₁_ {⊥s} {⊥s} a~ x x' = ⊤p'
_⊢_~El₁_ {⊥s} {Σs a' b' refb'} () x x'
_⊢_~El₁_ {⊥s} {Πs a' b' refb'} () x x'
_⊢_~El₁_ {⊥s} {ι a} () x x'
_⊢_~El₁_ {Σs a b refb} {⊤s} () x x'
_⊢_~El₁_ {Σs a b refb} {⊥s} () x x'
_⊢_~El₁_ {Σs a b refb} {Σs a' b' refb'} w~ (x ,Σ y) (x' ,Σ y') =
  Σp (proj₁p w~ ⊢ x ~El₁ x') λ x~ → proj₂p w~ x~ ⊢ y ~El₁ y'
_⊢_~El₁_ {Σs a b refb} {Πs a' b' refb'} () x x'
_⊢_~El₁_ {Σs a b refb} {ι c} () x x'
_⊢_~El₁_ {Πs a b refb} {⊤s} () x x'
_⊢_~El₁_ {Πs a b refb} {⊥s} () x x'
_⊢_~El₁_ {Πs a b refb} {Σs a' b' refb'} () x x'
_⊢_~El₁_ {Πs a b refb} {Πs a' b' refb'} w~ (f ,sp _) (f' ,sp _) =
  {x : El₁ a}{x' : El₁ a'}(x~ : proj₁p w~ ⊢ x ~El₁ x') → proj₂p w~ x~ ⊢ f x ~El₁ f' x'
_⊢_~El₁_ {Πs a b refb} {ι c} () x x'
_⊢_~El₁_ {ι a} {⊤s} ()
_⊢_~El₁_ {ι a} {⊥s} ()
_⊢_~El₁_ {ι a} {Σs b b₁ refb} ()
_⊢_~El₁_ {ι a} {Πs b b₁ refb} ()
_⊢_~El₁_ {ι a} {ι b} a~ (lift x) (lift y) = LiftP (a~ ⊢ x ~El y)
_⊢_~El₁_ {u} {⊤s} ()                                  
_⊢_~El₁_ {u} {⊥s} ()                                  
_⊢_~El₁_ {u} {Σs b b₁ refb} ()                        
_⊢_~El₁_ {u} {Πs b b₁ refb} ()                        
_⊢_~El₁_ {u} {ι x₁} ()
_⊢_~El₁_ {u} {u} _ x y = x ~U y

symEl₁ {⊤s} {⊤s} {a~} {x} {x'} x~ = ttp'
symEl₁ {⊤s} {⊥s} {()}
symEl₁ {⊤s} {Σs a' b' refb'} {()}
symEl₁ {⊤s} {Πs a' b' refb'} {()}
symEl₁ {⊥s} {⊤s} {()}
symEl₁ {⊥s} {⊥s} {a~} {x} {x'} x~ = ttp'
symEl₁ {⊥s} {Σs a' b' refb'} {()}
symEl₁ {⊥s} {Πs a' b' refb'} {()}
symEl₁ {Σs a b refb} {⊤s} {()}
symEl₁ {Σs a b refb} {⊥s} {()}
symEl₁ {Σs a b refb} {Σs a' b' refb'} {w~} {x ,Σ y} {x' ,Σ y'}(x~ ,p y~) = symEl₁ {a} x~ ,p symEl₁ {b x} y~
symEl₁ {Σs a b refb} {Πs a' b' refb'} {()}
symEl₁ {Πs a b refb} {⊤s} {()}
symEl₁ {Πs a b refb} {⊥s} {()}
symEl₁ {Πs a b refb} {Σs a' b' refb'} {()}
symEl₁ {Πs a b refb} {Πs a' b' refb'} {w~} {f ,sp _} {f' ,sp _} f~ {x}{x'} x~ = symEl₁ {b _} (f~ (symEl₁ {a'} x~))
symEl₁ {ι a} {⊤s} {()}
symEl₁ {ι a} {⊥s} {()}
symEl₁ {ι a} {Σs b b₁ refb} {()}
symEl₁ {ι a} {Πs b b₁ refb} {()}
symEl₁ {ι a} {ι b} (liftP x) = liftP (symEl x)
symEl₁ {u} {⊤s} {()}                                  
symEl₁ {u} {⊥s} {()}                                  
symEl₁ {u} {Σs b b₁ refb} {()}                        
symEl₁ {u} {Πs b b₁ refb} {()}                        
symEl₁ {u} {ι x₁} {()}
symEl₁ {u} {u} a~ = symU a~

transEl₁ {⊤s} {⊤s} {⊤s} {_} {_} {x} {x'} {x''} x~ x~' = ttp'
transEl₁ {⊤s} {⊤s} {⊥s} {_} {()}
transEl₁ {⊤s} {⊤s} {Σs a'' b'' refb''} {_} {()}
transEl₁ {⊤s} {⊤s} {Πs a'' b'' refb''} {_} {()}
transEl₁ {⊤s} {⊥s} {_} {()}
transEl₁ {⊤s} {Σs a' b' refb'} {_} {()}
transEl₁ {⊤s} {Πs a' b' refb'} {_} {()}
transEl₁ {⊥s} {⊤s} {_} {()}
transEl₁ {⊥s} {⊥s} {⊤s} {_} {()}
transEl₁ {⊥s} {⊥s} {⊥s} {_} {_} {x} {x'} {x''} x~ x~' = ttp'
transEl₁ {⊥s} {⊥s} {Σs a'' b'' refb''} {_} {()}
transEl₁ {⊥s} {⊥s} {Πs a'' b'' refb''} {_} {()}
transEl₁ {⊥s} {Σs a' b' refb'} {_} {()}
transEl₁ {⊥s} {Πs a' b' refb'} {_} {()}
transEl₁ {Σs a b refb} {⊤s} {_} {()}
transEl₁ {Σs a b refb} {⊥s} {_} {()}
transEl₁ {Σs a b refb} {Σs a' b' refb'} {⊤s} {_} {()}
transEl₁ {Σs a b refb} {Σs a' b' refb'} {⊥s} {_} {()}
transEl₁ {Σs a b refb} {Σs a' b' refb'} {Σs a'' b'' refb''}{_}{_}{x ,Σ y}{x' ,Σ y'}{x'' ,Σ y''}(x~ ,p y~)(x~' ,p y~') =
  transEl₁ {a} x~ x~' ,p transEl₁ {b _} y~ y~'
transEl₁ {Σs a b refb} {Σs a' b' refb'} {Πs a'' b'' refb''} {_} {()}
transEl₁ {Σs a b refb} {Πs a' b' refb'} {_} {()}
transEl₁ {Πs a b refb} {⊤s} {_} {()}
transEl₁ {Πs a b refb} {⊥s} {_} {()}
transEl₁ {Πs a b refb} {Σs a' b' refb'} {_} {()}
transEl₁ {Πs a b refb} {Πs a' b' refb'} {⊤s} {_} {()}
transEl₁ {Πs a b refb} {Πs a' b' refb'} {⊥s} {_} {()}
transEl₁ {Πs a b refb} {Πs a' b' refb'} {Σs a'' b'' refb''}{_}{()}
transEl₁ {Πs a b refb} {Πs a' b' refb'} {Πs a'' b'' refb''}{a~ ,p b~}{a~' ,p b~'}{f ,sp reff}{f' ,sp reff'}{f'' ,sp reff''} f~ f~' {x}{x''} x~ = transEl₁ {b _}
  (f~ (cohEl₁ a~ x)) (transEl₁ {b' _}{b' _}{b'' _}{refb' (transEl₁ {a'} (symEl₁ {a} (cohEl₁ a~ x)) (transEl₁ {a} x~ (cohEl₁ (symU₁ {a'} a~') x'')))}{_}
  (reff' (transEl₁ {a'} (symEl₁ {a} (cohEl₁ {a} a~ x)) (transEl₁ {a} x~ (cohEl₁ {a''} (symU₁ {a'} a~') x'')) ))
  (f~' (symEl₁ {a''} (cohEl₁ {a''} (symU₁ {a'} a~') x''))))
transEl₁ {ι a} {⊤s} {c} {()}
transEl₁ {ι a} {⊥s} {c} {()}
transEl₁ {ι a} {Σs b b₁ refb} {c} {()}
transEl₁ {ι a} {Πs b b₁ refb} {c} {()}
transEl₁ {ι a} {ι b} {⊤s} {_} {()}
transEl₁ {ι a} {ι b} {⊥s} {_} {()}
transEl₁ {ι a} {ι b} {Σs c b₁ refb} {_} {()}
transEl₁ {ι a} {ι b} {Πs c b₁ refb} {_} {()}
transEl₁ {ι a} {ι b} {ι c} (liftP x) (liftP y) = liftP (transEl x y)
transEl₁ {u} {⊤s} {_} {()}
transEl₁ {u} {⊥s} {_} {()}
transEl₁ {u} {Σs a'' b refb} {_} {()}
transEl₁ {u} {Πs a'' b refb} {_} {()}
transEl₁ {u} {ι x} {_} {()}
transEl₁ {u} {u} {⊤s} {_} {()}
transEl₁ {u} {u} {⊥s} {_} {()}
transEl₁ {u} {u} {Σs a'' b refb} {_} {()}
transEl₁ {u} {u} {Πs a'' b refb} {_} {()}
transEl₁ {u} {u} {ι x} {_} {()}
transEl₁ {u} {u} {u} x y = transU x y

coeEl₁ {⊤s} {⊤s} a~ x = x
coeEl₁ {⊤s} {⊥s} () x
coeEl₁ {⊤s} {Σs a' b' refb'} () x
coeEl₁ {⊤s} {Πs a' b' refb'} () x
coeEl₁ {⊥s} {⊤s} () x
coeEl₁ {⊥s} {⊥s} a~ x = x
coeEl₁ {⊥s} {Σs a' b refb} () x
coeEl₁ {⊥s} {Πs a' b refb} () x
coeEl₁ {Σs a b refb} {⊤s} () x
coeEl₁ {Σs a b refb} {⊥s} () x
coeEl₁ {Σs a b refb} {Σs a' b' refb'} (a~ ,p b~) (x ,Σ y) = coeEl₁ a~ x ,Σ coeEl₁ (b~ (cohEl₁ a~ x)) y
coeEl₁ {Σs a b refb} {Πs a' b' refb'} () x
coeEl₁ {Πs a b refb} {⊤s} () x
coeEl₁ {Πs a b refb} {⊥s} () x
coeEl₁ {Πs a b refb} {Σs a' b' refb'} () x
coeEl₁ {Πs a b refb} {Πs a' b' refb'} (a~ ,p b~) (f ,sp reff) = (λ x' → coeEl₁ (b~ (symEl₁ {a'} (cohEl₁ (symU₁ {a} a~) x'))) (f (coeEl₁ (symU₁ {a} a~) x'))) ,sp λ {x}{x'} x~ → transEl₁ {b' _}
  (symEl₁ {b _} (cohEl₁ (b~ (symEl₁ {a'} (cohEl₁ (symU₁ {a} a~) x))) (f (coeEl₁ (symU₁ {a} a~) x)))) (transEl₁ {b _}{b _}{b' _}
  {refb (transEl₁ {a} (symEl₁ {a'} (cohEl₁ (symU₁ {a} a~) x)) (transEl₁ {a'} x~ (cohEl₁ (symU₁ {a} a~) x')))}{_}
  (reff (transEl₁ {a} (symEl₁ {a'} (cohEl₁ (symU₁ {a} a~) x)) (transEl₁ {a'} x~ (cohEl₁ {a'} (symU₁ {a} a~) x'))))
  (cohEl₁ (b~ (symEl₁ {a'} (cohEl₁ (symU₁ {a} a~) x'))) (f (coeEl₁ (symU₁ {a} a~) x'))))
coeEl₁ {ι a} {⊤s} x = abort⊥p' x
coeEl₁ {ι a} {⊥s} x = abort⊥p' x
coeEl₁ {ι a} {Σs b b₁ refb} x = abort⊥p' x
coeEl₁ {ι a} {Πs b b₁ refb} x = abort⊥p' x
coeEl₁ {ι a} {ι b} p (lift x) = lift (coeEl p x)
coeEl₁ {u} {⊤s} x = abort⊥p' x
coeEl₁ {u} {⊥s} x = abort⊥p' x
coeEl₁ {u} {Σs b b₁ refb} x = abort⊥p' x
coeEl₁ {u} {Πs b b₁ refb} x = abort⊥p' x
coeEl₁ {u} {ι x₁} x = abort⊥p' x
coeEl₁ {u} {u} _ a = a

cohEl₁ {⊤s} {⊤s} a~ x = ttp'
cohEl₁ {⊤s} {⊥s} ()
cohEl₁ {⊤s} {Σs a' b' refb'} ()
cohEl₁ {⊤s} {Πs a' b' refb'} ()
cohEl₁ {⊥s} {⊤s} ()
cohEl₁ {⊥s} {⊥s} a~ x = ttp'
cohEl₁ {⊥s} {Σs a' b' refb'} ()
cohEl₁ {⊥s} {Πs a' b' refb'} ()
cohEl₁ {Σs a b refb} {⊤s} ()
cohEl₁ {Σs a b refb} {⊥s} ()
cohEl₁ {Σs a b refb} {Σs a' b' refb'}(a~ ,p b~)(x ,Σ y) = cohEl₁ a~ x ,p cohEl₁ (b~ (cohEl₁ a~ x)) y 
cohEl₁ {Σs a b refb} {Πs a' b' refb'} ()
cohEl₁ {Πs a b refb} {⊤s} ()
cohEl₁ {Πs a b refb} {⊥s} ()
cohEl₁ {Πs a b refb} {Σs a' b' refb'} ()
cohEl₁ {Πs a b refb} {Πs a' b' refb'}(a~ ,p b~)(f ,sp reff){x}{x'} x~ = transEl₁ {b x} {b (coeEl₁ (symU₁ {a} a~) x' )} {b' x'}
  {refb (transEl₁ {a} x~ (cohEl₁ (symU₁ {a} a~) x'))}{_}
  (reff (transEl₁ {a} x~ (cohEl₁ (symU₁ {a} a~) x')))
  (cohEl₁ {b (coeEl₁ (symU₁ {a} a~) x')} (b~ (symEl₁ {a'} (cohEl₁ (symU₁ {a} a~) x'))) (f (coeEl₁ (symU₁ {a} a~) x')))
cohEl₁ {ι a} {⊤s} ()
cohEl₁ {ι a} {⊥s} ()
cohEl₁ {ι a} {Σs b b₁ refb} ()
cohEl₁ {ι a} {Πs b b₁ refb} ()
cohEl₁ {ι a} {ι b} p (lift x) = liftP (cohEl p x)
cohEl₁ {u} {⊤s} ()                                  
cohEl₁ {u} {⊥s} ()                                  
cohEl₁ {u} {Σs b b₁ refb} ()                        
cohEl₁ {u} {Πs b b₁ refb} ()                        
cohEl₁ {u} {ι x₁} ()
cohEl₁ {u} {u} _ x = refU x

refEl₁ : {a : U₁}(x : El₁ a) → refU₁ a ⊢ x ~El₁ x
refEl₁ {⊤s} x = ttp'
refEl₁ {⊥s} x = ttp'
refEl₁ {Σs a b refb} (x ,Σ y) = refEl₁ x ,p refEl₁ y
refEl₁ {Πs a b refb} (f ,sp reff) = reff
refEl₁ {ι a} (lift x) = liftP (refEl x)
refEl₁ {u} x = refU x

𝕊₁ : {Γ : Con} → Ty Γ l1
𝕊₁ {Γ} = record
  { ∣_∣T_ = λ _ → U₁
  ; _T_⊢_~_ = λ _ → _~U₁_
  ; refT = refU₁
  ; symT = λ {γ}{γ'}{γ~}{α}{α'} α~ → symU₁ {α}{α'} α~
  ; transT = λ {γ}{γ'}{γ'}{γ~}{γ~'}{α}{α'}{α''} α~ α~' → transU₁ {α}{α'}{α''} α~ α~'
  ; coeT = λ _ a → a
  ; cohT = λ _ → refU₁
  }

El𝕊₁ : {Γ : Con} → Tm Γ 𝕊₁ → Ty Γ l1
El𝕊₁ {Γ} a = record
  { ∣_∣T_ = λ γ → El₁ (∣ a ∣t γ)
  ; _T_⊢_~_ = λ γ~ → ~t a γ~ ⊢_~El₁_
  ; refT = refEl₁
  ; symT =  λ {γ}{γ'}{γ~}{α}{α'} α~ → symEl₁ {x = α}{α'} α~
  ; transT = λ {γ}{γ'}{γ'}{γ~}{γ~'}{α}{α'}{α''} α~ α~' → transEl₁ {x = α}{α'}{α''} α~ α~'
  ; coeT = λ γ~ → coeEl₁ (~t a γ~)
  ; cohT = λ γ~ → cohEl₁ (~t a γ~)
  }

unit₁ : {Γ : Con} → Tm Γ 𝕊₁
unit₁ {Γ} = record { ∣_∣t = λ _ → ⊤s ; ~t = λ _ → ttp' }

*₁ : ∀{Γ} → Tm Γ (El𝕊₁ unit₁)
*₁ {Γ} = record { ∣_∣t = λ _ → lift tt ; ~t = λ _ → liftP ttp }

ΣΣ₁ : ∀{Γ}(a : Tm Γ 𝕊₁)(b : Tm (Γ , El𝕊₁ a) 𝕊₁) → Tm Γ 𝕊₁
ΣΣ₁ {Γ} a b = record {
  ∣_∣t = λ γ → Σs (∣ a ∣t γ) (λ x → ∣ b ∣t (γ ,Σ x)) λ x~ → ~t b (refC Γ γ ,p x~) ;
  ~t = λ γ~ → ~t a γ~ ,p λ x~ → ~t b (γ~ ,p x~) }

_,ΣΣ₁_ : ∀{Γ}{a : Tm Γ 𝕊₁}{b : Tm (Γ , El𝕊₁ a) 𝕊₁}(u : Tm Γ (El𝕊₁ a))(v : Tm Γ (El𝕊₁ b [ < u > ]T)) → Tm Γ (El𝕊₁ (ΣΣ₁ a b))
_,ΣΣ₁_ t v = record { ∣_∣t = λ γ → ∣ t ∣t γ ,Σ ∣ v ∣t γ ; ~t = λ γ~ → ~t t γ~ ,p ~t v γ~ }

proj₀Σ₁ : ∀{Γ}{a : Tm Γ 𝕊₁}{b : Tm (Γ , El𝕊₁ a) 𝕊₁}(w : Tm Γ (El𝕊₁ (ΣΣ₁ a b))) → Tm Γ (El𝕊₁ a)
proj₀Σ₁ w = record { ∣_∣t = λ γ → proj₁ (∣ w ∣t γ) ; ~t = λ γ~ → proj₁p (~t w γ~)  }

proj₂Σ₁ : ∀{Γ}{a : Tm Γ 𝕊₁}{b : Tm (Γ , El𝕊₁ a) 𝕊₁}(w : Tm Γ (El𝕊₁ (ΣΣ₁ a b))) → Tm Γ (El𝕊₁ b [ < proj₀Σ₁ {Γ}{a}{b} w > ]T)
proj₂Σ₁ w = record { ∣_∣t = λ γ → proj₂ (∣ w ∣t γ) ; ~t = λ γ~ → proj₂p (~t w γ~)  }

Π₁ : ∀{Γ}(a : Tm Γ 𝕊₁)(b : Tm (Γ , El𝕊₁ a) 𝕊₁) → Tm Γ 𝕊₁
Π₁ {Γ} a b = record {
  ∣_∣t = λ γ → Πs (∣ a ∣t γ) (λ x → ∣ b ∣t (γ ,Σ x)) λ x~ → ~t b (refC Γ γ ,p x~) ;
  ~t = λ γ~ → ~t a γ~ ,p λ x~ → ~t b (γ~ ,p x~) }

lam₁ : ∀{Γ}{a : Tm Γ 𝕊₁}{b : Tm (Γ , El𝕊₁ a) 𝕊₁} → Tm (Γ , El𝕊₁ a) (El𝕊₁ b) → Tm Γ (El𝕊₁ (Π₁ a b))
lam₁ {Γ} t = record {
  ∣_∣t = λ γ → (λ x → ∣ t ∣t (γ ,Σ x)) ,sp (λ x~ → ~t t (refC Γ γ ,p x~)) ;
  ~t = λ γ~ x~ → ~t t (γ~ ,p x~) }

app₁ : ∀{Γ}{a : Tm Γ 𝕊₁}{b : Tm (Γ , El𝕊₁ a) 𝕊₁} → Tm Γ (El𝕊₁ (Π₁ a b)) → Tm (Γ , El𝕊₁ a) (El𝕊₁ b)
app₁ {Γ} t = record {
  ∣_∣t = λ { (γ ,Σ x) → proj₁sp (∣ t ∣t γ) x } ;
  ~t = λ { (γ~ ,p x~) → ~t t γ~ x~ } }
