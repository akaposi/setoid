{-# OPTIONS --without-K --prop #-}

module Model.Func where

open import lib
open import Agda.Primitive

open import Model.Decl
open import Model.Core

∣Π∣ : {Γ : Con}{l : level}(A : Ty Γ l)(B : Ty (Γ , A) l)(γ : ∣ Γ ∣C) → Set ⌜ l ⌝
∣Π∣ {Γ}{l0}  A B γ = Σsp ((α : ∣ A ∣T γ) → ∣ B ∣T (γ ,Σ α)) λ ∣_∣Π → {α α' : ∣ A ∣T γ}(α~ : A T refC Γ γ ⊢ α ~ α') → B T refC Γ γ ,p α~ ⊢ ∣_∣Π α ~ ∣_∣Π α'
∣Π∣ {Γ}{l1} A B γ = Σsp ((α : ∣ A ∣T γ) → ∣ B ∣T (γ ,Σ α)) λ ∣_∣Π → {α α' : ∣ A ∣T γ}(α~ : A T refC Γ γ ⊢ α ~ α') → B T refC Γ γ ,p α~ ⊢ ∣_∣Π α ~ ∣_∣Π α'

infix 4 ∣_∣Π

∣_∣Π : ∀{ℓ ℓ'}{A : Set ℓ}{B : A → Prop ℓ'} → Σsp A B → A
∣_∣Π = proj₁sp

~Π : ∀{ℓ ℓ'}{A : Set ℓ}{B : A → Prop ℓ'}(w : Σsp A B) → B (proj₁sp w)
~Π = proj₂sp

Π : {Γ : Con}{l : level}(A : Ty Γ l) → Ty (Γ , A) l → Ty Γ l
Π {Γ}{l0} A B = record
  { ∣_∣T_ = ∣Π∣ A B
  ; _T_⊢_~_ = λ {γ}{γ'} γ~ f f' → {α : ∣ A ∣T γ}{α' : ∣ A ∣T γ'}(α~ : A T γ~ ⊢ α ~ α') → B T γ~ ,p α~ ⊢ ∣ f ∣Π α ~ ∣ f' ∣Π α'
  ; refT = ~Π
  ; symT = λ f~ α~ → symT B (f~ (symT A α~))
  ; transT = λ {_}{_}{_}{γ~} f~ f~' {α}{α''} α~ → transT B (f~ (cohT A γ~ α)) (f~' (transT A (symT A (cohT A γ~ α)) α~))
  ; coeT = λ {γ}{γ'} γ~ f → (λ α' → coeT B (γ~ ,p symT A (cohT A (symC Γ γ~) α')) (∣ f ∣Π (coeT A (symC Γ γ~) α'))) ,sp (λ {α}{α'} α~ → transT B (symT B (cohT B (γ~ ,p symT A (cohT A (symC Γ γ~) α)) (∣ f ∣Π (coeT A (symC Γ γ~) α)))) (transT B (~Π f (transT A (symT A (cohT A (symC Γ γ~) α)) (transT A α~ (cohT A (symC Γ γ~) α')))) (cohT B (γ~ ,p symT A (cohT A (symC Γ γ~) α')) (∣ f ∣Π (coeT A (symC Γ γ~) α')))))
  ; cohT = λ {γ}{γ'} γ~ f {α}{α'} α~ → transT B (~Π f (transT A α~ (cohT A (symC Γ γ~) α'))) (cohT B (γ~ ,p symT A (cohT A (symC Γ γ~) α')) (∣ f ∣Π (coeT A (symC Γ γ~) α')))
  }
Π {Γ}{l1} A B = record
  { ∣_∣T_ = ∣Π∣ A B
  ; _T_⊢_~_ = λ {γ}{γ'} γ~ f f' → {α : ∣ A ∣T γ}{α' : ∣ A ∣T γ'}(α~ : A T γ~ ⊢ α ~ α') → B T γ~ ,p α~ ⊢ ∣ f ∣Π α ~ ∣ f' ∣Π α'
  ; refT = ~Π
  ; symT = λ f~ α~ → symT B (f~ (symT A α~))
  ; transT = λ {_}{_}{_}{γ~} f~ f~' {α}{α''} α~ → transT B (f~ (cohT A γ~ α)) (f~' (transT A (symT A (cohT A γ~ α)) α~))
  ; coeT = λ {γ}{γ'} γ~ f → (λ α' → coeT B (γ~ ,p symT A (cohT A (symC Γ γ~) α')) (∣ f ∣Π (coeT A (symC Γ γ~) α'))) ,sp (λ {α}{α'} α~ → transT B (symT B (cohT B (γ~ ,p symT A (cohT A (symC Γ γ~) α)) (∣ f ∣Π (coeT A (symC Γ γ~) α)))) (transT B (~Π f (transT A (symT A (cohT A (symC Γ γ~) α)) (transT A α~ (cohT A (symC Γ γ~) α')))) (cohT B (γ~ ,p symT A (cohT A (symC Γ γ~) α')) (∣ f ∣Π (coeT A (symC Γ γ~) α')))))
  ; cohT = λ {γ}{γ'} γ~ f {α}{α'} α~ → transT B (~Π f (transT A α~ (cohT A (symC Γ γ~) α'))) (cohT B (γ~ ,p symT A (cohT A (symC Γ γ~) α')) (∣ f ∣Π (coeT A (symC Γ γ~) α')))
  }

Π[] : {Γ Δ : Con} {σ : Tms Γ Δ}{l : level}{A : Ty Δ l} {B : Ty (Δ , A) l} →
  Π A B [ σ ]T ≡ Π (A [ σ ]T) (B [ _,s_ (σ ∘ π₁ {A = A [ σ ]T} id) {A = A} (π₂ {A = A [ σ ]T} id) ]T)
Π[] {l = l0}  = refl
Π[] {l = l1} = refl

lam : {Γ : Con} {l : level} {A : Ty Γ l} {B : Ty (Γ , A) l} → Tm (Γ , A) B → Tm Γ (Π A B)
lam {Γ}{l0} {A}{B} t = record { ∣_∣t = λ γ → (λ α → ∣ t ∣t (γ ,Σ α)) ,sp λ α~ → ~t t (refC Γ γ ,p α~) ; ~t = λ γ~ α~ → ~t t (γ~ ,p α~) }
lam {Γ}{l1}{A}{B} t = record { ∣_∣t = λ γ → (λ α → ∣ t ∣t (γ ,Σ α)) ,sp λ α~ → ~t t (refC Γ γ ,p α~) ; ~t = λ γ~ α~ → ~t t (γ~ ,p α~) }

app : {Γ : Con} {l : level} {A : Ty Γ l} {B : Ty (Γ , A) l} → Tm Γ (Π A B) → Tm (Γ , A) B
app {Γ}{l0} {A}{B} t = record { ∣_∣t = λ { (γ ,Σ α) → ∣ ∣ t ∣t γ ∣Π α } ; ~t = λ { (γ~ ,p α~) → ~t t γ~ α~ } }
app {Γ}{l1}{A}{B} t = record { ∣_∣t = λ { (γ ,Σ α) → ∣ ∣ t ∣t γ ∣Π α } ; ~t = λ { (γ~ ,p α~) → ~t t γ~ α~ } }

lam[] : ∀{Γ Δ}{δ : Tms Γ Δ}{A : Ty Δ l0}{B : Ty (Δ , A) l0}{t : Tm (Δ , A) B} →
  _≡_ {A = Tm Γ (Π A B [ δ ]T)}
      (_[_]t {Γ}{Δ}{l0}{Π A B}(lam {Δ}{l0}{A}{B} t) δ)
      (lam {Γ}{l0}{A [ δ ]T}{B [ δ ^ A ]T} (t [ δ ^ A ]t))
lam[] = refl

Πβ : {Γ : Con}{l : level} {A : Ty Γ l} {B : Ty (Γ , A) l}{t : Tm (Γ , A) B} → app {Γ}(lam t) ≡ t
Πβ {Γ} {l0} = refl
Πβ {Γ} {l1} = refl

Πη : {Γ : Con}{l : level} {A : Ty Γ l} {B : Ty (Γ , A) l}{t : Tm Γ (Π A B)} → lam (app t) ≡ t
Πη {Γ} {l0} = refl
Πη {Γ} {l1} = refl

-- functional extensionality

open import Model.Iden

extt : {Γ : Con}{A : Ty Γ l0} {B : Ty (Γ , A) l0} {f g : Tm Γ (Π A B)} → Tm (Γ , A) (Id {A = B} (app {A = A} f) (app {A = A} g)) → Tm Γ (Id {A = Π A B} f g)
extf : {Γ : Con}{A : Ty Γ l1}{B : Ty (Γ , A) l1}{f g : Tm Γ (Π A B)} → Tm (Γ , A) (Id {A = B} (app {A = A} f) (app {A = A} g)) → Tm Γ (Id {A = Π A B} f g)
extt {Γ}{A}{B}{f}{g} t = record { ∣_∣t = λ γ → liftp λ {α}{α'} α~ → transT B (unliftp (∣ t ∣t (γ ,Σ α))) (~Π (∣ g ∣t γ) α~) }
extf {Γ}{A}{B}{f}{g} t = record { ∣_∣t = λ γ → liftp λ {α}{α'} α~ → transT B (unliftp (∣ t ∣t (γ ,Σ α))) (~Π (∣ g ∣t γ) α~) }
