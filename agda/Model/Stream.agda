{-# OPTIONS --guardedness --without-K --prop #-}

module Model.Stream where

open import lib hiding (ind)

open import Model.Decl
open import Model.Core

infixr 5 _∷_
record Stream {l} (A : Set l) : Set l where
  constructor _∷_
  coinductive
  field
    hd : A
    tl : Stream A

open Stream

record Streamᴿ {l} {A A' : Set l} (Aᴿ : A → A' → Prop l) (s : Stream A) (s' : Stream A') : Prop l where
  constructor _∷ᴿ_
  coinductive
  field
    hdᴿ : Aᴿ (hd s) (hd s')
    tlᴿ : Streamᴿ Aᴿ (tl s) (tl s')

open Streamᴿ


map : ∀ {a b} {A : Set a} {B : Set b} → (A → B) → Stream A → Stream B
hd (map f s) = f (hd s)
tl (map f s) = map f (tl s)

mapᴿ : ∀ {a} {A A' : Set a} {Aᴿ : A → A' → Prop a} {f : A → A'} (f' : ∀ x → Aᴿ x (f x)) s → Streamᴿ Aᴿ s (map f s)
hdᴿ (mapᴿ f' s) = f' (hd s)
tlᴿ (mapᴿ f' s) = mapᴿ f' (tl s)

reflᴿ : ∀ {a} {A : Set a} {Aᴿ : A → A → Prop a} (refA : ∀ a → Aᴿ a a) (s : Stream A) → Streamᴿ Aᴿ s s
hdᴿ (reflᴿ refA s) = refA (hd s)
tlᴿ (reflᴿ refA s) = reflᴿ refA (tl s)

symᴿ : ∀ {a} {A A' : Set a} {Aᴿ : A → A' → Prop a} {Aᴿ' : A' → A → Prop a} (symA : ∀ {x₀ x₁} → Aᴿ x₀ x₁ → Aᴿ' x₁ x₀) {s₀ s₁} → Streamᴿ Aᴿ s₀ s₁ → Streamᴿ Aᴿ' s₁ s₀
hdᴿ (symᴿ symA s₀₁) = symA (hdᴿ s₀₁)
tlᴿ (symᴿ symA s₀₁) = symᴿ symA (tlᴿ s₀₁)

transᴿ : ∀ {a}{A₀ A₁ A₂ : Set a}{A₀₁ : A₀ → A₁ → Prop a}{A₁₂ : A₁ → A₂ → Prop a}{A₀₂ : A₀ → A₂ → Prop a}
           (transA : ∀ {x₀ x₁ x₂} → A₀₁ x₀ x₁ → A₁₂ x₁ x₂ → A₀₂ x₀ x₂) {s₀ s₁ s₂} → Streamᴿ A₀₁ s₀ s₁ → Streamᴿ A₁₂ s₁ s₂ → Streamᴿ A₀₂ s₀ s₂
hdᴿ (transᴿ transA s₀₁ s₁₂) = transA (hdᴿ s₀₁) (hdᴿ s₁₂)
tlᴿ (transᴿ transA s₀₁ s₁₂) = transᴿ transA (tlᴿ s₀₁) (tlᴿ s₁₂)


Streamᵗ : {Γ : Con}(A : Ty Γ l0) → Ty Γ l0
Streamᵗ A = record
            { ∣_∣T_ = λ γ → Stream (∣ A ∣T γ)
            ; _T_⊢_~_ = λ p s₀ s₁ → Streamᴿ (_T_⊢_~_ A p) s₀ s₁
            ; refT = reflᴿ (refT A)
            ; symT = symᴿ (symT A)
            ; transT = transᴿ (transT A)
            ; coeT = λ p → map (coeT A p)
            ; cohT = λ p → mapᴿ (cohT A p)
            }

hdᵗ : {Γ : Con}{A : Ty Γ l0} → Tm Γ (Streamᵗ A) → Tm Γ A
hdᵗ t = record { ∣_∣t = λ γ → hd (∣ t ∣t γ) ; ~t = λ p → hdᴿ (~t t p) }

tlᵗ : {Γ : Con}{A : Ty Γ l0} → Tm Γ (Streamᵗ A) → Tm Γ (Streamᵗ A)
tlᵗ t = record { ∣_∣t = λ γ → tl (∣ t ∣t γ) ; ~t = λ p → tlᴿ (~t t p) }

corec : ∀ {ℓ}{A : Set}(S : Set ℓ)(M₀ : S → A)(M₁ : S → S)(N : S) → Stream A
hd (corec S M₀ M₁ N) = M₀ N
tl (corec S M₀ M₁ N) = corec S M₀ M₁ (M₁ N)



corec' : ∀ {ℓ}{A A' : Set ℓ}(Aᴿ : A → A' → Prop ℓ)(S : Stream A → Stream A' → Set ℓ)
           (M₀ : ∀ {s₀ s₁} (s₀₁ : S s₀ s₁) → Aᴿ (hd s₀) (hd s₁))
           (M₁ : ∀ {s₀ s₁} (s₀₁ : S s₀ s₁) → S  (tl s₀) (tl s₁))
           {s₀ s₁} (N : S s₀ s₁) → Streamᴿ Aᴿ s₀ s₁
hdᴿ (corec' Aᴿ S M₀ M₁ N) = M₀ N
tlᴿ (corec' Aᴿ S M₀ M₁ N) = corec' Aᴿ S M₀ M₁ (M₁ N)


lemma₀ : ∀ {Γ}(A : Ty Γ l0)(S : Ty Γ l0)(M₀ : Tm (Γ , S) (A [ wk' S ]T))(M₁ : Tm (Γ , S) (S [ wk' S ]T))(N : Tm Γ S){γ γ'}(p : Γ C γ ~ γ') → 
       Streamᴿ (_T_⊢_~_ A p) (corec (∣ S ∣T γ) (λ x → ∣ M₀ ∣t (γ ,Σ x)) (λ x → ∣ M₁ ∣t (γ ,Σ x)) (∣ N ∣t γ))
                             (corec (∣ S ∣T γ') (λ x → ∣ M₀ ∣t (γ' ,Σ x)) (λ x → ∣ M₁ ∣t (γ' ,Σ x)) (∣ N ∣t γ'))
hdᴿ (lemma₀ A S M₀ M₁ N p) = ~t M₀ (p ,p (~t N p))
tlᴿ (lemma₀ A S M₀ M₁ N p) = lemma₀ A S M₀ M₁ (M₁ [ < N > ]t) p

lemma₁ : ∀ {Γ}(A : Ty Γ l0)(S : Ty Γ l1)(M₀ : Tm (Γ , S) (A [ wk' S ]T))(M₁ : Tm (Γ , S) (S [ wk' S ]T))(N : Tm Γ S){γ γ'}(p : Γ C γ ~ γ') → 
       Streamᴿ (_T_⊢_~_ A p) (corec (∣ S ∣T γ) (λ x → ∣ M₀ ∣t (γ ,Σ x)) (λ x → ∣ M₁ ∣t (γ ,Σ x)) (∣ N ∣t γ))
                             (corec (∣ S ∣T γ') (λ x → ∣ M₀ ∣t (γ' ,Σ x)) (λ x → ∣ M₁ ∣t (γ' ,Σ x)) (∣ N ∣t γ'))
hdᴿ (lemma₁ A S M₀ M₁ N p) = ~t M₀ (p ,p (~t N p))
tlᴿ (lemma₁ A S M₀ M₁ N p) = lemma₁ A S M₀ M₁ (M₁ [ < N > ]t) p



corecᵗ : ∀ {Γ}{A : Ty Γ l0}{b}(S : Ty Γ b)(M₀ : Tm (Γ , S) (A [ wk' S ]T))(M₁ : Tm (Γ , S) (S [ wk' S ]T))(N : Tm Γ S) → Tm Γ (Streamᵗ A)
corecᵗ {Γ}{A}{l0} S M₀ M₁ N = record { ∣_∣t = λ γ → corec (∣ S ∣T γ) (λ x → ∣ M₀ ∣t (γ ,Σ x)) (λ x → ∣ M₁ ∣t (γ ,Σ x)) (∣ N ∣t γ)
                          ; ~t = λ p → lemma₀ A S M₀ M₁ N p }
corecᵗ {Γ}{A}{l1} S M₀ M₁ N = record { ∣_∣t = λ γ → corec (∣ S ∣T γ) (λ x → ∣ M₀ ∣t (γ ,Σ x)) (λ x → ∣ M₁ ∣t (γ ,Σ x)) (∣ N ∣t γ)
                          ; ~t = λ p → lemma₁ A S M₀ M₁ N p }
