{-# OPTIONS --without-K --prop #-}

module Model.List where

open import lib hiding (ind)

open import Model.Decl
open import Model.Core

infixr 5 _∷_
data List {a} (A : Set a) : Set a where
  []  : List A
  _∷_ : (x : A) (xs : List A) → List A

map : ∀ {a b} {A : Set a} {B : Set b} → (A → B) → List A → List B
map f []       = []
map f (x ∷ xs) = f x ∷ map f xs

data Listᴿ {a} {A A' : Set a} (Aᴿ : A → A' → Prop a) : List A → List A' → Prop a where
  []ᴿ  : Listᴿ Aᴿ [] []
  _∷ᴿ_ : ∀ {x₀ x₁ xs₀ xs₁} → Aᴿ x₀ x₁ → Listᴿ Aᴿ xs₀ xs₁ → Listᴿ Aᴿ (x₀ ∷ xs₀) (x₁ ∷ xs₁)

mapᴿ : ∀ {a} {A A' : Set a} {Aᴿ : A → A' → Prop a} {f : A → A'} (f' : ∀ x → Aᴿ x (f x)) l → Listᴿ Aᴿ l (map f l)
mapᴿ f' [] = []ᴿ
mapᴿ f' (x ∷ l) = (f' x) ∷ᴿ (mapᴿ f' l)


reflᴿ : ∀ {a} {A : Set a} {Aᴿ : A → A → Prop a} (refA : ∀ a → Aᴿ a a) (l : List A) → Listᴿ Aᴿ l l
reflᴿ refA [] = []ᴿ
reflᴿ refA (x ∷ l) = (refA x) ∷ᴿ (reflᴿ refA l)

symᴿ : ∀ {a} {A A' : Set a} {Aᴿ : A → A' → Prop a} {Aᴿ' : A' → A → Prop a} (symA : ∀ {x₀ x₁} → Aᴿ x₀ x₁ → Aᴿ' x₁ x₀) {l₀ l₁} → Listᴿ Aᴿ l₀ l₁ → Listᴿ Aᴿ' l₁ l₀
symᴿ symA []ᴿ = []ᴿ
symᴿ symA (x₀₁ ∷ᴿ l₀₁) = (symA x₀₁) ∷ᴿ (symᴿ symA l₀₁)

transᴿ : ∀ {a}{A₀ A₁ A₂ : Set a}{A₀₁ : A₀ → A₁ → Prop a}{A₁₂ : A₁ → A₂ → Prop a}{A₀₂ : A₀ → A₂ → Prop a}
           (transA : ∀ {x₀ x₁ x₂} → A₀₁ x₀ x₁ → A₁₂ x₁ x₂ → A₀₂ x₀ x₂) {l₀ l₁ l₂} → Listᴿ A₀₁ l₀ l₁ → Listᴿ A₁₂ l₁ l₂ → Listᴿ A₀₂ l₀ l₂
transᴿ transA []ᴿ []ᴿ = []ᴿ
transᴿ transA (x₀₁ ∷ᴿ l₀₁) (x₁₂ ∷ᴿ l₁₂) = transA x₀₁ x₁₂ ∷ᴿ transᴿ transA l₀₁ l₁₂

-- transᴿ : {n m k : ℕ} → ℕᴿ n m → ℕᴿ m k → ℕᴿ n k
-- transᴿ zeroᴿ zeroᴿ = zeroᴿ
-- transᴿ (sucᴿ p) (sucᴿ q) = sucᴿ (transᴿ p q)

Listᵗ : {Γ : Con}(A : Ty Γ l0) → Ty Γ l0
Listᵗ A = record
            { ∣_∣T_ = λ γ → List (∣ A ∣T γ)
            ; _T_⊢_~_ = λ p l₀ l₁ → Listᴿ (_T_⊢_~_ A p) l₀ l₁
            ; refT = reflᴿ (refT A)
            ; symT = symᴿ (symT A)
            ; transT = transᴿ (transT A)
            ; coeT = λ p → map (coeT A p)
            ; cohT = λ p → mapᴿ (cohT A p)
            }

nilᵗ : {Γ : Con}{A : Ty Γ l0} → Tm Γ (Listᵗ A)
nilᵗ = record { ∣_∣t = λ γ → [] ; ~t = λ p → []ᴿ }

consᵗ : {Γ : Con}{A : Ty Γ l0}(x : Tm Γ A)(l : Tm Γ (Listᵗ A)) → Tm Γ (Listᵗ A)
consᵗ x l = record { ∣_∣t = λ γ → ∣ x ∣t γ ∷ ∣ l ∣t γ
                   ; ~t = λ p → (~t x p) ∷ᴿ (~t l p) }

ind : ∀ {ℓ}{A : Set}(P : List A → Set ℓ)(z : P [])(s : ∀ x l → P l → P (x ∷ l)) l → P l
ind P z s [] = z
ind P z s (x ∷ l) = s x l (ind P z s l)

indᵗ : ∀ {Γ}{A}{b}(P : Ty (Γ , Listᵗ A) b)(z : Tm Γ (P [ < nilᵗ {_}{A} > ]T))(s : Tm (Γ , Listᵗ A , P , A [ {!!} ]T) (P [ {!!} ]T))(l : Tm Γ (Listᵗ A)) → Tm Γ (P [ < l > ]T)
indᵗ P z s l = record { ∣_∣t = λ γ → ind (λ l → ∣ P ∣T γ ,Σ l) (∣ z ∣t γ) (λ x l hl → {!!}) (∣ l ∣t γ)
                      ; ~t = {!!} }
-- indᵗ {Γ}{l0} P z s n =
--   record { ∣_∣t = λ γ → ind' (λ n → ∣ P ∣T γ ,Σ n) (∣ z ∣t γ) (λ IH → ∣ s ∣t (_ ,Σ IH)) (∣ n ∣t γ)
--          ; ~t   = λ {γ}{γ'}p → indᴿ (λ n → ∣ P ∣T γ ,Σ n) (λ n → ∣ P ∣T γ' ,Σ n) (λ nₚ x₀ x₁ → P T (p ,p nₚ) ⊢ x₀ ~ x₁) (∣ z ∣t γ) (∣ z ∣t γ') (~t z p) (λ IH → ∣ s ∣t (_ ,Σ IH)) (λ IH → ∣ s ∣t (_ ,Σ IH)) (λ x → ~t s (_ ,p x)) (~t n p) }
-- indᵗ {Γ}{false} P z s n =
--   record { ∣_∣t = λ γ → ind' (λ n → ∣ P ∣T γ ,Σ n) (∣ z ∣t γ) (λ IH → ∣ s ∣t (_ ,Σ IH)) (∣ n ∣t γ)
--          ; ~t   = λ {γ}{γ'}p → indᴿ (λ n → ∣ P ∣T γ ,Σ n) (λ n → ∣ P ∣T γ' ,Σ n) (λ nₚ x₀ x₁ → P T (p ,p nₚ) ⊢ x₀ ~ x₁) (∣ z ∣t γ) (∣ z ∣t γ') (~t z p) (λ IH → ∣ s ∣t (_ ,Σ IH)) (λ IH → ∣ s ∣t (_ ,Σ IH)) (λ x → ~t s (_ ,p x)) (~t n p) }
