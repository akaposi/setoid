{-# OPTIONS --without-K --prop #-}

module Model.Props1 where

open import lib
open import Agda.Primitive

open import Model.Decl
open import Model.Core

-- a universe of propositions

ℙ₀ : {Γ : Con} → Ty Γ l1
ℙ₀ {Γ} = record
  { ∣_∣T_ = λ γ → Prop
  ; _T_⊢_~_ = λ _ a b → LiftP ((a → b) ×p (b → a))
  ; refT = λ _ → liftP ((λ x → x) ,p (λ x → x))
  ; symT = λ { (liftP (f ,p g)) → liftP (g ,p f) }
  ; transT = λ { (liftP (f ,p g)) (liftP (f' ,p g')) → liftP ((λ x → f' (f x)) ,p (λ y → g (g' y))) }
  ; coeT = λ _ a → a
  ; cohT = λ _ _ → liftP ((λ x → x) ,p (λ x → x))
  }

Elℙ₀ : {Γ : Con} → Tm Γ ℙ₀ → Ty Γ l0
Elℙ₀ {Γ} t = record
  { ∣_∣T_ = λ γ → Liftp (∣ t ∣t γ)
  ; _T_⊢_~_ = λ _ _ _ → ⊤p
  ; coeT = λ { γ~ (liftp α) → liftp (proj₁p (unliftP (~t t γ~)) α) }
  }

ℙ₀[] : {Γ Δ : Con}{σ : Tms Γ Δ} → ℙ₀ [ σ ]T ≡ ℙ₀
ℙ₀[] = refl
Elℙ₀[] : {Γ Δ : Con}{σ : Tms Γ Δ}{t : Tm Δ ℙ₀} → Elℙ₀ t [ σ ]T ≡ Elℙ₀ (t [ σ ]t)
Elℙ₀[] = refl

-- strict Prop

irr : {Γ : Con}{a : Tm Γ ℙ₀}(u v : Tm Γ (Elℙ₀ a)) → u ≡ v
irr u v = refl

-- closed under unit

⊤ℙ₀ : {Γ : Con} → Tm Γ ℙ₀
⊤ℙ₀ {Γ} = record { ∣_∣t = λ _ → ⊤p ; ~t = λ _ → liftP ((λ x → x) ,p (λ x → x)) }

ttℙ₀ : {Γ : Con} → Tm Γ (Elℙ₀ ⊤ℙ₀)
ttℙ₀ {Γ} = record {}

⊤ℙ₀[] : {Γ Δ : Con}{σ : Tms Γ Δ} → ⊤ℙ₀ [ σ ]t ≡ ⊤ℙ₀
⊤ℙ₀[] = refl
ttℙ₀[] : {Γ Δ : Con}{σ : Tms Γ Δ} → ttℙ₀ [ σ ]t ≡ ttℙ₀
ttℙ₀[] = refl

-- closed under empty

⊥ℙ₀ : {Γ : Con} → Tm Γ ℙ₀
⊥ℙ₀ {Γ} = record { ∣_∣t = λ γ → ⊥p ; ~t = λ _ → liftP ((λ x → x) ,p (λ x → x)) }

abortℙ₀ : {Γ : Con}{l : level}{A : Ty Γ l} → Tm Γ (Elℙ₀ ⊥ℙ₀) → Tm Γ A
abortℙ₀ {Γ}{A} t = record
  { ∣_∣t = λ γ → abort⊥p (unliftp (∣ t ∣t γ))
  ; ~t = λ {γ} _ → abort⊥pp (unliftp (∣ t ∣t γ))
  }

⊥ℙ₀[] : {Γ Δ : Con}{σ : Tms Γ Δ} → ⊥ℙ₀ [ σ ]t ≡ ⊥ℙ₀
⊥ℙ₀[] = refl
abortℙ₀[] : {Γ Δ : Con}{σ : Tms Γ Δ}{l : level}{A : Ty Δ l}{t : Tm Δ (Elℙ₀ ⊥ℙ₀)} → abortℙ₀ {A = A} t [ σ ]t ≡ abortℙ₀ (t [ σ ]t)
abortℙ₀[] = refl

-- closed under Pi

Πℙ₀ : {Γ : Con}(A : Ty Γ l0)(b : Tm (Γ , A) ℙ₀) → Tm Γ ℙ₀
Πℙ₀ {Γ} A b = record
  { ∣_∣t = λ γ → (α : ∣ A ∣T γ) → ∣ b ∣t (γ ,Σ α)
    ; ~t = λ {γ}{γ'} γ~ → liftP ((λ f α' → proj₁p (unliftP (~t b (γ~ ,p symT A (cohT A (symC Γ γ~) α')))) (f (coeT A (symC Γ γ~) α'))) ,p
                                 λ f' α → proj₂p (unliftP (~t b (γ~ ,p cohT A γ~ α))) (f' (coeT A γ~ α))) }

lamℙ₀ : {Γ : Con}{A : Ty Γ l0}{b : Tm (Γ , A) ℙ₀} → Tm (Γ , A) (Elℙ₀ b) → Tm Γ (Elℙ₀ (Πℙ₀ A b))
lamℙ₀ {Γ}{A}{b} t = record { ∣_∣t = λ γ → liftp λ α → unliftp (∣ t ∣t (γ ,Σ α)) }

appℙ₀ : {Γ : Con}{A : Ty Γ l0}{b : Tm (Γ , A) ℙ₀} → Tm Γ (Elℙ₀ (Πℙ₀ A b)) → Tm (Γ , A) (Elℙ₀ b)
appℙ₀ {Γ}{A}{b} t = record { ∣_∣t = λ { (γ ,Σ α) → liftp (unliftp (∣ t ∣t γ) α) } }

Πℙ₀β : {Γ : Con}{A : Ty Γ l0}{b : Tm (Γ , A) ℙ₀}{t : Tm (Γ , A) (Elℙ₀ b)} → appℙ₀ {A = A}{b} (lamℙ₀ {A = A}{b} t) ≡ t
Πℙ₀β = refl

Πℙ₀η : {Γ : Con}{A : Ty Γ l0}{b : Tm (Γ , A) ℙ₀}{t : Tm Γ (Elℙ₀ (Πℙ₀ A b))} → lamℙ₀ {A = A}{b} (appℙ₀ {A = A}{b} t) ≡ t
Πℙ₀η = refl

Πℙ₀[] : {Γ : Con}{A : Ty Γ l0}{b : Tm (Γ , A) ℙ₀}{Θ : Con}{σ : Tms Θ Γ} → Πℙ₀ A b [ σ ]t ≡ Πℙ₀ (A [ σ ]T) (b [ σ ^ A ]t)
Πℙ₀[] = refl

lamℙ₀[] : {Γ : Con}{A : Ty Γ l0}{b : Tm (Γ , A) ℙ₀}{t : Tm (Γ , A) (Elℙ₀ b)}{Θ : Con}{σ : Tms Θ Γ} →
  lamℙ₀ {A = A}{b} t [ σ ]t ≡ lamℙ₀ {A = A [ σ ]T}{b [ σ ^ A ]t}(t [ σ ^ A ]t)
lamℙ₀[] = refl

-- closed under Sigma

Σℙ₀ : {Γ : Con}(a : Tm Γ ℙ₀)(b : Tm (Γ , Elℙ₀ a) ℙ₀) → Tm Γ ℙ₀
Σℙ₀ {Γ} a b = record
  { ∣_∣t = λ γ → Σp (∣ a ∣t γ) λ α → ∣ b ∣t (γ ,Σ liftp α)
  ; ~t = λ {γ}{γ'} γ~ → liftP ((λ { (α ,p β) → proj₁p (unliftP (~t a γ~)) α ,p proj₁p (unliftP (~t b (γ~ ,p _))) β }) ,p
                               (λ { (α ,p β) → proj₂p (unliftP (~t a γ~)) α ,p proj₂p (unliftP (~t b (γ~ ,p _))) β }))
  }

_,ℙ₀_ : {Γ : Con}{a : Tm Γ ℙ₀}{b : Tm (Γ , Elℙ₀ a) ℙ₀}(t : Tm Γ (Elℙ₀ a))(u : Tm Γ (Elℙ₀ b [ < t > ]T)) →
  Tm Γ (Elℙ₀ (Σℙ₀ a b))
t ,ℙ₀ u = record { ∣_∣t = λ γ → liftp (unliftp (∣ t ∣t γ) ,p unliftp (∣ u ∣t γ)) }

proj₁ℙ₀ : {Γ : Con}{a : Tm Γ ℙ₀}{b : Tm (Γ , Elℙ₀ a) ℙ₀} → Tm Γ (Elℙ₀ (Σℙ₀ a b)) → Tm Γ (Elℙ₀ a)
proj₁ℙ₀ w = record { ∣_∣t = λ γ → liftp (proj₁p (unliftp (∣ w ∣t γ))) }

proj₂ℙ₀ : {Γ : Con}{a : Tm Γ ℙ₀}{b : Tm (Γ , Elℙ₀ a) ℙ₀}(w : Tm Γ (Elℙ₀ (Σℙ₀ a b))) →
  Tm Γ (Elℙ₀ b [ < proj₁ℙ₀ {a = a}{b} w > ]T)
proj₂ℙ₀ w = record { ∣_∣t = λ γ → liftp (proj₂p (unliftp (∣ w ∣t γ))) }

Σℙ₀β₁ : {Γ : Con}{a : Tm Γ ℙ₀}{b : Tm (Γ , Elℙ₀ a) ℙ₀}{t : Tm Γ (Elℙ₀ a)}{u : Tm Γ (Elℙ₀ b [ < t > ]T)} →
  proj₁ℙ₀ {a = a}{b} (_,ℙ₀_ {a = a}{b} t u) ≡ t
Σℙ₀β₁ = refl

Σℙ₀β₂ : {Γ : Con}{a : Tm Γ ℙ₀}{b : Tm (Γ , Elℙ₀ a) ℙ₀}{t : Tm Γ (Elℙ₀ a)}{u : Tm Γ (Elℙ₀ b [ < t > ]T)}
  → proj₂ℙ₀ {a = a}{b} (_,ℙ₀_ {a = a}{b} t u) ≡ u
Σℙ₀β₂ = refl

Σℙ₀[] : {Γ : Con}{a : Tm Γ ℙ₀}{b : Tm (Γ , Elℙ₀ a) ℙ₀}{Θ : Con}{σ : Tms Θ Γ} →
  Σℙ₀ a b [ σ ]t ≡ Σℙ₀ (a [ σ ]t) (b [ σ ^ Elℙ₀ a ]t)
Σℙ₀[] = refl

,ℙ₀[] : {Γ : Con}{a : Tm Γ ℙ₀}{b : Tm (Γ , Elℙ₀ a) ℙ₀}{t : Tm Γ (Elℙ₀ a)}{u : Tm Γ (Elℙ₀ b [ < t > ]T)}{Θ : Con}{σ : Tms Θ Γ} →
  (_,ℙ₀_ {a = a}{b} t u) [ σ ]t ≡ _,ℙ₀_ {a = a [ σ ]t}{b = b [ σ ^ Elℙ₀ a ]t} (t [ σ ]t) (u [ σ ]t)
,ℙ₀[] = refl

record Pf (Γ : Con)(a : Tm Γ ℙ₀) : Set₁ where
  field
    ∣_∣p : (γ : ∣ Γ ∣C) → ∣ a ∣t γ
  --    ~t   : {γ γ' : ∣ Γ ∣C}(p : Γ C γ ~ γ') → A T p ⊢ (∣_∣t γ) ~ (∣_∣t γ')
  infix 4 ∣_∣p
open Pf public

mkPf : ∀{Γ a} → Tm Γ (Elℙ₀ a) → Pf Γ a
mkPf t = record { ∣_∣p = λ γ → unliftp (∣ t ∣t γ) }

unPf : ∀{Γ a} → Pf Γ a → Tm Γ (Elℙ₀ a)
unPf t = record { ∣_∣t = λ γ → liftp (∣ t ∣p γ) }

unmk : ∀{Γ a}(t : Tm Γ (Elℙ₀ a)) → unPf {Γ}{a}(mkPf t) ≡ t
unmk t = refl

mkun : ∀{Γ a}(t : Pf Γ a) → mkPf {Γ}{a}(unPf {Γ}{a} t) ≡ t
mkun t = refl
