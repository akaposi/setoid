{-# OPTIONS --without-K --prop #-}

module Model.CountableChoice where

open import lib
open import Agda.Primitive

open import Model.Decl
open import Model.Core
open import Model.Trunc
open import Model.Nat
open import Model.Func

countableChoice : {Γ : Con}(P : Ty (Γ , Nat) l0) → Tm (Γ , Π Nat (Tr P)) (Tr (Π Nat (P [ wk' {Γ}(Π Nat (Tr P)) ^ Nat ]T)))
countableChoice {Γ} P = record {
  ∣_∣t = λ { (γ ,Σ (f ,sp _)) → f ,sp λ {n}{n'} n~ → J-ℕᴿ (λ {n'} n~ → P T refC Γ γ ,p n~ ⊢ f n ~ f n') (refT P (f n)) n~ } }

I : Ty • l0
I = record
      { ∣_∣T_ = λ _ → 𝟚
      ; _T_⊢_~_ = λ _ _ _ → ⊤p
      ; coeT = λ _ x → x
      }

IChoice : (P : Ty (• , I) l0) → Tm (• , Π I (Tr P)) (Tr (Π (I [ ε ]T) (P [ wk' {•}(Π I (Tr P)) ^ I ]T)))
IChoice P = record { ∣_∣t = λ { (_ ,Σ (f ,sp _)) → f ,sp {!!} } }

¬IChoice : ((P : Ty (• , I) l0) → Tm (• , Π I (Tr P)) (Tr (Π (I [ ε ]T) (P [ wk' {•}(Π I (Tr P)) ^ I ]T)))) → ⊥
¬IChoice ch = {!∣ ch Nat ∣t !}


P : Ty (• , I) l0
P = record
      { ∣_∣T_ = λ _ → 𝟚
      ; _T_⊢_~_ = λ { {_ ,Σ tt} _ _ _ → ⊤p ; {_ ,Σ ff} _ _ _ → ⊥p }
      ; refT = {!!}
      ; symT = {!!}
      ; transT = {!!}
      ; coeT = {!!}
      ; cohT = {!!}
      }
