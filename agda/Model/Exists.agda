{-# OPTIONS --without-K --prop #-}

module Model.Exists where

open import lib
open import Agda.Primitive

open import Model.Decl
open import Model.Core
open import Model.Props0

Exists : {Γ : Con}(A : Ty Γ l0)(p : Tm (Γ , A) ℙ₀) → Tm Γ ℙ₀
Exists {Γ} A p = record {
  ∣_∣t = λ γ → ∃ (∣ A ∣T γ) λ α → ∣ p ∣t (γ ,Σ α) ;
  ~t = λ {γ γ'} γ~ → liftP ((λ { (α ,∃ β) → coeT A γ~ α ,∃ unliftp (coeT (Elℙ₀ p) (γ~ ,p cohT A γ~ α) (liftp β)) }) ,p
                            (λ { (α ,∃ β) → coeT A (symC Γ γ~) α ,∃ unliftp (coeT (Elℙ₀ p) (symC Γ γ~ ,p cohT A (symC Γ γ~) α) (liftp β)) })) }

_,Exists_ : {Γ : Con}{A : Ty Γ l0}{p : Tm (Γ , A) ℙ₀}(u : Tm Γ A)(v : Tm Γ (Elℙ₀ (p [ < u > ]t))) → Tm Γ (Elℙ₀ (Exists A p))
u ,Exists v = record { ∣_∣t = λ γ → liftp (∣ u ∣t γ ,∃ unliftp (∣ v ∣t γ)) }

proj₁Exists : {Γ : Con}{A : Ty Γ l0}{p : Tm (Γ , A) ℙ₀}(t : Tm Γ (Elℙ₀ (Exists A p))) → Tm Γ A
proj₁Exists t = record {
  ∣_∣t = λ γ → {!unliftp (∣ t ∣t γ)!} ;
  ~t = {!!} }
