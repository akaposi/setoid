{-# OPTIONS --without-K --prop #-}

module ModelR.Core where

open import lib
open import Agda.Primitive

open import ModelR.Decl

infixl 5 _▷_
infixl 7 _[_]T
infixl 5 _,_
infix  6 _∘_
infixl 8 _[_]t

-- definitions

• : Con lzero
• = record
  { ∣_∣C   = ⊤
  ; _C_~_  = λ _ _ → ⊤p
  }

_▷_ : ∀{i}(Γ : Con i){j} → Ty Γ j → Con (i ⊔ j)
_▷_ Γ A = record
  { ∣_∣C   = Σ ∣ Γ ∣C ∣ A ∣T_
  ; _C_~_  = λ { (γ ,Σ α)(γ' ,Σ α') → Σp (Γ C γ ~ γ') (A T_⊢ α ~ α') }
  ; refC   = λ { (γ ,Σ α) → refC Γ γ ,p refT A α }
  ; symC   = λ { (p ,p r) → symC Γ p ,p symT A r }
  ; transC = λ { (p ,p r)(p' ,p r') → transC Γ p p' ,p transT A r r' }
  }

_[_]T : ∀{i}{Γ : Con i}{j}{Δ : Con j}{k} → Ty Δ k → Tms Γ Δ → Ty Γ k
A [ σ ]T = record
  { ∣_∣T_   = λ γ → ∣ A ∣T (∣ σ ∣s γ)
  ; _T_⊢_~_ = λ p α α' → A T ~s σ p ⊢ α ~ α'
  ; refT    = refT A
  ; symT    = symT A
  ; transT  = transT A
  ; coeT    = λ p → coeT A (~s σ p)
  ; cohT    = λ p → cohT A (~s σ p)
  ; coeTRef = ap-p (λ z → λ {γ} → z {∣ σ ∣s γ}) (coeTRef A)
  }

id : ∀{i}{Γ : Con i} → Tms Γ Γ
id = record
  { ∣_∣s = λ γ → γ
  ; ~s   = λ p → p
  }

_∘_ : ∀{i}{Γ : Con i}{j}{Θ : Con j}{k}{Δ : Con k} → Tms Θ Δ → Tms Γ Θ → Tms Γ Δ
σ ∘ ν = record
  { ∣_∣s = λ γ → ∣ σ ∣s (∣ ν ∣s γ)
  ; ~s   = λ p → ~s σ (~s ν p)
  }

ε : ∀{i}{Γ : Con i} → Tms Γ •
ε = record
  { ∣_∣s = λ _ → tt
  }

_,_ : ∀{i}{Γ : Con i}{j}{Δ : Con j}(σ : Tms Γ Δ){b}{A : Ty Δ b} → Tm Γ (A [ σ ]T) → Tms Γ (Δ ▷ A)
σ , t = record { ∣_∣s = λ γ → ∣ σ ∣s γ ,Σ ∣ t ∣t γ ; ~s = λ p → ~s σ p ,p ~t t p }

π₁ : ∀{i}{Γ : Con i}{j}{Δ : Con j}{k}{A : Ty Δ k} → Tms Γ (Δ ▷ A) →  Tms Γ Δ
π₁ σ = record { ∣_∣s = λ γ → proj₁ (∣ σ ∣s γ) ; ~s = λ p → proj₁p (~s σ p) }

_[_]t : ∀{i}{Γ : Con i}{j}{Δ : Con j}{k}{A : Ty Δ k} → Tm Δ A → (σ : Tms Γ Δ) → Tm Γ (A [ σ ]T)
t [ σ ]t = record { ∣_∣t = λ γ → ∣ t ∣t (∣ σ ∣s γ) ; ~t = λ p → ~t t (~s σ p) }

π₂ : ∀{i}{Γ : Con i}{j}{Δ : Con j}{k}{A : Ty Δ k}(σ : Tms Γ (Δ ▷ A)) → Tm Γ (A [ π₁ {A = A} σ ]T)
π₂ σ = record { ∣_∣t = λ γ → proj₂ (∣ σ ∣s γ) ; ~t = λ p → proj₂p (~s σ p) }

[id]T : ∀{i}{Γ : Con i}{j}{A : Ty Γ j} → A [ id ]T ≡ A
[][]T : ∀{i}{Γ : Con i}{j}{Θ : Con j}{k}{Δ : Con k}{l}{A : Ty Δ l}{σ : Tms Θ Δ}{δ : Tms Γ Θ} → (A [ σ ]T [ δ ]T) ≡ (A [ σ ∘ δ ]T)
idl   : ∀{i}{Γ : Con i}{j}{Δ : Con j}{δ : Tms Γ Δ} → (id ∘ δ) ≡ δ
idr   : ∀{i}{Γ : Con i}{j}{Δ : Con j}{δ : Tms Γ Δ} → (δ ∘ id) ≡ δ
ass   : ∀{i}{Γ : Con i}{j}{Θ : Con j}{k}{Ψ : Con k}{l}{Δ : Con l}{σ : Tms Ψ Δ}{δ : Tms Θ Ψ}{ν : Tms Γ Θ} →
  ((σ ∘ δ) ∘ ν) ≡ (σ ∘ (δ ∘ ν))
,∘    : ∀{i}{Γ : Con i}{j}{Θ : Con j}{k}{Δ : Con k}{σ : Tms Θ Δ}{δ : Tms Γ Θ}{l}{A : Ty Δ l}{t : Tm Θ (A [ σ ]T)} →
  (_,_ σ {A = A} t) ∘ δ ≡ _,_ (σ ∘ δ) {A = A} (t [ δ ]t)
π₁β   : ∀{i}{Γ : Con i}{j}{Δ : Con j}{δ : Tms Γ Δ}{k}{A : Ty Δ k}{a : Tm Γ (A [ δ ]T)} → (π₁ {A = A}(_,_ δ {A = A} a)) ≡ δ
πη    : ∀{i}{Γ : Con i}{j}{Δ : Con j}{k}{A : Ty Δ k}{δ : Tms Γ (Δ ▷ A)} → (_,_ (π₁ {A = A} δ) {A = A} (π₂ {A = A} δ)) ≡ δ
εη    : ∀{i}{Γ : Con i}{σ : Tms Γ •} → σ ≡ ε
π₂β   : ∀{i}{Γ : Con i}{j}{Δ : Con j}{δ : Tms Γ Δ}{k}{A : Ty Δ k}{a : Tm Γ (A [ δ ]T)} → (π₂ {A = A}(_,_ δ {A = A} a)) ≡ a

[id]T = refl
[][]T = refl
idl   = refl
idr   = refl
ass   = refl
,∘    = refl
π₁β   = refl
πη    = refl
εη    = refl
π₂β   = refl

wk : ∀{i}{Γ : Con i}{j}{A : Ty Γ j} → Tms (Γ ▷ A) Γ
wk {A = A} = π₁ {A = A} id

vz : ∀{i}{Γ : Con i}{j}{A : Ty Γ j} → Tm (Γ ▷ A) (A [ wk {A = A} ]T)
vz {A = A} = π₂ {A = A} id

vs : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{B : Ty Γ k} → Tm Γ A → Tm (Γ ▷ B) (A [ wk {A = B} ]T) 
vs {B = B} x = x [ wk {A = B} ]t

<_> : ∀{i}{Γ : Con i}{j}{A : Ty Γ j} → Tm Γ A → Tms Γ (Γ ▷ A)
<_> {i}{Γ}{j}{A} t = _,_ id {A = A} t

infix 4 <_>

_^_ : ∀{i}{Γ : Con i}{j}{Δ : Con j}(σ : Tms Γ Δ){k}(A : Ty Δ k) → Tms (Γ ▷ A [ σ ]T) (Δ ▷ A)
_^_ {Γ}{Δ} σ {b} A = _,_ (σ ∘ wk {A = A [ σ ]T}) {A = A} (vz {A = A [ σ ]T})

infixl 5 _^_
