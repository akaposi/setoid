{-# OPTIONS --without-K --prop #-}

module ModelR.Decl where

open import lib
open import Agda.Primitive

record Con i : Set (lsuc i) where
  field
    ∣_∣C : Set i
    _C_~_ : ∣_∣C → ∣_∣C → Prop i
    refC : ∀ γ → _C_~_ γ γ
    symC : ∀{γ γ'} → _C_~_ γ γ' → _C_~_ γ' γ
    transC : ∀{γ γ' γ''} → _C_~_ γ γ' → _C_~_ γ' γ'' → _C_~_ γ γ''
  infix 4 ∣_∣C
  infix 5 _C_~_
open Con public

record Tms {i j}(Γ : Con i)(Δ : Con j) : Set (i ⊔ j) where
  field
    ∣_∣s : ∣ Γ ∣C → ∣ Δ ∣C
    ~s   : {γ γ' : ∣ Γ ∣C} → Γ C γ ~ γ' → Δ C (∣_∣s γ) ~ (∣_∣s γ')
  infix 4 ∣_∣s
open Tms public

record Ty {i}(Γ : Con i) j : Set (i ⊔ lsuc j) where
  constructor mkTy
  field
    ∣_∣T_   : ∣ Γ ∣C → Set j
    _T_⊢_~_ : ∀{γ γ'}(p : Γ C γ ~ γ') → ∣_∣T_ γ → ∣_∣T_ γ' → Prop j
    refT    : ∀{γ} α → _T_⊢_~_ (refC Γ γ) α α
    symT    : ∀{γ γ'}{p : Γ C γ ~ γ'}{α : ∣_∣T_ γ}{α' : ∣_∣T_ γ'}
            → _T_⊢_~_ p α α' → _T_⊢_~_ (symC Γ p) α' α
    transT  : ∀{γ γ' γ''}{p : Γ C γ ~ γ'}{q : Γ C γ' ~ γ''}
              {α : ∣_∣T_ γ}{α' : ∣_∣T_ γ'}{α'' : ∣_∣T_ γ''}
            → _T_⊢_~_ p α α' → _T_⊢_~_ q α' α'' → _T_⊢_~_ (transC Γ p q) α α''
    coeT    : {γ γ' : ∣ Γ ∣C} → Γ C γ ~ γ' → ∣_∣T_ γ → ∣_∣T_ γ'
    cohT    : {γ γ' : ∣ Γ ∣C}(p : Γ C γ ~ γ')(α : ∣_∣T_ γ) → _T_⊢_~_ p α (coeT p α)
    coeTRef : _≡p_ {A = {γ : ∣ Γ ∣C} → ∣_∣T_ γ → ∣_∣T_ γ}
                   (λ {γ} → coeT (refC Γ γ))
                   (λ α → α)
  infix 4 ∣_∣T_
  infix 5 _T_⊢_~_
open Ty public

record Tm {i}(Γ : Con i){j}(A : Ty Γ j) : Set (i ⊔ j) where
  field
    ∣_∣t : (γ : ∣ Γ ∣C) → ∣ A ∣T γ
    ~t   : {γ γ' : ∣ Γ ∣C}(p : Γ C γ ~ γ') → A T p ⊢ (∣_∣t γ) ~ (∣_∣t γ')
  infix 4 ∣_∣t
open Tm public

coeT* : ∀{i}{Γ : Con i}{j}(A : Ty Γ j){γ γ' : ∣ Γ ∣C} → Γ C γ ~ γ' → ∣ A ∣T γ' → ∣ A ∣T γ
coeT* {_}{Γ} A γ~ α' = coeT A (symC Γ γ~) α'

cohT* : ∀{i}{Γ : Con i}{j}(A : Ty Γ j){γ γ' : ∣ Γ ∣C}(γ~ : Γ C γ ~ γ')(α' : ∣ A ∣T γ') → A T γ~ ⊢ coeT* A γ~ α' ~ α'
cohT* {_}{Γ} A γ~ α' = symT A (cohT A (symC Γ γ~) α')

transC3 : ∀{i}(Γ : Con i){γ γ' γ'' γ''' : ∣ Γ ∣C} → Γ C γ ~ γ' → Γ C γ' ~ γ'' → Γ C γ'' ~ γ''' → Γ C γ ~ γ'''
transC3 Γ γ~ γ~' γ~'' = transC Γ γ~ (transC Γ γ~' γ~'')

transT3 : ∀{i}{Γ : Con i}{j}(A : Ty Γ j){γ γ' γ'' γ''' : ∣ Γ ∣C}
  {γ~ : Γ C γ ~ γ'}{γ~' : Γ C γ' ~ γ''}{γ~'' : Γ C γ'' ~ γ'''}
  {α : ∣ A ∣T γ}{α' : ∣ A ∣T γ'}{α'' : ∣ A ∣T γ''}{α''' : ∣ A ∣T γ'''}
  (α~ : A T γ~ ⊢ α ~ α')(α~' : A T γ~' ⊢ α' ~ α'')(α~'' : A T γ~'' ⊢ α'' ~ α''')
  → A T transC3 Γ γ~ γ~' γ~'' ⊢ α ~ α'''
transT3 A α~ α~' α~'' = transT A α~ (transT A α~' α~'')

mkTm= : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}
  {∣∣₀ ∣∣₁ : (γ : ∣ Γ ∣C) → ∣ A ∣T γ}(∣∣₂ : ∣∣₀ ≡p ∣∣₁)
  {~₀ : {γ γ' : ∣ Γ ∣C}(p : Γ C γ ~ γ') → A T p ⊢ (∣∣₀ γ) ~ (∣∣₀ γ')}
  {~₁ : {γ γ' : ∣ Γ ∣C}(p : Γ C γ ~ γ') → A T p ⊢ (∣∣₁ γ) ~ (∣∣₁ γ')} →
  _≡p_ {A = Tm Γ A} (record { ∣_∣t = ∣∣₀ ; ~t = ~₀ }) (record { ∣_∣t = ∣∣₁ ; ~t = ~₁ })
mkTm= reflp = reflp
