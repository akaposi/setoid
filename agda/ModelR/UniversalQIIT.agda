{-# OPTIONS --prop #-}

module ModelR.UniversalQIIT where

open import lib

-- Setoid of syntax
--------------------------------------------------------------------------------

infixl 7 _[_]T
infixl 5 _,ₛ_
infixr 6 _∘ₛ_
infixl 8 _[_]
infixl 3 _▶_

data Con  : Set
data Ty   : Con → Set
data Sub  : Con → Con → Set
data Tm   : (Γ : Con) → Ty Γ → Set

data Ix  : Set
data ⌞_⌟ : Ix → Prop
Con~ : Con → Con → Prop
Ty~  : ∀ {Γ₀ Γ₁} → Con~ Γ₀ Γ₁ → Ty Γ₀ → Ty Γ₁ → Prop
Sub~ : ∀ {Γ₀ Γ₁} → Con~ Γ₀ Γ₁ → ∀ {Δ₀ Δ₁} → Con~ Δ₀ Δ₁ → Sub Γ₀ Δ₀ → Sub Γ₁ Δ₁ → Prop
Tm~  : ∀ {Γ₀ Γ₁} (Γ₀₁ : Con~ Γ₀ Γ₁){A₀ A₁} → Ty~ Γ₀₁ A₀ A₁ → Tm Γ₀ A₀ → Tm Γ₁ A₁ → Prop

data Ix where
  Con~I : Con → Con → Ix
  Ty~I  : ∀ {Γ₀ Γ₁} → Con~ Γ₀ Γ₁ → Ty Γ₀ → Ty Γ₁ → Ix
  Sub~I : ∀ {Γ₀ Γ₁} → Con~ Γ₀ Γ₁ → ∀{Δ₀ Δ₁} → Con~ Δ₀ Δ₁ → Sub Γ₀ Δ₀ → Sub Γ₁ Δ₁ → Ix
  Tm~I  : ∀ {Γ₀ Γ₁} (Γ₀₁ : Con~ Γ₀ Γ₁) {A₀ A₁} → Ty~ Γ₀₁ A₀ A₁ → Tm Γ₀ A₀ → Tm Γ₁ A₁ → Ix

Con~ Γ₀ Γ₁         = ⌞ Con~I Γ₀ Γ₁ ⌟
Ty~  Γ₀₁ A₀ A₁     = ⌞ Ty~I  Γ₀₁ A₀ A₁ ⌟
Sub~ Γ₀₁ Δ₀₁ σ₀ σ₁ = ⌞ Sub~I Γ₀₁ Δ₀₁ σ₀ σ₁ ⌟
Tm~  Γ₀₁ A₀₁ t₀ t₁ = ⌞ Tm~I  Γ₀₁ A₀₁ t₀ t₁ ⌟

data Con where
  ∙   : Con
  _▶_ : (Γ : Con) → Ty Γ → Con

data Ty where
  U      : ∀ {Γ} → Ty Γ
  El     : ∀ {Γ} → Tm Γ U → Ty Γ
  Π      : ∀ {Γ}(a : Tm Γ U) → Ty (Γ ▶ El a) → Ty Γ
  _[_]T  : ∀ {Γ Δ} → Ty Δ → Sub Γ Δ → Ty Γ
  coerce : ∀ {Γ₀ Γ₁} → Con~ Γ₀ Γ₁ → Ty Γ₀ → Ty Γ₁

data Sub where
  idₛ    : ∀ {Γ} → Sub Γ Γ
  ε      : ∀ {Γ} → Sub Γ ∙
  _∘ₛ_   : ∀ {Γ Δ Σ} → Sub Δ Σ → Sub Γ Δ → Sub Γ Σ
  _,ₛ_   : ∀ {Γ Δ A} → (σ : Sub Γ Δ) → Tm Γ (A [ σ ]T) → Sub Γ (Δ ▶ A)
  π₁     : ∀ {Γ Δ A} → Sub Γ (Δ ▶ A) → Sub Γ Δ
  coerce : ∀ {Γ₀ Γ₁} (Γ₀₁ : Con~ Γ₀ Γ₁)
             {Δ₀ Δ₁} (Δ₀₁ : Con~ Δ₀ Δ₁)
             → Sub Γ₀ Δ₀ → Sub Γ₁ Δ₁

data Tm where
  π₂     : ∀ {Γ Δ A}(σ : Sub Γ (Δ ▶ A)) → Tm Γ (A [ π₁ σ ]T)
  _[_]   : ∀ {Γ Δ A}(t : Tm Δ A)(σ : Sub Γ Δ) → Tm Γ (A [ σ ]T)
  app    : ∀ {Γ a B} → Tm Γ (Π a B) → Tm (Γ ▶ El a) B
  coerce : ∀ {Γ₀ Γ₁} (Γ₀₁ : Con~ Γ₀ Γ₁)
             {A₀ A₁} (A₀₁ : Ty~ Γ₀₁ A₀ A₁) → Tm Γ₀ A₀ → Tm Γ₁ A₁

data ⌞_⌟ where
  ∙~    : Con~ ∙ ∙
  ▶~    : ∀ {Γ₀ Γ₁} Γ₀₁ {A₀ A₁} → Ty~ {Γ₀}{Γ₁} Γ₀₁ A₀ A₁ → Con~ (Γ₀ ▶ A₀) (Γ₁ ▶ A₁)

  U~    : ∀ {Γ₀ Γ₁ Γ₀₁} → Ty~ {Γ₀}{Γ₁} Γ₀₁ U U
  El~   : ∀ {Γ₀ Γ₁ Γ₀₁}{t₀ : Tm Γ₀ U}{t₁ : Tm Γ₁ U} → Tm~ Γ₀₁ U~ t₀ t₁ → Ty~ Γ₀₁ (El t₀) (El t₁)
  Π~    : ∀ {Γ₀ Γ₁ Γ₀₁ a₀ a₁} (a₀₁ : Tm~ Γ₀₁ U~ a₀ a₁) {B₀ B₁} (B₀₁ : Ty~ (▶~ Γ₀₁ (El~ a₀₁)) B₀ B₁)
           → Ty~ {Γ₀}{Γ₁} Γ₀₁ (Π a₀ B₀) (Π a₁ B₁)
  []T~  : ∀ {Γ₀ Γ₁ Γ₀₁ Δ₀ Δ₁ Δ₀₁ A₀ A₁} (A₀₁ : Ty~ {Δ₀}{Δ₁} Δ₀₁ A₀ A₁) {σ₀ σ₁}
             (σ₀₁ : Sub~ Γ₀₁ Δ₀₁ σ₀ σ₁) →
           Ty~ {Γ₀}{Γ₁} Γ₀₁ (A₀ [ σ₀ ]T) (A₁ [ σ₁ ]T)

  idₛ~  : ∀ {Γ₀ Γ₁}(Γ₀₁ : Con~ Γ₀ Γ₁) → Sub~ Γ₀₁ Γ₀₁ idₛ idₛ
  ε~    : ∀ {Γ₀ Γ₁ Γ₀₁} → Sub~ Γ₀₁ ∙~ (ε {Γ₀}) (ε {Γ₁})
  ∘ₛ~   : ∀ {Γ₀ Γ₁ Γ₀₁ Δ₀ Δ₁ Σ₀ Σ₁ Σ₀₁}{σ₀ : Sub Δ₀ Σ₀}{σ₁ : Sub Δ₁ Σ₁}{δ₀ δ₁}
          → Sub~ {Γ₀}{Γ₁} Γ₀₁ {Σ₀}{Σ₁} Σ₀₁ (σ₀ ∘ₛ δ₀) (σ₁ ∘ₛ δ₁)
  ,ₛ~   : ∀ {Γ₀ Γ₁ Γ₀₁ Δ₀ Δ₁ Δ₀₁ A₀ A₁ A₀₁ σ₀ σ₁} (σ₀₁ : Sub~ Γ₀₁ Δ₀₁ σ₀ σ₁) {t₀ t₁}
            (t₀₁ : Tm~ Γ₀₁ ([]T~ A₀₁ σ₀₁) t₀ t₁)
          → Sub~ {Γ₀}{Γ₁} Γ₀₁ {Δ₀ ▶ A₀}{Δ₁ ▶ A₁} (▶~ Δ₀₁ A₀₁) (σ₀ ,ₛ t₀) (σ₁ ,ₛ t₁)
  π₁~   : ∀ {Γ₀ Γ₁ Γ₀₁ Δ₀ Δ₁ Δ₀₁ A₀ A₁ A₀₁ σ₀ σ₁} (σ₀₁ : Sub~ Γ₀₁ (▶~ Δ₀₁ {A₀}{A₁} A₀₁) σ₀ σ₁)
          → Sub~ {Γ₀}{Γ₁} Γ₀₁ {Δ₀}{Δ₁} Δ₀₁ (π₁ σ₀) (π₁ σ₁)

  π₂~   : ∀ {Γ₀ Γ₁ Γ₀₁ Δ₀ Δ₁ Δ₀₁ A₀ A₁ A₀₁ σ₀ σ₁} (σ₀₁ : Sub~ {Γ₀}{Γ₁}Γ₀₁ (▶~ {Δ₀}{Δ₁} Δ₀₁ {A₀}{A₁} A₀₁) σ₀ σ₁)
        → Tm~ Γ₀₁ ([]T~ A₀₁ (π₁~ {A₀₁ = A₀₁} σ₀₁)) (π₂ σ₀) (π₂ σ₁)
  []~   : ∀ {Γ₀ Γ₁ Γ₀₁ Δ₀ Δ₁ Δ₀₁ A₀ A₁ A₀₁ t₀ t₁}(t₀₁ : Tm~ {Δ₀}{Δ₁} Δ₀₁ {A₀}{A₁} A₀₁ t₀ t₁){σ₀ σ₁}
        → (σ₀₁ : Sub~ {Γ₀}{Γ₁} Γ₀₁ Δ₀₁ σ₀ σ₁)
        → Tm~ Γ₀₁ ([]T~ A₀₁ σ₀₁) (t₀ [ σ₀ ]) (t₁ [ σ₁ ])
  app~  : ∀ {Γ₀ Γ₁ Γ₀₁ a₀ a₁ a₀₁ B₀ B₁ B₀₁ t₀ t₁}
           (t₀₁ : Tm~ {Γ₀}{Γ₁} Γ₀₁ (Π~ a₀₁ {B₀}{B₁} B₀₁) t₀ t₁)
        → Tm~ (▶~ Γ₀₁ {El a₀}{El a₁} (El~ a₀₁)) B₀₁ (app t₀) (app t₁)

  rflC~ : ∀ {Γ} → Con~ Γ Γ
  symC~ : ∀ {Γ₀ Γ₁} → Con~ Γ₀ Γ₁ → Con~ Γ₁ Γ₀
  trsC~ : ∀ {Γ₀ Γ₁ Γ₂} → Con~ Γ₀ Γ₁ → Con~ Γ₁ Γ₂ → Con~ Γ₀ Γ₂

  cohT  : ∀ {Γ₀ Γ₁}(Γ₀₁ : Con~ Γ₀ Γ₁)(A : Ty Γ₀) → Ty~ Γ₀₁ A (coerce Γ₀₁ A)
  rflT~ : ∀ {Γ A} → Ty~ {Γ}{Γ} rflC~ A A
  symT~ : ∀ {Γ₀ Γ₁ Γ₀₁ A₀ A₁} → Ty~ {Γ₀}{Γ₁} Γ₀₁ A₀ A₁ → Ty~ (symC~ Γ₀₁) A₁ A₀
  trsT~ : ∀ {Γ₀ Γ₁ Γ₂ Γ₀₁ Γ₁₂}{A₀ A₁ A₂} → Ty~ {Γ₀}{Γ₁} Γ₀₁ A₀ A₁ → Ty~ {Γ₁}{Γ₂} Γ₁₂ A₁ A₂
          → Ty~ (trsC~ Γ₀₁ Γ₁₂) A₀ A₂

  cohs  : ∀ {Γ₀ Γ₁} Γ₀₁ {Δ₀ Δ₁} Δ₀₁ (σ : Sub Γ₀ Δ₀) → Sub~ {Γ₀}{Γ₁} Γ₀₁ {Δ₀}{Δ₁} Δ₀₁ σ (coerce Γ₀₁ Δ₀₁ σ)
  rfls~ : ∀ {Γ Δ}{σ : Sub Γ Δ} → Sub~ rflC~ rflC~ σ σ
  syms~ : ∀ {Γ₀ Γ₁ Γ₀₁ Δ₀ Δ₁ Δ₀₁ σ₀ σ₁} → Sub~ {Γ₀}{Γ₁} Γ₀₁ {Δ₀}{Δ₁} Δ₀₁ σ₀ σ₁ →
                                          Sub~ (symC~ Γ₀₁) (symC~ Δ₀₁) σ₁ σ₀
  trss~ : ∀ {Γ₀ Γ₁ Γ₂ Γ₀₁ Γ₁₂ Δ₀ Δ₁ Δ₂ Δ₀₁ Δ₁₂ σ₀ σ₁ σ₂} → Sub~ {Γ₀}{Γ₁} Γ₀₁ {Δ₀}{Δ₁} Δ₀₁ σ₀ σ₁
          → Sub~ {Γ₁}{Γ₂}Γ₁₂ {Δ₁}{Δ₂} Δ₁₂ σ₁ σ₂
          → Sub~ (trsC~ Γ₀₁ Γ₁₂) (trsC~ Δ₀₁ Δ₁₂) σ₀ σ₂

  coht   : ∀ {Γ₀ Γ₁} Γ₀₁ {A₀ A₁} A₀₁ (t : Tm Γ₀ A₀) → Tm~ {Γ₀}{Γ₁} Γ₀₁ {A₀}{A₁} A₀₁ t (coerce Γ₀₁ A₀₁ t)
  rflt~  : ∀ {Γ A t} → Tm~ {Γ} rflC~ {A} rflT~ t t
  symt~  : ∀ {Γ₀ Γ₁ Γ₀₁ A₀ A₁ A₀₁ t₀ t₁}(t₀₁ : Tm~ {Γ₀}{Γ₁} Γ₀₁ {A₀}{A₁} A₀₁ t₀ t₁)
        → Tm~ (symC~ Γ₀₁) (symT~ A₀₁) t₁ t₀
  trst~  : ∀ {Γ₀ Γ₁ Γ₂ Γ₀₁ Γ₁₂ A₀ A₁ A₂ A₀₁ A₁₂ t₀ t₁ t₂}
         → Tm~ {Γ₀}{Γ₁} Γ₀₁ {A₀}{A₁} A₀₁ t₀ t₁
         → Tm~ {Γ₁}{Γ₂} Γ₁₂ {A₁}{A₂} A₁₂ t₁ t₂
         → Tm~ (trsC~ Γ₀₁ Γ₁₂) (trsT~  A₀₁ A₁₂) t₀ t₂

  U[]   : ∀ {Γ Δ}{σ : Sub Γ Δ} → Ty~ rflC~ (U [ σ ]T) U
  El[]  : ∀ {Γ Δ}{σ : Sub Γ Δ}{a : Tm Δ U}
          → Ty~ rflC~ (El a [ σ ]T) (El (coerce rflC~ U[] ((a [ σ ]))))

  [id]  : ∀ {Γ A} → Ty~ rflC~ (A [ idₛ {Γ} ]T) A
  [∘]   : ∀ {Γ Δ Σ}(A : Ty Σ) (σ : Sub Δ Σ) (δ : Sub Γ Δ)
          → Ty~ rflC~ (A [ σ ]T [ δ ]T) (A [ σ ∘ₛ δ ]T)

  Π[]   : ∀ {Γ Δ a B}{σ : Sub Γ Δ}
          → Ty~ rflC~
          (Π a B [ σ ]T)
          (Π (coerce rflC~ U[] (a [ σ ]))
             (B [ σ ∘ₛ π₁ idₛ ,ₛ
                  coerce rflC~ (trsT~ ([]T~ (symT~ El[]) rfls~) ([∘] _ _ _))
                  (π₂ (idₛ {Γ ▶ El (coerce rflC~ U[] (a [ σ ]))})) ]T))

  idl   : ∀ {Γ Δ}{σ : Sub Γ Δ} → Sub~ rflC~ rflC~ (idₛ ∘ₛ σ) σ
  idr   : ∀ {Γ Δ}{σ : Sub Γ Δ} → Sub~ rflC~ rflC~ (σ ∘ₛ idₛ) σ
  ass   : ∀ {Γ Δ Σ Ξ}{σ : Sub Σ Ξ}{δ : Sub Δ Σ}{ν : Sub Γ Δ}
          → Sub~ rflC~ rflC~ ((σ ∘ₛ δ) ∘ₛ ν) (σ ∘ₛ δ ∘ₛ ν)

  π₁β   : ∀ {Γ Δ A}{σ : Sub Γ Δ}{t : Tm Γ (A [ σ ]T)} → Sub~ rflC~ rflC~ (π₁ (σ ,ₛ t)) σ
  εη    : ∀ {Γ}{σ : Sub Γ ∙} → Sub~ rflC~ rflC~ σ ε
  πη    : ∀{Γ Δ}{A : Ty Δ}{σ : Sub Γ (Δ ▶ A)} → Sub~ rflC~ rflC~ (π₁ σ ,ₛ π₂ σ) σ
  ,∘    : ∀{Γ Δ Σ}{δ : Sub Γ Δ}{σ : Sub Σ Γ}{A : Ty Δ}{t : Tm Γ (A [ δ ]T)}
          → Sub~ rflC~ rflC~ ((δ ,ₛ t) ∘ₛ σ) ((δ ∘ₛ σ) ,ₛ coerce rflC~ ([∘] A δ σ) (t [ σ ]))

  π₂β   : ∀ {Γ Δ A}{σ : Sub Γ Δ}{t : Tm Γ (A [ σ ]T)}
        → Tm~ rflC~ ([]T~ rflT~ π₁β) (π₂ (σ ,ₛ t)) t
  app[] : ∀{Γ Δ a}{σ : Sub Γ Δ}{B : Ty (Δ ▶ El a)}{t : Tm Δ (Π a B)}
          → Tm~ rflC~ rflT~ (app t [ σ ∘ₛ π₁ idₛ ,ₛ
                coerce rflC~ (trsT~ ([]T~ (symT~ El[]) rfls~) ([∘] _ _ _)) (π₂ idₛ) ])
                          (app (coerce rflC~ Π[] (t [ σ ])))

--------------------------------------------------------------------------------

open import Agda.Primitive
import ModelR.Decl as M
import ModelR.Core as M

Conᵀ : Set₁
Conᵀ = M.Ty M.• lzero

Conˢ : Conᵀ
M.∣ Conˢ ∣T    _ = Con
M._T_⊢_~_ Conˢ _ = Con~
M.refT Conˢ    _ = rflC~
M.symT Conˢ      = symC~
M.transT Conˢ    = trsC~
M.coeT Conˢ _ Γˢ = Γˢ
M.cohT Conˢ _ _  = rflC~
M.coeTRef Conˢ   = reflp

Tyᵀ : Set₁
Tyᵀ = M.Ty (M.• M.▷ Γᵀ) lzero where
  Γᵀ = Conˢ

Tyˢ : Tyᵀ
M.∣ Tyˢ ∣T     _ ,Σ Γˢ   = Ty  Γˢ
M._T_⊢_~_ Tyˢ (_ ,p Γˢ~) = Ty~ Γˢ~
M.refT Tyˢ _ = rflT~
M.symT Tyˢ   = symT~
M.transT Tyˢ = trsT~
M.coeT Tyˢ (_ ,p Γˢ~) = coerce Γˢ~
M.cohT Tyˢ (_ ,p Γˢ~) = cohT   Γˢ~
M.coeTRef Tyˢ = {!!}

Subᵀ : Set₁
Subᵀ = M.Ty (M.• M.▷ Γᵀ M.▷ Δᵀ) lzero where
  Γᵀ = Conˢ
  Δᵀ = Conˢ M.[ M.ε ]T

Subˢ : Subᵀ
M.∣ Subˢ ∣T     _ ,Σ Γˢ  ,Σ Δˢ   = Sub  Γˢ  Δˢ
M._T_⊢_~_ Subˢ (_ ,p Γˢ~ ,p Δˢ~) = Sub~ Γˢ~ Δˢ~
M.refT Subˢ _ = rfls~
M.symT Subˢ   = syms~
M.transT Subˢ = trss~
M.coeT Subˢ (_ ,p Γˢ~ ,p Δˢ~) = coerce Γˢ~ Δˢ~
M.cohT Subˢ (_ ,p Γˢ~ ,p Δˢ~) = cohs   Γˢ~ Δˢ~
M.coeTRef Subˢ = {!!}

Tmᵀ : Set₁
Tmᵀ = M.Ty (M.• M.▷ Γᵀ M.▷ Aᵀ) lzero where
  Γᵀ = Conˢ
  Aᵀ = Tyˢ

Tmˢ : Tmᵀ
M.∣ Tmˢ ∣T     _ ,Σ Γˢ  ,Σ Aˢ   = Tm  Γˢ  Aˢ
M._T_⊢_~_ Tmˢ (_ ,p Γˢ~ ,p Aˢ~) = Tm~ Γˢ~ Aˢ~
M.refT Tmˢ _ = rflt~
M.symT Tmˢ   = symt~
M.transT Tmˢ = trst~
M.coeT Tmˢ (_ ,p Γˢ~ ,p Aˢ~) = coerce Γˢ~ Aˢ~
M.cohT Tmˢ (_ ,p Γˢ~ ,p Aˢ~) = coht   Γˢ~ Aˢ~
M.coeTRef Tmˢ = {!!}

∙ᵀ : Set
∙ᵀ = M.Tm M.• T where
  T = Conˢ

∙ˢ : ∙ᵀ
M.∣ ∙ˢ ∣t _ = ∙
M.~t ∙ˢ   _ = ∙~

▶ᵀ : Set
▶ᵀ = M.Tm (M.• M.▷ Γᵀ M.▷ Aᵀ) T where
  Γᵀ = Conˢ
  Aᵀ = Tyˢ
  T  = Conˢ M.[ M.ε ]T

▶ˢ : ▶ᵀ
M.∣ ▶ˢ ∣t (_ ,Σ Γˢ  ,Σ Aˢ)  = Γˢ ▶ Aˢ
M.~t ▶ˢ   (_ ,p Γˢ~ ,p Aˢ~) = ▶~ Γˢ~ Aˢ~

Uᵀ : Set
Uᵀ = M.Tm (M.• M.▷ Γᵀ) T where
  Γᵀ = Conˢ
  T  = Tyˢ

Uˢ : Uᵀ
M.∣ Uˢ ∣t _ = U
M.~t Uˢ   _ = U~

Elᵀ : Set
Elᵀ = M.Tm (M.• M.▷ Γᵀ M.▷ aᵀ) T where
  Γᵀ = Conˢ
  aᵀ = Tmˢ M.[ M.< Uˢ > ]T
  T  = Tyˢ M.[ M.wk {A = aᵀ} ]T

Elˢ : Elᵀ
M.∣ Elˢ ∣t (_ ,Σ Aˢ)  = El  Aˢ
M.~t Elˢ   (_ ,p Aˢ~) = El~ Aˢ~

Πᵀ : Set
Πᵀ = M.Tm (M.• M.▷ Γᵀ M.▷ aᵀ M.▷ Bᵀ) T where
  Γᵀ = Conˢ
  aᵀ = Tmˢ M.[ M.< Uˢ > ]T
  Bᵀ = Tyˢ M.[ M._,_ M.ε {A = Conˢ} (▶ˢ M.[ M._,_ (M.wk {A = aᵀ}) {A = Tyˢ} Elˢ ]t) ]T
  T  = Tyˢ M.[ M.wk {A = aᵀ} M.∘ M.wk {A = Bᵀ} ]T

Πˢ : Πᵀ
M.∣ Πˢ ∣t (_ ,Σ aˢ  ,Σ Bˢ)  = Π  aˢ  Bˢ
M.~t Πˢ   (_ ,p aˢ~ ,p Bˢ~) = Π~ aˢ~ Bˢ~

[]Tᵀ : Set
[]Tᵀ = M.Tm (M.• M.▷ Γᵀ M.▷ Δᵀ M.▷ Aᵀ M.▷ σᵀ) T where
  Γᵀ = Conˢ
  Δᵀ = Conˢ M.[ M.ε ]T
  Aᵀ = Tyˢ  M.[ M.ε {Γ = M.• M.▷ Γᵀ} M.^ Conˢ ]T
  σᵀ = Subˢ M.[ M.wk {A = Aᵀ} ]T
  T  = Tyˢ  M.[ M.wk {A = Δᵀ} M.∘ (M.wk {A = Aᵀ} M.∘ M.wk {A = σᵀ}) ]T

[]Tˢ : []Tᵀ
M.∣ []Tˢ ∣t (_ ,Σ Aˢ  ,Σ σˢ)  = Aˢ [ σˢ ]T
M.~t []Tˢ   (_ ,p Aˢ~ ,p σˢ~) = []T~ Aˢ~ σˢ~

idᵀ : Set
idᵀ = M.Tm (M.• M.▷ Γᵀ) T where
  Γᵀ = Conˢ
  T  = Subˢ M.[ M.< M.vz {A = Γᵀ} > ]T

idˢ : idᵀ
M.∣ idˢ ∣t _ = idₛ
M.~t idˢ   _ = idₛ~ _

εᵀ : Set
εᵀ = M.Tm (M.• M.▷ Γᵀ) T where
  Γᵀ = Conˢ
  T  = Subˢ M.[ M.< ∙ˢ M.[ M.ε ]t > ]T

εˢ : εᵀ
M.∣ εˢ ∣t _ = ε
M.~t εˢ   _ = ε~

∘ᵀ : Set
∘ᵀ = M.Tm (M.• M.▷ Γᵀ M.▷ Δᵀ M.▷ Σᵀ M.▷ σᵀ M.▷ δᵀ) T where
  Γᵀ = Conˢ
  Δᵀ = Conˢ M.[ M.ε ]T
  Σᵀ = Conˢ M.[ M.ε ]T
  σᵀ = Subˢ M.[ M.ε {Γ = M.• M.▷ Γᵀ} M.^ Conˢ M.^ Conˢ M.[ M.ε ]T ]T
  δᵀ = Subˢ M.[ M.wk {A = Σᵀ} M.∘ M.wk {A = σᵀ} ]T
  T  = Subˢ M.[ (M.wk {A = Δᵀ} M.^ Conˢ M.[ M.ε ]T)
                M.∘ (M.wk {A = σᵀ} M.∘ M.wk {A = δᵀ}) ]T

∘ˢ : ∘ᵀ
M.∣ ∘ˢ ∣t (_ ,Σ σˢ  ,Σ δˢ)  = σˢ ∘ₛ δˢ
M.~t ∘ˢ   (_ ,p σˢ~ ,p δˢ~) = ∘ₛ~

,ᵀ : Set
,ᵀ = M.Tm (M.• M.▷ Γᵀ M.▷ Δᵀ M.▷ Aᵀ M.▷ σᵀ M.▷ tᵀ) T where
  Γᵀ = Conˢ
  Δᵀ = Conˢ M.[ M.ε ]T
  Aᵀ = Tyˢ  M.[ M.ε {Γ = M.• M.▷ Conˢ} M.^ Conˢ ]T
  σᵀ = Subˢ M.[ M.wk {A = Aᵀ} ]T
  tᵀ = Tmˢ  M.[ M._,_ (M.wk {A = Δᵀ} M.∘ (M.wk {A = Aᵀ} M.∘ M.wk {A = σᵀ})) {A = Tyˢ} []Tˢ ]T
  T  = Subˢ M.[ M._,_ (M.wk {A = Δᵀ} M.∘ M.wk {A = Aᵀ})
                      {A = Conˢ M.[ M.ε ]T}
                      (▶ˢ M.[ M.ε {Γ = M.• M.▷ Γᵀ} M.^ Conˢ M.^ Tyˢ ]t)
                M.∘ (M.wk {A = σᵀ} M.∘ M.wk {A = tᵀ}) ]T

,ˢ : ,ᵀ
M.∣ ,ˢ ∣t (_ ,Σ σˢ ,Σ tˢ) = σˢ ,ₛ tˢ
M.~t ,ˢ (_ ,p Aˢ~ ,p σˢ~ ,p tˢ~) = ,ₛ~ {A₀₁ = Aˢ~} σˢ~ tˢ~

π₁ᵀ : Set
π₁ᵀ = M.Tm (M.• M.▷ Γᵀ M.▷ Δᵀ M.▷ Aᵀ M.▷ σᵀ) T where
  Γᵀ = Conˢ
  Δᵀ = Conˢ M.[ M.ε ]T
  Aᵀ = Tyˢ  M.[ M.ε {Γ = M.• M.▷ Γᵀ} M.^ Conˢ ]T
  σᵀ = Subˢ M.[ M._,_ (M.wk {A = Δᵀ} M.∘ M.wk {A = Aᵀ})
                      {A = Conˢ M.[ M.ε ]T}
                      (▶ˢ M.[ M.ε {Γ = M.• M.▷ Γᵀ} M.^ Conˢ M.^ Tyˢ ]t) ]T
  T  = Subˢ M.[ M.wk {A = Aᵀ} M.∘ M.wk {A = σᵀ} ]T

π₁ˢ : π₁ᵀ
M.∣ π₁ˢ ∣t (_ ,Σ σˢ) = π₁ σˢ
M.~t π₁ˢ (_ ,p Aˢ~ ,p σˢ~) = π₁~ {A₀₁ = Aˢ~} σˢ~

π₂ᵀ : Set
π₂ᵀ = M.Tm (M.• M.▷ Γᵀ M.▷ Δᵀ M.▷ Aᵀ M.▷ σᵀ) T where
  Γᵀ = Conˢ
  Δᵀ = Conˢ M.[ M.ε ]T
  Aᵀ = Tyˢ M.[ M.ε {Γ = M.• M.▷ Γᵀ} M.^ Conˢ ]T
  σᵀ = Subˢ M.[ M._,_ (M.wk {A = Δᵀ} M.∘ M.wk {A = Aᵀ})
                      {A = Conˢ M.[ M.ε ]T}
                      (▶ˢ M.[ M.ε {Γ = M.• M.▷ Γᵀ} M.^ Conˢ M.^ Tyˢ ]t) ]T
  T  = Tmˢ M.[ M._,_ (M.wk {A = Δᵀ} M.∘ (M.wk {A = Aᵀ} M.∘ M.wk {A = σᵀ}))
                     {A = Tyˢ}
                     ([]Tˢ M.[ M._,_ (M.wk {A = σᵀ})
                                     {A = Subˢ M.[ M.wk {A = Aᵀ} ]T}
                                     π₁ˢ ]t) ]T

π₂ˢ : π₂ᵀ
M.∣ π₂ˢ ∣t (_ ,Σ σˢ) = π₂ σˢ
M.~t π₂ˢ (_ ,p Aˢ~ ,p σˢ~) = π₂~ {A₀₁ = Aˢ~} σˢ~

[]ᵀ : Set
[]ᵀ = M.Tm (M.• M.▷ Γᵀ M.▷ Δᵀ M.▷ Aᵀ M.▷ tᵀ M.▷ σᵀ) T where
  Γᵀ = Conˢ
  Δᵀ = Conˢ M.[ M.ε ]T
  Aᵀ = Tyˢ  M.[ M.ε {Γ = M.• M.▷ Γᵀ} M.^ Conˢ ]T
  tᵀ = Tmˢ  M.[ M.ε {Γ = M.• M.▷ Γᵀ} M.^ Conˢ M.^ Tyˢ ]T
  σᵀ = Subˢ M.[ M.wk {A = Aᵀ} M.∘ M.wk {A = tᵀ} ]T
  T  = Tmˢ  M.[ M._,_ (M.wk {A = Δᵀ} M.∘ (M.wk {A = Aᵀ} M.∘ (M.wk {A = tᵀ} M.∘ M.wk {A = σᵀ})))
                      {A = Tyˢ}
                      ([]Tˢ M.[ M.wk {A = tᵀ} M.^ Subˢ M.[ M.wk {A = Aᵀ} ]T ]t) ]T

[]ˢ : []ᵀ
M.∣ []ˢ ∣t (_ ,Σ tˢ  ,Σ σˢ)  = tˢ [ σˢ ]
M.~t []ˢ   (_ ,p tˢ~ ,p σˢ~) = []~ tˢ~ σˢ~

appᵀ : Set
appᵀ = M.Tm (M.• M.▷ Γᵀ M.▷ aᵀ M.▷ Bᵀ M.▷ tᵀ) T where
  Γᵀ = Conˢ
  aᵀ = Tmˢ M.[ M.< Uˢ > ]T
  Bᵀ = Tyˢ M.[ M._,_ M.ε {A = Conˢ} (▶ˢ M.[ M._,_ (M.wk {A = aᵀ}) {A = Tyˢ} Elˢ ]t) ]T
  tᵀ = Tmˢ M.[ M._,_ (M.wk {A = aᵀ} M.∘ M.wk {A = Bᵀ}) {A = Tyˢ} Πˢ ]T
  T  = Tmˢ M.[ M._,_ (M._,_ M.ε {A = Conˢ} (▶ˢ M.[ M._,_ (M.wk {A = aᵀ}) {A = Tyˢ} Elˢ ]t)
                      M.∘ M.wk {A = Bᵀ})
                     {A = Tyˢ} (M.vz {A = Bᵀ})
               M.∘ M.wk {A = tᵀ} ]T

appˢ : appᵀ
M.∣ appˢ ∣t (_ ,Σ tˢ) = app tˢ
M.~t appˢ (_ ,p aˢ~ ,p Bˢ~ ,p tˢ~) = app~ {a₀₁ = aˢ~}{B₀₁ = Bˢ~} tˢ~

import ModelR.Iden as M

-- U[]ˢ : ∀{Γˢ Δˢ σˢ} → M.Tm Γˢ (M.Id ([]Tˢ M.[ M.id M., Γˢ M., Δˢ M., Uˢ M., σˢ ]t) Uˢ)
-- U[]ˢ = ?

-- 13 more equalities

-- recursor: we could use this:
-- Δ : M.Con, Δ has length 32
-- Δ = ∙ ▷ con : U ▷ ty : con ⇒ U ▷ sub : con ⇒ con ⇒ U ▷ tm : Π (x:con) (ty [ x ] ⇒ U ) ▷ empty : con ▷ ....
-- recCon : M.Tm Δ (Conˢ ⇒ El v³¹)
-- recTy  : M.Tm Δ (Π (Γˢ : Conˢ) (Tyˢ [ Γˢ ] ⇒ (El v³⁰ $ recCon $ Γˢ) ))
-- recSub
-- recTm
-- rec∙ : recCon $ ∙ = v²⁷
-- ...

-- but instead, do it this way:
{-
module _
  (Γ : M.Con) -- instead of this, one could use the ∙ empty context
  (Conᴿ : M.Ty Γ)
  (Tyᴿ  : M.Ty (Γ M.▷ Conᴿ))
  ...
  (∙ᴿ : M.Tm Γ Conᴿ)
  ...
  where

    recCon : M.Tm (Γ M.▷ Conˢ) (Conᴿ M.[ M.wk ]T)
    recTy  : ...
    rec...
    rec∙ : M.Tm (Id (recCon [ ∙ˢ ]) ∙ᴿ)
    ...
-}
