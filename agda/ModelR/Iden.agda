{-# OPTIONS --without-K --prop #-}

module ModelR.Iden where

open import lib

open import ModelR.Decl
open import ModelR.Core

Id : ∀{i}{Γ : Con i}{j}{A : Ty Γ j} → Tm Γ A → Tm Γ A → Ty Γ j
Id {i}{Γ}{j}{A} t u = record
  { ∣_∣T_ = λ γ → Liftp (A T refC Γ γ ⊢ ∣ t ∣t γ ~ ∣ u ∣t γ)
  ; _T_⊢_~_ = λ _ _ _ → LiftP ⊤p
  ; coeT = λ { {γ}{γ'} γ~ (liftp α~) → liftp (transT A (symT A (~t t γ~)) (transT A
                                                       α~
                                                       (~t u γ~))) }
  ; coeTRef = reflp
  }

Id[] : ∀{i}{Γ : Con i}{j}{Θ : Con j}{k}{A : Ty Γ k}{u v : Tm Γ A}{σ : Tms Θ Γ}
     → Id u v [ σ ]T ≡ Id (u [ σ ]t) (v [ σ ]t)
Id[] = refl

ref : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}(u : Tm Γ A) → Tm Γ (Id u u)
ref {i}{Γ}{j}{A} u = record { ∣_∣t = λ γ → liftp (~t u (refC Γ γ)) }

ref[] : ∀{i}{Γ : Con i}{j}{Θ : Con j}{k}{A : Ty Γ k}{u : Tm Γ A}{σ : Tms Θ Γ} → ref u [ σ ]t ≡ ref (u [ σ ]t)
ref[] = refl

transp : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}(P : Ty (Γ ▷ A) k){t u : Tm Γ A}(e : Tm Γ (Id t u)) →
  Tm Γ (P [ < t > ]T) → Tm Γ (P [ < u > ]T)
transp {i}{Γ}{j}{A} P {t}{u} e w = record
  { ∣_∣t = λ γ → coeT P (refC Γ γ ,p unliftp (∣ e ∣t γ)) (∣ w ∣t γ )
  ; ~t = λ {γ}{γ'} γ~ → transT P (symT P (cohT P (refC Γ γ ,p unliftp (∣ e ∣t γ)) (∣ w ∣t γ))) (transT P (~t w γ~) (cohT P (refC Γ γ' ,p unliftp (∣ e ∣t γ')) (∣ w ∣t γ')))
  }
transp[] : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{P : Ty (Γ ▷ A) k}{t u : Tm Γ A}{e : Tm Γ (Id t u)}
  {w : Tm Γ (P [ < t > ]T)}{l}{Θ : Con l}{σ : Tms Θ Γ} →
  _[_]t {A = P [ <_> {A = A} u ]T} (transp P {t}{u} e w) σ ≡
  transp (P [ σ ^ A ]T) {t [ σ ]t}{u [ σ ]t}(_[_]t e σ) (_[_]t {A = P [ < t > ]T} w σ)
transp[] = refl

-- strong β rules

transpβ : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{P : Ty (Γ ▷ A) k}{t : Tm Γ A}{w : Tm Γ (P [ < t > ]T)} → transp P {t}{t} (ref t) w ≡p w
transpβ {i}{Γ}{j}{A}{k}{P}{t}{w} = mkTm= (ap-p (λ z → λ γ → z  (∣ w ∣t γ)) (coeTRef P)){~t (transp P {t}{t} (ref t) w)}{~t w}
