{-# OPTIONS --without-K --prop #-}

module ModelR.Func where

open import lib
open import Agda.Primitive

open import ModelR.Decl
open import ModelR.Core

module nondependent {i}{Γ : Con i}{j}(A : Ty Γ j){k}(B : Ty Γ k) where
  ∣Π∣ : ∣ Γ ∣C → Set (j ⊔ k)
  ∣Π∣ γ = Σsp (∣ A ∣T γ → ∣ B ∣T γ) λ f →
              (α α' : ∣ A ∣T γ)(α~ : A T refC Γ γ ⊢ α ~ α') → B T refC Γ γ ⊢ f α ~ f α'

  Fun : Ty Γ (j ⊔ k)
  Fun = mkTy
    ∣Π∣
    (λ {γ}{γ'} γ~ f f' → (α : ∣ A ∣T γ)(α' : ∣ A ∣T γ')(α~ : A T γ~ ⊢ α ~ α') → B T γ~ ⊢ proj₁sp f α ~ proj₁sp f' α')
    proj₂sp
    (λ f~ α α' α~ → symT B (f~ _ _ (symT A α~)))
    (λ {_}{_}{_}{γ~} f~ f~' α α' α~ → transT B (f~ _ _ (cohT A γ~ α)) (f~' _ _ (transT A (symT A (cohT A γ~ α)) α~)))
    (λ γ~ f → (λ α' → coeT B γ~ (proj₁sp f (coeT A (symC Γ γ~) α'))) ,sp
              λ α α' α~ → transT3 B
                                  (symT B (cohT B γ~ (proj₁sp f (coeT* A γ~ α))))
                                  (proj₂sp f _ _ (transT3 A (cohT* A γ~ α) α~ (cohT A (symC Γ γ~) α')))
                                  (cohT B γ~ (proj₁sp f (coeT* A γ~ α'))))
    (λ {γ}{γ'} γ~ f α α' α~ → transT B (proj₂sp f _ _ (transT A α~ (cohT A (symC Γ γ~) α'))) (cohT B γ~ (proj₁sp f (coeT A (symC Γ γ~) α'))))
    (J-p
       {A = {γ : ∣ Γ ∣C} → ∣ B ∣T γ → ∣ B ∣T γ}
       {λ {γ} → coeT B (refC Γ γ)}
       (λ {z} e →
         _≡p_
           {A = {γ : ∣ Γ ∣C} → ∣Π∣ γ → ∣Π∣ γ}
           (λ {γ} f → (λ α' → coeT B (refC Γ γ) (proj₁sp f (coeT A (symC Γ (refC Γ γ)) α'))) ,sp
                      (λ α α' α~ → transT3 B (symT B (cohT B (refC Γ γ) (proj₁sp f (coeT* A (refC Γ γ) α))))
                                             (proj₂sp f _ _ (transT A (cohT* A (refC Γ γ) α) (transT A α~ (cohT A (symC Γ (refC Γ γ)) α'))))
                                             (cohT B (refC Γ γ) (proj₁sp f (coeT* A (refC Γ γ) α')))))
           (λ {γ} f → (λ α' → z (proj₁sp f α')) ,sp
                      (λ α α' α~ → tr-p (λ z → B T refC Γ γ ⊢ z (proj₁sp f α) ~ z (proj₁sp f α'))
                                        e
                                        (transT3 B (symT B (cohT B (refC Γ γ) (proj₁sp f α))) (proj₂sp f _ _ α~) (cohT B (refC Γ γ) (proj₁sp f α'))))))
       (J-p
          {A = {γ : ∣ Γ ∣C} → ∣ A ∣T γ → ∣ A ∣T γ}
          {λ {γ} → coeT A (refC Γ γ)}
          (λ {z} e →
             _≡p_
               {A = {γ : ∣ Γ ∣C} → ∣Π∣ γ → ∣Π∣ γ}
               (λ {γ} f → (λ α' → coeT B (refC Γ γ) (proj₁sp f (coeT A (symC Γ (refC Γ γ)) α'))) ,sp
                          (λ α α' α~ → transT3 B (symT B (cohT B (refC Γ γ) (proj₁sp f (coeT* A (refC Γ γ) α))))
                                                 (proj₂sp f _ _ (transT A (cohT* A (refC Γ γ) α) (transT A α~ (cohT A (symC Γ (refC Γ γ)) α'))))
                                                 (cohT B (refC Γ γ) (proj₁sp f (coeT* A (refC Γ γ) α')))))
               (λ {γ} f → (λ α' → coeT B (refC Γ γ) (proj₁sp f (z α'))) ,sp
                          (λ α α' α~ → tr-p (λ z → B T refC Γ γ ⊢ coeT B (refC Γ γ) (proj₁sp f (z α)) ~ coeT B (refC Γ γ) (proj₁sp f (z α')))
                                            e
                                            (transT3 B (symT B (cohT B (refC Γ γ) (proj₁sp f (coeT* A (refC Γ γ) α))))
                                                       (proj₂sp f _ _ (transT A (cohT* A (refC Γ γ) α) (transT A α~ (cohT A (symC Γ (refC Γ γ)) α'))))
                                                       (cohT B (refC Γ γ) (proj₁sp f (coeT* A (refC Γ γ) α')))))))
          reflp
          (coeTRef A))
       (coeTRef B))

module _ {i}{Γ : Con i}{j}(A : Ty Γ j){k}(B : Ty (Γ ▷ A) k) where
  ∣Π∣ : ∣ Γ ∣C → Set (j ⊔ k)
  ∣Π∣ γ = Σsp ((α : ∣ A ∣T γ) → ∣ B ∣T (γ ,Σ α)) λ f →
              (α α' : ∣ A ∣T γ)(α~ : Liftp (A T refC Γ γ ⊢ α ~ α')) → B T refC Γ γ ,p unliftp α~ ⊢ f α ~ f α'

  Π : Ty Γ (j ⊔ k)
  Π = mkTy {Γ = Γ}{j ⊔ k}
    ∣Π∣
    (λ {γ}{γ'} γ~ f f' → (α : ∣ A ∣T γ)(α' : ∣ A ∣T γ')(α~ : Liftp (A T γ~ ⊢ α ~ α')) → B T γ~ ,p unliftp α~ ⊢ proj₁sp f α ~ proj₁sp f' α')
    proj₂sp
    (λ f~ _ _ α~ → symT B (f~ _ _ (liftp (symT A (unliftp α~)))))
    (λ {_}{_}{_}{γ~} f~ f~' α α' α~ → transT B (f~ _ _ (liftp (cohT A γ~ α))) (f~' _ _ (liftp (transT A (symT A (cohT A γ~ α)) (unliftp α~)))))
    (λ γ~ f → (λ α' → coeT B (γ~ ,p cohT* A γ~ α') (proj₁sp f (coeT A (symC Γ γ~) α'))) ,sp
              λ α α' α~ → transT3 B
                                  (symT B (cohT B (γ~ ,p cohT* A γ~ α) (proj₁sp f (coeT* A γ~ α))))
                                  (proj₂sp f _ _ (liftp (transT A (cohT* A γ~ α) (transT A (unliftp α~) (cohT A (symC Γ γ~) α')))))
                                  (cohT B (γ~ ,p cohT* A γ~ α') (proj₁sp f (coeT* A γ~ α'))))
    (λ {γ}{γ'} γ~ f α α' α~ → transT B (proj₂sp f _ _ (liftp (transT A (unliftp α~) (cohT A (symC Γ γ~) α')))) (cohT B (γ~ ,p cohT* A γ~ α') (proj₁sp f (coeT A (symC Γ γ~) α'))))
    (J-p
       {A = {γα : ∣ Γ ▷ A ∣C} → ∣ B ∣T γα → ∣ B ∣T γα}
       {λ {γα} → coeT B (refC (Γ ▷ A) γα)}
       (λ {z} e →
         _≡p_
           {A = {γ : ∣ Γ ∣C} → ∣Π∣ γ → ∣Π∣ γ}
           (λ {γ} f → (λ α' → coeT B (refC Γ γ ,p cohT* A (refC Γ γ) α') (proj₁sp f (coeT* A (refC Γ γ) α'))) ,sp
                      (λ α α' α~ → transT3 B (symT B (cohT B (refC Γ γ ,p cohT* A (refC Γ γ) α) (proj₁sp f (coeT* A (refC Γ γ) α))))
                                             (proj₂sp f _ _ (liftp (transT A (cohT* A (refC Γ γ) α) (transT A (unliftp α~) (cohT A (symC Γ (refC Γ γ)) α')))))
                                             (cohT B (refC Γ γ ,p cohT* A (refC Γ γ) α') (proj₁sp f (coeT* A (refC Γ γ) α')))))
           (λ {γ} f → (λ α' → z (proj₁sp f α')) ,sp
                      (λ α α' α~ → tr-p (λ z → B T refC Γ γ ,p unliftp α~ ⊢ z (proj₁sp f α) ~ z (proj₁sp f α'))
                                        e
                                        (transT3 B (symT B (cohT B (refC (Γ ▷ A) (γ ,Σ α)) (proj₁sp f α)))
                                                   (proj₂sp f _ _ α~)
                                                   (cohT B (refC (Γ ▷ A) (γ ,Σ α')) (proj₁sp f α'))))))
       (J-p {A = {γ : ∣ Γ ∣C} → ∣ A ∣T γ → ∣ A ∣T γ}
          {λ {γ} → coeT A (refC Γ γ)}
          (λ {z} e →
             _≡p_
               {A = {γ : ∣ Γ ∣C} → ∣Π∣ γ → ∣Π∣ γ}
               (λ {γ} f → (λ α' → coeT B (refC Γ γ ,p cohT* A (refC Γ γ) α') (proj₁sp f (coeT* A (refC Γ γ) α'))) ,sp
                      (λ α α' α~ → transT3 B (symT B (cohT B (refC Γ γ ,p cohT* A (refC Γ γ) α) (proj₁sp f (coeT* A (refC Γ γ) α))))
                                             (proj₂sp f _ _ (liftp (transT A (cohT* A (refC Γ γ) α) (transT A (unliftp α~) (cohT A (symC Γ (refC Γ γ)) α')))))
                                             (cohT B (refC Γ γ ,p cohT* A (refC Γ γ) α') (proj₁sp f (coeT* A (refC Γ γ) α')))))
               (λ {γ} f → (λ α' → coeT B (refC Γ γ ,p tr-p (λ z → A T refC Γ γ ⊢ z α' ~ α') e (cohT* A (refC Γ γ) α')) (proj₁sp f (z α'))) ,sp
                          (λ α α' α~ →
                            J-p (λ {z} e' → B T refC Γ γ ,p unliftp α~ ⊢ coeT B (refC Γ γ ,p tr-p (λ z₁ → A T refC Γ γ ⊢ z₁ α  ~ α ) e' (cohT* A (refC Γ γ) α )) (proj₁sp f (z α )) ~
                                                                coeT B (refC Γ γ ,p tr-p (λ z₁ → A T refC Γ γ ⊢ z₁ α' ~ α') e' (cohT* A (refC Γ γ) α')) (proj₁sp f (z α')))
                                (transT3 B (symT B (cohT B (refC Γ γ ,p cohT* A (refC Γ γ) α) (proj₁sp f (coeT* A (refC Γ γ) α))))
                                                   (proj₂sp f _ _ (liftp (transT A (cohT* A (refC Γ γ) α) (transT A (unliftp α~) (cohT A (symC Γ (refC Γ γ)) α')))))
                                                   (cohT B (refC Γ γ ,p cohT* A (refC Γ γ) α') (proj₁sp f (coeT* A (refC Γ γ) α'))))
                                e)))
          reflp
          (coeTRef A))
       (coeTRef B))

lam : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{B : Ty (Γ ▷ A) k} → Tm (Γ ▷ A) B → Tm Γ (Π A B)
lam {Γ = Γ} t = record { ∣_∣t = λ γ → (λ α → ∣ t ∣t (γ ,Σ α)) ,sp λ _ _ α~ → ~t t (refC Γ γ ,p unliftp α~) ; ~t = λ γ~ _ _ α~ → ~t t (γ~ ,p unliftp α~) }

app : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{B : Ty (Γ ▷ A) k} → Tm Γ (Π A B) → Tm (Γ ▷ A) B
app {Γ} t = record { ∣_∣t = λ { (γ ,Σ α) → proj₁sp (∣ t ∣t γ) α } ; ~t = λ { (γ~ ,p α~) → ~t t γ~ _ _ (liftp α~) } }

Π[] : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Tms Γ Δ}{k}{A : Ty Δ k}{l}{B : Ty (Δ ▷ A) l} →
  Π A B [ σ ]T ≡ Π (A [ σ ]T) (B [ σ ^ A ]T)
Π[] = refl

lam[] : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Tms Γ Δ}{k}{A : Ty Δ k}{l}{B : Ty (Δ ▷ A) l}{t : Tm (Δ ▷ A) B} →
      (_[_]t {i}{Γ}{j}{Δ}{_}{Π A B}(lam {_}{Δ}{_}{A}{_}{B} t) σ) ≡ (lam {_}{Γ}{_}{A [ σ ]T}{_}{B [ σ ^ A ]T}(t [ σ ^ A ]t))
lam[] = refl

Πβ : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{B : Ty (Γ ▷ A) k}{t : Tm (Γ ▷ A) B} →
  app {i}{Γ}{j}{A}{k}{B} (lam {i}{Γ}{j}{A}{k}{B} t) ≡ t
Πβ = refl

Πη : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{B : Ty (Γ ▷ A) k}{t : Tm Γ (Π A B)} →
  lam {i}{Γ}{j}{A}{k}{B} (app {i}{Γ}{j}{A}{k}{B} t) ≡ t
Πη = refl

open import ModelR.Iden

extt : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{B : Ty (Γ ▷ A) k}{f g : Tm Γ (Π A B)} → Tm (Γ ▷ A) (Id {A = B} (app {A = A} f) (app {A = A} g)) → Tm Γ (Id {A = Π A B} f g)
extt {i}{Γ}{j}{A}{k}{B}{f}{g} t = record { ∣_∣t = λ γ → liftp λ α α' α~ → transT B (unliftp (∣ t ∣t (γ ,Σ α))) (proj₂sp (∣ g ∣t γ) _ _ α~) }
