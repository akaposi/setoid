{-# OPTIONS --without-K --prop #-}

module ModelR.Props where

open import lib
open import Agda.Primitive

open import ModelR.Decl
open import ModelR.Core

infixl 5 _⇒P_
infixl 5 _×P_
infixl 5 _,P_

-- a universe of propositions

Props : ∀{i}{Γ : Con i} j → Ty Γ (lsuc j)
Props {i}{Γ} j = record
  { ∣_∣T_ = λ γ → Prop j
  ; _T_⊢_~_ = λ _ a b → LiftP ((Liftp a → b) ×p (Liftp b → a))
  ; refT = λ _ → liftP ((λ x → unliftp x) ,p (λ x → unliftp x))
  ; symT = λ { (liftP (f ,p g)) → liftP (g ,p f) }
  ; transT = λ { (liftP (f ,p g)) (liftP (f' ,p g')) → liftP ((λ x → f' (liftp (f x))) ,p (λ y → g (liftp (g' y)))) }
  ; coeT = λ _ a → a
  ; cohT = λ _ _ → liftP ((λ x → unliftp x) ,p (λ x → unliftp x))
  ; coeTRef = reflp
  }

ElP : ∀{i}{Γ : Con i}{j} → Tm Γ (Props j) → Ty Γ j
ElP {Γ} a = record
  { ∣_∣T_ = λ γ → Liftp (∣ a ∣t γ)
  ; _T_⊢_~_ = λ _ _ _ → LiftP ⊤p
  ; coeT = λ { γ~ (liftp α) → liftp (proj₁p (unliftP (~t a γ~)) (liftp α)) }
  ; coeTRef = reflp
  }

Props[] : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Tms Γ Δ}{k} → Props k [ σ ]T ≡ Props k
Props[] = refl

ElP[] : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Tms Γ Δ}{k}{t : Tm Δ (Props k)} → ElP t [ σ ]T ≡ ElP (t [ σ ]t)
ElP[] = refl

irr : ∀{i}{Γ : Con i}{j}{a : Tm Γ (Props j)}{u v : Tm Γ (ElP a)} → u ≡ v
irr = refl

-- closed under unit

⊤P : ∀{i}{Γ : Con i} → Tm Γ (Props lzero)
⊤P = record { ∣_∣t = λ γ → ⊤p }

ttP : ∀{i}{Γ : Con i} → Tm Γ (ElP ⊤P)
ttP = record {}

⊤P[] : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Tms Γ Δ} → ⊤P [ σ ]t ≡ ⊤P
⊤P[] = refl
ttP[] : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Tms Γ Δ} → ttP [ σ ]t ≡ ttP
ttP[] = refl

-- closed under empty

⊥P : ∀{i}{Γ : Con i} → Tm Γ (Props lzero)
⊥P = record { ∣_∣t = λ γ → ⊥p ; ~t = λ _ → liftP ((λ x → unliftp x) ,p (λ x → unliftp x)) }

abortP : ∀{i}{Γ : Con i}{j}{A : Ty Γ j} → Tm Γ (ElP ⊥P) → Tm Γ A
abortP t = record
  { ∣_∣t = λ γ → abort⊥p (unliftp (∣ t ∣t γ))
  ; ~t = λ {γ} _ → abort⊥pp (unliftp (∣ t ∣t γ))
  }

⊥P[] : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Tms Γ Δ} → ⊥P [ σ ]t ≡ ⊥P
⊥P[] = refl
abortP[] : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Tms Γ Δ}{k}{A : Ty Δ k}{t : Tm Δ (ElP ⊥P)} → abortP {A = A} t [ σ ]t ≡ abortP (t [ σ ]t)
abortP[] = refl

-- closed under Pi

ΠP : ∀{i}{Γ : Con i}{j}(A : Ty Γ j){k}(b : Tm (Γ ▷ A) (Props k)) → Tm Γ (Props (j ⊔ k))
ΠP {i}{Γ} A b = record
  { ∣_∣t = λ γ → (α : ∣ A ∣T γ) → ∣ b ∣t (γ ,Σ α)
    ; ~t = λ γ~ → liftP ((λ f α' → proj₁p (unliftP (~t b (γ~ ,p symT A (cohT A (symC Γ γ~) α')))) (liftp (unliftp f (coeT A (symC Γ γ~) α')))) ,p
                          λ f' α → proj₂p (unliftP (~t b (γ~ ,p cohT A γ~ α))) (liftp (unliftp f' (coeT A γ~ α))))
  }

lamP : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{b : Tm (Γ ▷ A) (Props k)} → Tm (Γ ▷ A) (ElP b) → Tm Γ (ElP (ΠP A b))
lamP {Γ}{A}{b} t = record { ∣_∣t = λ γ → liftp λ α → unliftp (∣ t ∣t (γ ,Σ α)) }

appP : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{b : Tm (Γ ▷ A) (Props k)} → Tm Γ (ElP (ΠP A b)) → Tm (Γ ▷ A) (ElP b)
appP {Γ}{A}{b} t = record { ∣_∣t = λ { (γ ,Σ α) → liftp (unliftp (∣ t ∣t γ) α) } }

ΠP[] : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{b : Tm (Γ ▷ A) (Props k)}{l}{Θ : Con l}{σ : Tms Θ Γ} → ΠP A b [ σ ]t ≡ ΠP (A [ σ ]T) (b [ σ ^ A ]t)
ΠP[] = refl

_⇒P_ : ∀{i}{Γ : Con i}{j}(A : Ty Γ j){k}(b : Tm Γ (Props k)) → Tm Γ (Props (j ⊔ k))
A ⇒P b = ΠP A (b [ wk {A = A} ]t)

-- closed under Sigma

ΣP : ∀{i}{Γ : Con i}{j}(a : Tm Γ (Props j)){k}(b : Tm (Γ ▷ ElP a) (Props k)) → Tm Γ (Props (j ⊔ k))
ΣP a b = record
  { ∣_∣t = λ γ → Σp (∣ a ∣t γ) λ α → ∣ b ∣t (γ ,Σ liftp α)
  ; ~t = λ {γ}{γ'} γ~ → liftP ((λ { (liftp (α ,p β)) → proj₁p (unliftP (~t a γ~)) (liftp α) ,p proj₁p (unliftP (~t b (γ~ ,p _))) (liftp β) }) ,p
                               (λ { (liftp (α ,p β)) → proj₂p (unliftP (~t a γ~)) (liftp α) ,p proj₂p (unliftP (~t b (γ~ ,p _))) (liftp β) }))
  }

_,P_ : ∀{i}{Γ : Con i}{j}{a : Tm Γ (Props j)}{k}{b : Tm (Γ ▷ ElP a) (Props k)}(t : Tm Γ (ElP a))(u : Tm Γ (ElP b [ < t > ]T)) →
  Tm Γ (ElP (ΣP a b))
t ,P u = record { ∣_∣t = λ γ → liftp (unliftp (∣ t ∣t γ) ,p unliftp (∣ u ∣t γ)) }

proj₁P : ∀{i}{Γ : Con i}{j}{a : Tm Γ (Props j)}{k}{b : Tm (Γ ▷ ElP a) (Props k)} → Tm Γ (ElP (ΣP a b)) → Tm Γ (ElP a)
proj₁P w = record { ∣_∣t = λ γ → liftp (proj₁p (unliftp (∣ w ∣t γ))) }

proj₂P : ∀{i}{Γ : Con i}{j}{a : Tm Γ (Props j)}{k}{b : Tm (Γ ▷ ElP a) (Props k)}(w : Tm Γ (ElP (ΣP a b))) →
  Tm Γ (ElP b [ < proj₁P {a = a}{b = b} w > ]T)
proj₂P w = record { ∣_∣t = λ γ → liftp (proj₂p (unliftp (∣ w ∣t γ))) }

ΣP[] : ∀{i}{Γ : Con i}{j}{a : Tm Γ (Props j)}{k}{b : Tm (Γ ▷ ElP a) (Props k)}{l}{Θ : Con l}{σ : Tms Θ Γ} →
  ΣP a b [ σ ]t ≡ ΣP (a [ σ ]t) (b [ σ ^ ElP a ]t)
ΣP[] = refl

_×P_ : ∀{i}{Γ : Con i}{j}(a : Tm Γ (Props j)){k}(b : Tm Γ (Props k)) → Tm Γ (Props (j ⊔ k))
a ×P b = ΣP a (b [ wk {A = ElP a} ]t)

-- propositional extensionality

open import ModelR.Iden

ext : ∀{i}{Γ : Con i}{j}{a b : Tm Γ (Props j)} →
  Tm (Γ ▷ ElP a) (ElP b [ wk {A = ElP a} ]T) → 
  Tm (Γ ▷ ElP b) (ElP a [ wk {A = ElP b} ]T) →
  Tm Γ (Id a b)
ext f g = record { ∣_∣t = λ γ → liftp (liftP ((λ α → unliftp (∣ f ∣t (γ ,Σ α))) ,p
                                               λ β → unliftp (∣ g ∣t (γ ,Σ β)))) }

-- lifting propositions

LIFTP : ∀{i}{Γ : Con i}{j}(a : Tm Γ (Props j)){k} → Tm Γ (Props (j ⊔ k))
LIFTP a {k} = record {
  ∣_∣t = λ γ → LiftP {_}{k}(∣ a ∣t γ) ;
  ~t   = λ γ~ → liftP ((λ α  → liftP (proj₁p (unliftP (~t a γ~)) (liftp (unliftP (unliftp α))))) ,p
                       (λ α' → liftP (proj₂p (unliftP (~t a γ~)) (liftp (unliftP (unliftp α')))))) }

mkLIFTP : ∀{i}{Γ : Con i}{j}{a : Tm Γ (Props j)}(t : Tm Γ (ElP a)){k} → Tm Γ (ElP (LIFTP a {k}))
mkLIFTP t = record { ∣_∣t = λ γ → liftp (liftP (unliftp (∣ t ∣t γ))) }

unLIFTP : ∀{i}{Γ : Con i}{j}{a : Tm Γ (Props j)}{k}(t : Tm Γ (ElP (LIFTP a {k}))) → Tm Γ (ElP a)
unLIFTP t = record { ∣_∣t = λ γ → liftp (unliftP (unliftp (∣ t ∣t γ))) }
