{-# OPTIONS --without-K --prop #-}

module ModelR.Sigma where

open import lib
open import Agda.Primitive

open import ModelR.Decl
open import ModelR.Core

Σ' : ∀{i}{Γ : Con i}{j}(A : Ty Γ j){k}(B : Ty (Γ ▷ A) k) → Ty Γ (j ⊔ k)
Σ' {i}{Γ}{j} A {k} B = mkTy
  (λ γ → Σ (∣ A ∣T γ) (λ α → ∣ B ∣T (γ ,Σ α)))
  (λ { γ~ (u₀ ,Σ v₀) (u₁ ,Σ v₁) → Σp (A T γ~ ⊢ u₀ ~ u₁) λ u₀₁ → B T γ~ ,p u₀₁ ⊢ v₀ ~ v₁ })
  (λ { (u ,Σ v) → refT A u ,p refT B v })
  (λ { {_}{_}{_}{u₀ ,Σ v₀}{u₁ ,Σ v₁}(u₀₁ ,p v₀₁) → (symT A u₀₁) ,p symT B v₀₁ })
  (λ { {_}{_}{_}{_}{_}{u₀ ,Σ v₀}{u₁ ,Σ v₁}{u₂ ,Σ v₂}(u₀₁ ,p v₀₁)(u₁₂ ,p v₁₂) → (transT A u₀₁ u₁₂) ,p transT B v₀₁ v₁₂ })
  (λ { γ~ (u₀ ,Σ v₀) → coeT A γ~ u₀ ,Σ coeT B (γ~ ,p (cohT A γ~ u₀)) v₀ })
  (λ { γ~ (u₀ ,Σ v₀) → cohT A γ~ u₀ ,p cohT B (γ~ ,p (cohT A γ~ u₀)) v₀ })
  (tr-p
    (λ z →
      _≡p_
        {A = {γ : ∣ Γ ∣C} → Σ (∣ A ∣T γ) (λ α → ∣ B ∣T (γ ,Σ α)) → Σ (∣ A ∣T γ) (λ α → ∣ B ∣T (γ ,Σ α))}
        (λ { {γ}(u₀ ,Σ v₀) → coeT A (refC Γ γ) u₀ ,Σ coeT B (refC Γ γ ,p cohT A (refC Γ γ) u₀) v₀ })
        (λ { {γ}(u₀ ,Σ v₀) → u₀ ,Σ z v₀ }))
    {x = λ {γ : ∣ Γ ▷ A ∣C} → coeT B (refC Γ (proj₁ γ) ,p refT A (proj₂ γ))}
    (coeTRef B)
    (J-p
      {x = λ {γ : ∣ Γ ∣C} → coeT A (refC Γ γ)}
      (λ {z} e → _≡p_
        {A = {γ : ∣ Γ ∣C} → Σ (∣ A ∣T γ) (λ α → ∣ B ∣T (γ ,Σ α)) → Σ (∣ A ∣T γ) (λ α → ∣ B ∣T (γ ,Σ α))}
        (λ { {γ}(u₀ ,Σ v₀) → coeT A (refC Γ γ) u₀ ,Σ coeT B (refC Γ γ ,p cohT A (refC Γ γ) u₀) v₀ })
        (λ { {γ}(u₀ ,Σ v₀) → z u₀ ,Σ coeT B (refC Γ γ ,p tr-p (λ z → A T refC Γ γ ⊢ u₀ ~ z u₀) e (cohT A (refC Γ γ) u₀)) v₀ }))
      reflp
      (coeTRef A)))

_,Σ'_ : {i j k : Level}{Γ : Con i}{A : Ty Γ j}{B : Ty (Γ ▷ A) k}(u : Tm Γ A)(v : Tm Γ (B [ _,_ id {A = A} u ]T)) → Tm Γ (Σ' A B)
u ,Σ' v = record {
  ∣_∣t = λ γ → ∣ u ∣t γ ,Σ ∣ v ∣t γ ;
  ~t   = λ γ~ → ~t u γ~ ,p ~t v γ~ }

pr₁ : {i j k : Level}{Γ : Con i}{A : Ty Γ j}{B : Ty (Γ ▷ A) k} → Tm Γ (Σ' A B) → Tm Γ A
pr₁ t = record {
  ∣_∣t = λ γ → proj₁ (∣ t ∣t γ) ;
  ~t   = λ γ~ → proj₁p (~t t γ~) }

pr₂ : {i j k : Level}{Γ : Con i}{A : Ty Γ j}{B : Ty (Γ ▷ A) k}(t : Tm Γ (Σ' A B)) → Tm Γ (B [ _,_ id {A = A} (pr₁ {A = A}{B} t) ]T)
pr₂ t = record {
  ∣_∣t = λ γ → proj₂ (∣ t ∣t γ) ;
  ~t   = λ γ~ → proj₂p (~t t γ~) }

Σ[] : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Tms Γ Δ}{k}{A : Ty Δ k}{l}{B : Ty (Δ ▷ A) l} →
  Σ' A B [ σ ]T ≡ Σ' (A [ σ ]T) (B [ σ ^ A ]T)
Σ[] = refl

,Σ[] : ∀{i j k : Level}{Γ : Con i}{A : Ty Γ j}{B : Ty (Γ ▷ A) k}{u : Tm Γ A}{v : Tm Γ (B [ _,_ id {A = A} u ]T)}{l}{Ω : Con l}{ν : Tms Ω Γ} →
  (_,Σ'_ {A = A}{B = B} u v) [ ν ]t ≡ _,Σ'_ {A = A [ ν ]T}{B = B [ ν ^ A ]T} (u [ ν ]t) (v [ ν ]t)
,Σ[] = refl

Σβ₁ : ∀{i j k : Level}{Γ : Con i}{A : Ty Γ j}{B : Ty (Γ ▷ A) k}{u : Tm Γ A}{v : Tm Γ (B [ _,_ id {A = A} u ]T)} →
  pr₁ {A = A}{B = B}(_,Σ'_ {A = A}{B = B} u v) ≡ u
Σβ₁ = refl

Σβ₂ : ∀{i j k : Level}{Γ : Con i}{A : Ty Γ j}{B : Ty (Γ ▷ A) k}{u : Tm Γ A}{v : Tm Γ (B [ _,_ id {A = A} u ]T)} →
  pr₂ {A = A}{B = B}(_,Σ'_ {A = A}{B = B} u v) ≡ v
Σβ₂ = refl

Ση : ∀{i j k : Level}{Γ : Con i}{A : Ty Γ j}{B : Ty (Γ ▷ A) k}{t : Tm Γ (Σ' A B)} →
  (_,Σ'_ {A = A}{B = B} (pr₁ {A = A}{B = B} t) (pr₂ {A = A}{B = B} t)) ≡ t
Ση = refl
