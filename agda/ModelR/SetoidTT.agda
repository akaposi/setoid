{-# OPTIONS --without-K --prop #-}

module ModelR.SetoidTT where

open import lib
open import Agda.Primitive

open import ModelR.Decl
open import ModelR.Core
open import ModelR.Func
open import ModelR.Sigma
open import ModelR.Bool
open import ModelR.Props

-- contexts

_~C : ∀{i}(Γ : Con i){j}{Ω : Con j} → Tms Ω Γ → Tms Ω Γ → Tm Ω (Props i)
(Γ ~C) ρ₀ ρ₁ =
  record { ∣_∣t = λ γ → Γ C ∣ ρ₀ ∣s γ ~ ∣ ρ₁ ∣s γ
         ; ~t = λ p → liftP ((λ { (liftp x) → transC Γ (symC Γ (~s ρ₀ p)) (transC Γ x (~s ρ₁ p)) }) ,p
                             (λ { (liftp x) → transC Γ (~s ρ₀ p) (transC Γ x (symC Γ (~s ρ₁ p))) })) }

RC : ∀{i}(Γ : Con i){j}{Ω : Con j}(ρ : Tms Ω Γ) → Tm Ω (ElP ((Γ ~C) ρ ρ))
RC Γ ρ = record { ∣_∣t = λ γ → liftp (refC Γ (∣ ρ ∣s γ)) }

SC : ∀{i}(Γ : Con i){j}{Ω : Con j}{ρ₀ ρ₁ : Tms Ω Γ}(ρ₀₁ : Tm Ω (ElP ((Γ ~C) ρ₀ ρ₁))) → Tm Ω (ElP ((Γ ~C) ρ₁ ρ₀))
SC Γ {Ω} ρ₀₁ = record { ∣_∣t = λ γ → liftp (symC Γ ( unliftp (∣ ρ₀₁ ∣t γ))) }

TC : ∀{i}(Γ : Con i){j}{Ω : Con j}{ρ₀ ρ₁ ρ₂ : Tms Ω Γ} → Tm Ω (ElP ((Γ ~C) ρ₀ ρ₁)) → Tm Ω (ElP ((Γ ~C) ρ₁ ρ₂)) → Tm Ω (ElP ((Γ ~C) ρ₀ ρ₂))
TC Γ {Ω} p q = record { ∣_∣t = λ γ → liftp ((transC Γ ( unliftp (∣ p ∣t γ)) ( unliftp (∣ q ∣t γ)))) }

-- types

_~T : ∀{i}{Γ : Con i}{k}(A : Ty Γ k){j}{Ω : Con j}{ρ₀ ρ₁ : Tms Ω Γ}(ρ₀₁ : Tm Ω (ElP ((Γ ~C) ρ₀ ρ₁))) → Tm Ω (A [ ρ₀ ]T) → Tm Ω (A [ ρ₁ ]T) → Tm Ω (Props k)
(A ~T) ρ₀₁ t₀ t₁ = record { ∣_∣t = λ γ → A T unliftp (∣ ρ₀₁ ∣t γ) ⊢ ∣ t₀ ∣t γ ~ ∣ t₁ ∣t γ
                          ; ~t = λ p₁ → liftP ((λ x → transT A (symT A (~t t₀ p₁)) (transT A (unliftp x) (~t t₁ p₁)))
                                           ,p (λ x → transT A (~t t₀ p₁) (transT A (unliftp x) (symT A (~t t₁ p₁))))) }

RT : ∀{i}{Γ : Con i}{j}{Ω : Con j}{k}(A : Ty Γ k){ρ : Tms Ω Γ}(t : Tm Ω (A [ ρ ]T)) → Tm Ω (ElP ((A ~T) (RC Γ ρ) t t))
RT A t = record { ∣_∣t = λ γ → liftp (refT A (∣ t ∣t γ )) }

ST : ∀{i}{Γ : Con i}{j}{Ω : Con j}{k}(A : Ty Γ k){ρ₀ ρ₁ : Tms Ω Γ}{ρ₀₁ : Tm Ω (ElP ((Γ ~C) ρ₀ ρ₁))}{t₀ : Tm Ω (A [ ρ₀ ]T)}{t₁ : Tm Ω (A [ ρ₁ ]T)}
     (t₀₁ : Tm Ω (ElP ((A ~T) ρ₀₁ t₀ t₁))) → Tm Ω (ElP ((A ~T) (SC Γ {ρ₀ = ρ₀}{ρ₁} ρ₀₁) t₁ t₀))
ST A t₀₁ = record { ∣_∣t = λ γ → liftp (symT A (unliftp (∣ t₀₁ ∣t γ))) }

TT : ∀{i}{Γ : Con i}{j}{Ω : Con j}{k}(A : Ty Γ k){ρ₀ ρ₁ ρ₂ : Tms Ω Γ}{ρ₀₁ : Tm Ω (ElP ((Γ ~C) ρ₀ ρ₁))}{ρ₁₂ : Tm Ω (ElP ((Γ ~C) ρ₁ ρ₂))}
     {t₀ : Tm Ω (A [ ρ₀ ]T)}{t₁ : Tm Ω (A [ ρ₁ ]T)}{t₂ : Tm Ω (A [ ρ₂ ]T)}
     (t₀₁ : Tm Ω (ElP ((A ~T) ρ₀₁ t₀ t₁)))(t₁₂ : Tm Ω (ElP ((A ~T) ρ₁₂ t₁ t₂))) → Tm Ω (ElP ((A ~T) (TC Γ {ρ₀ = ρ₀}{ρ₁}{ρ₂} ρ₀₁ ρ₁₂) t₀ t₂))
TT A t₀₁ t₁₂ = record { ∣_∣t = λ γ → liftp (transT A (unliftp (∣ t₀₁ ∣t γ)) (unliftp (∣ t₁₂ ∣t γ))) }

coeT' : ∀{i}{Γ : Con i}{j}{Ω : Con j}{k}(A : Ty Γ k){ρ₀ ρ₁ : Tms Ω Γ}(ρ₀₁ : Tm Ω (ElP ((Γ ~C) ρ₀ ρ₁)))(t₀ : Tm Ω (A [ ρ₀ ]T)) →
  Tm Ω (A [ ρ₁ ]T)
coeT' A ρ₀₁ t = record { ∣_∣t = λ γ → coeT A (unliftp (∣ ρ₀₁ ∣t γ)) (∣ t ∣t γ)
                       ; ~t   = λ p₁ → let X = symT A (cohT A (unliftp (∣ ρ₀₁ ∣t _)) (∣ t ∣t _) ) in
                                       let Y = cohT A (unliftp (∣ ρ₀₁ ∣t _)) (∣ t ∣t _) in
                                       transT A X (transT A (~t t p₁) Y) }

cohT' : ∀{i}{Γ : Con i}{j}{Ω : Con j}{k}(A : Ty Γ k){ρ₀ ρ₁ : Tms Ω Γ}(ρ₀₁ : Tm Ω (ElP ((Γ ~C) ρ₀ ρ₁)))(t₀ : Tm Ω (A [ ρ₀ ]T)) →
  Tm Ω (ElP ((A ~T) {ρ₀ = ρ₀}{ρ₁ = ρ₁} ρ₀₁ t₀ (coeT' A ρ₀₁ t₀)))
cohT' A ρ₀₁ t = record { ∣_∣t = λ γ → liftp (cohT A (unliftp (∣ ρ₀₁ ∣t γ)) (∣ t ∣t γ)) }

-- substitutions

_~s' : ∀{i}{Γ : Con i}{j}{Δ : Con j}(δ : Tms Γ Δ){k}{Ω : Con k}{ρ₀ ρ₁ : Tms Ω Γ}(ρ₀₁ : Tm Ω (ElP ((Γ ~C) ρ₀ ρ₁))) →
  Tm Ω (ElP ((Δ ~C) (δ ∘ ρ₀) (δ ∘ ρ₁)))
(δ ~s') ρ₀₁ = record { ∣_∣t = λ γ → liftp (~s δ (unliftp (∣ ρ₀₁ ∣t γ))) }

-- terms

_~t' : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}(t : Tm Γ A){k}{Ω : Con k}{ρ₀ ρ₁ : Tms Ω Γ}(ρ₀₁ : Tm Ω (ElP ((Γ ~C) ρ₀ ρ₁))) →
  Tm Ω (ElP ((A ~T) ρ₀₁ (t [ ρ₀ ]t) (t [ ρ₁ ]t)))
(t ~t') ρ₀₁ = record { ∣_∣t = λ γ → liftp (~t t (unliftp (∣ ρ₀₁ ∣t γ))) }

-- computation rules for ~

•~ : ∀{i}{Ω : Con i}{ρ₀ ρ₁ : Tms Ω •} → (• ~C) ρ₀ ρ₁ ≡ ⊤P
•~ = refl

▷~ : ∀{i}{Γ : Con i}{k}{A : Ty Γ k}{j}{Ω : Con j}{ρ₀ ρ₁ : Tms Ω Γ}{ρ₀₁ : Tm Ω (ElP ((Γ ~C) ρ₀ ρ₁))}
  {t₀ : Tm Ω (A [ ρ₀ ]T)}{t₁ : Tm Ω (A [ ρ₁ ]T)}{t₀₁ : Tm Ω (ElP ((A ~T) ρ₀₁ t₀ t₁))} →
  ((Γ ▷ A) ~C) (_,_ ρ₀ {A = A} t₀) (_,_ ρ₁ {A = A} t₁) ≡
  ΣP ((Γ ~C) ρ₀ ρ₁)
     ((A ~T) {ρ₀ = ρ₀ ∘ wk {A = ElP ((Γ ~C) ρ₀ ρ₁)}}
             (vz {A = ElP ((Γ ~C) ρ₀ ρ₁)})
             (t₀ [ wk {A = ElP ((Γ ~C) ρ₀ ρ₁)} ]t)
             (t₁ [ wk {A = ElP ((Γ ~C) ρ₀ ρ₁)} ]t))
▷~ = refl

[]~ : ∀{i}{Γ : Con i}{j}{Δ : Con j}{k}{A : Ty Δ k}{δ : Tms Γ Δ}{l}{Ω : Con l}{ρ₀ ρ₁ : Tms Ω Γ}{ρ₀₁ : Tm Ω (ElP ((Γ ~C) ρ₀ ρ₁))}
  {t₀ : Tm Ω (A [ δ ]T [ ρ₀ ]T)}{t₁ : Tm Ω (A [ δ ]T [ ρ₁ ]T)} →
  ((A [ δ ]T) ~T) {ρ₀ = ρ₀}{ρ₁ = ρ₁} ρ₀₁ t₀ t₁ ≡ (A ~T) ((δ ~s') {ρ₀ = ρ₀}{ρ₁ = ρ₁} ρ₀₁) t₀ t₁
[]~ = refl

Π~ : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{B : Ty (Γ ▷ A) k}{l}{Ω : Con l}{ρ₀ ρ₁ : Tms Ω Γ}{ρ₀₁ : Tm Ω (ElP ((Γ ~C) ρ₀ ρ₁))}
  {t₀ : Tm Ω (Π A B [ ρ₀ ]T)}{t₁ : Tm Ω (Π A B [ ρ₁ ]T)} →
  let A[ρ₀] = A [ ρ₀ ]T
      A[ρ₁] = A [ ρ₁ ∘ wk {A = A[ρ₀]} ]T
      v0    = vz {A = A[ρ₁]}
      v1    = vs {B = A[ρ₁]} (vz {A = A[ρ₀]})
      A~    = ElP ((A ~T) {ρ₀ = ρ₀ ∘ (wk {A = A[ρ₀]} ∘ wk {A = A[ρ₁]})}
                          {ρ₁ = ρ₁ ∘ (wk {A = A[ρ₀]} ∘ wk {A = A[ρ₁]})}
                          (ρ₀₁ [ wk {A = A[ρ₀]} ∘ wk {A = A[ρ₁]} ]t)
                          v1
                          v0)
      wk3   = ((wk {A = A[ρ₀]} ∘ wk {A = A[ρ₁]}) ∘ wk {A = A~})
  in
    ((Π A B) ~T) {ρ₀ = ρ₀}{ρ₁ = ρ₁} ρ₀₁ t₀ t₁ ≡
    ΠP A[ρ₀] (ΠP (A[ρ₁]) (ΠP A~ ((B ~T) {ρ₀ = _,_ (ρ₀ ∘ wk3){A = A}(vs {B = A~} v1)}
                                        {ρ₁ = _,_ (ρ₁ ∘ wk3){A = A}(vs {B = A~} v0)}
                                        (_,P_ {a = (Γ ~C) ρ₀ ρ₁ [ wk3 ]t}
                                              {b = (A ~T) {ρ₀ = (ρ₀ ∘ wk3) ∘ wk {A = ElP ((Γ ~C) ρ₀ ρ₁ [ wk3 ]t)}}
                                                          {ρ₁ = (ρ₁ ∘ wk3) ∘ wk {A = ElP ((Γ ~C) ρ₀ ρ₁ [ wk3 ]t)}}
                                                          (vz {A = ElP ((Γ ~C) ρ₀ ρ₁ [ wk3 ]t)})
                                                          (vs {B = ElP ((Γ ~C) ρ₀ ρ₁ [ wk3 ]t)} (vs {B = A~} v1))
                                                          (vs {B = ElP ((Γ ~C) ρ₀ ρ₁ [ wk3 ]t)} (vs {B = A~} v0))}
                                              (ρ₀₁ [ wk3 ]t)
                                              (vz {A = A~}))
                                        (app {A = A[ρ₀]}{B = B [ ρ₀ ^ A ]T} t₀ [ wk {A = A[ρ₁]} ∘ wk {A = A~} ]t)
                                        (app {A = A [ ρ₁ ]T}{B = B [ ρ₁ ^ A ]T} t₁ [ (wk {A = A[ρ₀]} ^ (A [ ρ₁ ]T)) ∘ wk {A = A~} ]t))))
Π~ = refl

Σ~ : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{B : Ty (Γ ▷ A) k}{l}{Ω : Con l}{ρ₀ ρ₁ : Tms Ω Γ}{ρ₀₁ : Tm Ω (ElP ((Γ ~C) ρ₀ ρ₁))}
  {t₀ : Tm Ω (Σ' A B [ ρ₀ ]T)}{t₁ : Tm Ω (Σ' A B [ ρ₁ ]T)} →
  ((Σ' A B) ~T) {ρ₀ = ρ₀}{ρ₁ = ρ₁} ρ₀₁ t₀ t₁ ≡
  let
    pr₁t₀ = pr₁ {A = A [ ρ₀ ]T}{B = B [ ρ₀ ^ A ]T} t₀
    pr₁t₁ = pr₁ {A = A [ ρ₁ ]T}{B = B [ ρ₁ ^ A ]T} t₁
    A~    = (A ~T) {ρ₀ = ρ₀}{ρ₁ = ρ₁} ρ₀₁ pr₁t₀ pr₁t₁
    wk'   = wk {A = ElP A~}
    pr₂t₀ = pr₂ {A = A [ ρ₀ ]T}{B = B [ ρ₀ ^ A ]T} t₀
    pr₂t₁ = pr₂ {A = A [ ρ₁ ]T}{B = B [ ρ₁ ^ A ]T} t₁
  in
    ΣP ((A ~T) {ρ₀ = ρ₀}{ρ₁ = ρ₁} ρ₀₁ pr₁t₀ pr₁t₁)
       ((B ~T) {ρ₀ = _,_ (ρ₀ ∘ wk'){A = A}(pr₁t₀ [ wk' ]t)}
               {ρ₁ = _,_ (ρ₁ ∘ wk'){A = A}(pr₁t₁ [ wk' ]t)}
               (_,P_ {a = (Γ ~C) ρ₀ ρ₁ [ wk' ]t}
                     {b = A~ [ wk' ∘ wk {A = ElP ((Γ ~C) ρ₀ ρ₁ [ wk' ]t)} ]t}
                     (ρ₀₁ [ wk' ]t)
                     (vz {A = ElP A~}))
               (pr₂t₀ [ wk' ]t)
               (pr₂t₁ [ wk' ]t))
Σ~ = refl

Bool~ : ∀{i}{Γ : Con i}{l}{Ω : Con l}{ρ₀ ρ₁ : Tms Ω Γ}{ρ₀₁ : Tm Ω (ElP ((Γ ~C) ρ₀ ρ₁))}
  {t₀ : Tm Ω (Bool [ ρ₀ ]T)}{t₁ : Tm Ω (Bool [ ρ₁ ]T)} →
  (Bool ~T) {ρ₀ = ρ₀}{ρ₁ = ρ₁} ρ₀₁ t₀ t₁ ≡ ite (Props lzero) (ite (Props lzero) ⊤P ⊥P t₁) (ite (Props lzero) ⊥P ⊤P t₁) t₀
Bool~ = refl

Props~ : ∀{i}{Γ : Con i}{j}{l}{Ω : Con l}{ρ₀ ρ₁ : Tms Ω Γ}{ρ₀₁ : Tm Ω (ElP ((Γ ~C) ρ₀ ρ₁))}
  {a₀ : Tm Ω (Props j [ ρ₀ ]T)}{a₁ : Tm Ω (Props j [ ρ₁ ]T)} →
  ((Props j) ~T) {ρ₀ = ρ₀}{ρ₁ = ρ₁} ρ₀₁ a₀ a₁ ≡ LIFTP ((ElP a₀ ⇒P a₁) ×P (ElP a₁ ⇒P a₀))
Props~ = refl

El~ : ∀{i}{Γ : Con i}{j}{a : Tm Γ (Props j)}{l}{Ω : Con l}{ρ₀ ρ₁ : Tms Ω Γ}{ρ₀₁ : Tm Ω (ElP ((Γ ~C) ρ₀ ρ₁))}
  {t₀ : Tm Ω (ElP a [ ρ₀ ]T)}{t₁ : Tm Ω (ElP a [ ρ₁ ]T)} →
  ((ElP a) ~T) {ρ₀ = ρ₀}{ρ₁ = ρ₁} ρ₀₁ t₀ t₁ ≡ LIFTP ⊤P
El~ = refl

-- computation rules for coe

coe[] : ∀{i}{Γ : Con i}{j}{Δ : Con j}{k}{A : Ty Δ k}{δ : Tms Γ Δ}{l}{Ω : Con l}{ρ₀ ρ₁ : Tms Ω Γ}{ρ₀₁ : Tm Ω (ElP ((Γ ~C) ρ₀ ρ₁))}
  {t₀ : Tm Ω (A [ δ ]T [ ρ₀ ]T)} →
  coeT' (A [ δ ]T) {ρ₀ = ρ₀}{ρ₁ = ρ₁} ρ₀₁ t₀ ≡ coeT' A {ρ₀ = δ ∘ ρ₀}{ρ₁ = δ ∘ ρ₁}((δ ~s') {ρ₀ = ρ₀}{ρ₁ = ρ₁} ρ₀₁) t₀
coe[] = refl

coeΠ : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{B : Ty (Γ ▷ A) k}{l}{Ω : Con l}{ρ₀ ρ₁ : Tms Ω Γ}{ρ₀₁ : Tm Ω (ElP ((Γ ~C) ρ₀ ρ₁))}{t₀ : Tm Ω (Π A B [ ρ₀ ]T)} →
  let
    wk' = wk {A = A [ ρ₁ ]T}
    vz' = vz {A = A [ ρ₁ ]T}
    Γ~  = (Γ ~C) ρ₀ ρ₁ [ wk' ]t
  in
    coeT' (Π A B) {ρ₀ = ρ₀}{ρ₁ = ρ₁} ρ₀₁ t₀ ≡
    lam {A = A [ ρ₁ ]T}{B = B [ ρ₁ ^ A ]T}
        (coeT' B {ρ₀ = _,_ (ρ₀ ∘ wk'){A = A}(coeT' A {ρ₀ = ρ₁ ∘ wk'}{ρ₁ = ρ₀ ∘ wk'}(SC Γ {ρ₀ = ρ₀}{ρ₁ = ρ₁} ρ₀₁ [ wk' ]t) vz')}
                 {ρ₁ = _,_ (ρ₁ ∘ wk'){A = A}(vz')}
                 (_,P_ {a = Γ~}
                       {b = (A ~T) {ρ₀ = (ρ₀ ∘ wk') ∘ wk {A = ElP Γ~}}
                                   {ρ₁ = (ρ₁ ∘ wk') ∘ wk {A = ElP Γ~}}
                                   (vz {A = ElP Γ~})
                                   (coeT' A {ρ₀ = ρ₁ ∘ wk'}{ρ₁ = ρ₀ ∘ wk'}(SC Γ {ρ₀ = ρ₀}{ρ₁ = ρ₁} ρ₀₁ [ wk' ]t) vz' [ wk {A = ElP Γ~} ]t)
                                   (vs {B = ElP Γ~} vz')}
                       (ρ₀₁ [ wk' ]t)
                       (ST A
                           {ρ₀ = ρ₁ ∘ wk'}
                           {ρ₁ = ρ₀ ∘ wk'}
                           {ρ₀₁ = SC Γ {ρ₀ = ρ₀}{ρ₁ = ρ₁} ρ₀₁ [ wk' ]t}
                           {t₀ = vz'}
                           {t₁ = (coeT' A {ρ₀ = ρ₁ ∘ wk'}{ρ₁ = ρ₀ ∘ wk'}(SC Γ {ρ₀ = ρ₀}{ρ₁ = ρ₁} ρ₀₁ [ wk' ]t) vz')}
                           (cohT' A {ρ₀ = ρ₁ ∘ wk'}{ρ₁ = ρ₀ ∘ wk'}(SC Γ {ρ₀ = ρ₀}{ρ₁ = ρ₁} ρ₀₁ [ wk' ]t) vz')))
                 (app {A = A [ ρ₀ ]T}{B = B [ ρ₀ ^ A ]T} t₀ [ _,_ wk' {A = A [ ρ₀ ]T} ((coeT' A {ρ₀ = ρ₁ ∘ wk'}{ρ₁ = ρ₀ ∘ wk'}(SC Γ {ρ₀ = ρ₀ ∘ wk'}{ρ₁ = ρ₁ ∘ wk'} (ρ₀₁ [ wk' ]t)) vz')) ]t))
coeΠ = refl

coeΣ : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{B : Ty (Γ ▷ A) k}{l}{Ω : Con l}{ρ₀ ρ₁ : Tms Ω Γ}{ρ₀₁ : Tm Ω (ElP ((Γ ~C) ρ₀ ρ₁))}
  {t₀ : Tm Ω (Σ' A B [ ρ₀ ]T)} →
  let
    pr₁t₀ = pr₁ {A = A [ ρ₀ ]T}{B = B [ ρ₀ ^ A ]T} t₀
    pr₂t₀ = pr₂ {A = A [ ρ₀ ]T}{B = B [ ρ₀ ^ A ]T} t₀
  in
    coeT' (Σ' A B) {ρ₀ = ρ₀}{ρ₁ = ρ₁} ρ₀₁ t₀ ≡
    (_,Σ'_ {A = A [ ρ₁ ]T}{B = B [ ρ₁ ^ A ]T}
           (coeT' A {ρ₀ = ρ₀}{ρ₁ = ρ₁} ρ₀₁ pr₁t₀)
           (coeT' B
                  {ρ₀ = _,_ ρ₀ {A = A} pr₁t₀}
                  {ρ₁ = _,_ ρ₁ {A = A} (coeT' A {ρ₀ = ρ₀}{ρ₁ = ρ₁} ρ₀₁ pr₁t₀)}
                  (_,P_ {a = (Γ ~C) ρ₀ ρ₁}
                        {b = (A ~T) {ρ₀ = ρ₀}{ρ₁ = ρ₁} ρ₀₁ pr₁t₀ (coeT' A {ρ₀ = ρ₀}{ρ₁ = ρ₁} ρ₀₁ pr₁t₀) [ wk {A = ElP ((Γ ~C) ρ₀ ρ₁)} ]t}
                        ρ₀₁
                        (cohT' A {ρ₀ = ρ₀}{ρ₁ = ρ₁} ρ₀₁ pr₁t₀))
                  pr₂t₀))
coeΣ = refl

coeBool : ∀{i}{Γ : Con i}{l}{Ω : Con l}{ρ₀ ρ₁ : Tms Ω Γ}{ρ₀₁ : Tm Ω (ElP ((Γ ~C) ρ₀ ρ₁))}
  {t₀ : Tm Ω (Bool [ ρ₀ ]T)} →
  coeT' Bool {ρ₀ = ρ₀}{ρ₁ = ρ₁} ρ₀₁ t₀ ≡ t₀
coeBool = refl

coeProp : ∀{i}{Γ : Con i}{j}{l}{Ω : Con l}{ρ₀ ρ₁ : Tms Ω Γ}{ρ₀₁ : Tm Ω (ElP ((Γ ~C) ρ₀ ρ₁))}
  {t₀ : Tm Ω (Props j [ ρ₀ ]T)} →
  coeT' (Props j) {ρ₀ = ρ₀}{ρ₁ = ρ₁} ρ₀₁ t₀ ≡ t₀
coeProp = refl

coeEl : ∀{i}{Γ : Con i}{j}{a : Tm Γ (Props j)}{l}{Ω : Con l}{ρ₀ ρ₁ : Tms Ω Γ}{ρ₀₁ : Tm Ω (ElP ((Γ ~C) ρ₀ ρ₁))}
  {t₀ : Tm Ω ((ElP a) [ ρ₀ ]T)} →
  coeT' (ElP a) {ρ₀ = ρ₀}{ρ₁ = ρ₁} ρ₀₁ t₀ ≡
  appP
    {A = ElP (a [ ρ₀ ]t)}
    {b = a [ ρ₁ ]t [ wk {A = ElP (a [ ρ₀ ]t)} ]t}
    (proj₁P
      {a = ElP (a [ ρ₀ ]t) ⇒P a [ ρ₁ ]t}
      {b = (ElP (a [ ρ₁ ]t) ⇒P a [ ρ₀ ]t) [ wk {A = ElP (ElP (a [ ρ₀ ]t) ⇒P a [ ρ₁ ]t)} ]t}
      (unLIFTP {a = ((ElP (a [ ρ₀ ]t) ⇒P (a [ ρ₁ ]t)) ×P (ElP (a [ ρ₁ ]t) ⇒P (a [ ρ₀ ]t)))}{lsuc j}((a ~t'){ρ₀ = ρ₀}{ρ₁ = ρ₁} ρ₀₁)))
  [ _,_ id {A = ElP (a [ ρ₀ ]t)} t₀ ]t
coeEl = refl

-- substitution laws

~C[] : ∀{i}{Γ : Con i}{j}{Ω : Con j}{ρ₀ ρ₁ : Tms Ω Γ}{k}{Ψ : Con k}{ν : Tms Ψ Ω} → (Γ ~C) ρ₀ ρ₁ [ ν ]t ≡ (Γ ~C) (ρ₀ ∘ ν) (ρ₁ ∘ ν)
~C[] = refl

~T[] : ∀{i}{Γ : Con i}{k}{A : Ty Γ k}{j}{Ω : Con j}{ρ₀ ρ₁ : Tms Ω Γ}{ρ₀₁ : Tm Ω (ElP ((Γ ~C) ρ₀ ρ₁))}{t₀ : Tm Ω (A [ ρ₀ ]T)}{t₁ : Tm Ω (A [ ρ₁ ]T)}{k}{Ψ : Con k}{ν : Tms Ψ Ω} →
  (A ~T) ρ₀₁ t₀ t₁ [ ν ]t ≡ (A ~T) (ρ₀₁ [ ν ]t) (t₀ [ ν ]t) (t₁ [ ν ]t)
~T[] = refl

coeT[] : ∀{i}{Γ : Con i}{j}{Ω : Con j}{k}{A : Ty Γ k}{ρ₀ ρ₁ : Tms Ω Γ}{ρ₀₁ : Tm Ω (ElP ((Γ ~C) ρ₀ ρ₁))}{t₀ : Tm Ω (A [ ρ₀ ]T)}{l}{Ψ : Con l}{ν : Tms Ψ Ω} →
  coeT' A {ρ₀ = ρ₀}{ρ₁ = ρ₁} ρ₀₁ t₀ [ ν ]t ≡ coeT' A (ρ₀₁ [ ν ]t) (t₀ [ ν ]t)
coeT[] = refl

-- coe on refl

coeTRef' : ∀{i}{Γ : Con i}{j}{Ω : Con j}{k}{A : Ty Γ k}{ρ : Tms Ω Γ}{t : Tm Ω (A [ ρ ]T)} → coeT' A (RC Γ ρ) t ≡p t
coeTRef' {A = A}{t = t} = mkTm= (ap-p (λ f → λ γ → f (∣ t ∣t γ)) (coeTRef A))
