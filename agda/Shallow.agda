{-# OPTIONS --without-K --rewriting --prop #-}

open import Agda.Primitive

------------------------------------------------------------------------------
-- The relevant stuffs start at wort "start". First, there is a small library.
------------------------------------------------------------------------------

infixl 5 _,_

Π : {ℓ ℓ' : Level} (A : Set ℓ) (B : A → Set ℓ') → Set (ℓ ⊔ ℓ')
Π A B = (x : A) → B x

record Σ {ℓ ℓ'} (A : Set ℓ) (B : A → Set ℓ') : Set (ℓ ⊔ ℓ') where
  constructor _,_
  field
    pr₀ : A
    pr₁ : B pr₀

open Σ public
record ⊤ : Set where
  constructor tt

data ⊥ : Set where

⊥-elim : ∀{ℓ}{A : Set ℓ} → ⊥ → A
⊥-elim ()

¬ : Set → Set
¬ A = A → ⊥


data Bool : Set where
  true false : Bool

if_then_else_ : ∀{ℓ}{C : Bool → Set ℓ}(b : Bool)(c : C true)(d : C false) → C b
if true then c else d = c
if false then c else d = d

record ⊤p : Prop where
  constructor ttp

data ⊥p : Prop where

abort⊥p : ∀{ℓ}{A : Set ℓ} → ⊥p → A
abort⊥p ()

abort⊥pp : ∀{ℓ}{A : Prop ℓ} → ⊥p → A
abort⊥pp ()

abortp : ∀{ℓ}{A : Prop ℓ} → ⊥ → A
abortp ()

record σ {ℓ ℓ'} (A : Prop ℓ) (B : A → Prop ℓ') : Prop (ℓ ⊔ ℓ') where
  constructor _,_
  field
    pr₀ : A
    pr₁ : B pr₀
open σ public

_×_ : ∀{ℓ ℓ'} → Prop ℓ → Prop ℓ' → Prop (ℓ ⊔ ℓ')
A × B = σ A λ _ → B
infixl 4 _×_

record LiftP {ℓ ℓ'}(A : Prop ℓ) : Prop (ℓ ⊔ ℓ') where
  constructor liftP
  field
    unliftP : A
open LiftP public

record El {ℓ}(A : Prop ℓ) : Set ℓ where
  constructor liftp
  field
    unliftp : A
open El public

 
postulate _→β_ : {ℓ : Level}{A : Set ℓ} → A → A → Set
{-# BUILTIN REWRITE _→β_ #-}


------------------------------------------------------------------------------
-- start. this is basically computational identity types (CITs)
-- composed with the standard interpretation
------------------------------------------------------------------------------

-- contexts
postulate _~C : {ℓ : Level}(Γ : Set ℓ)(ρ₀ ρ₁ : Γ) → Prop ℓ
postulate RC  : {ℓ : Level}(Γ : Set ℓ)(ρ : Γ) → (Γ ~C) ρ ρ
postulate SC  : {ℓ : Level}(Γ : Set ℓ){ρ₀ ρ₁ : Γ} → (Γ ~C) ρ₀ ρ₁ → (Γ ~C) ρ₁ ρ₀
postulate TC  : {ℓ : Level}(Γ : Set ℓ){ρ₀ ρ₁ ρ₂ : Γ} → (Γ ~C) ρ₀ ρ₁ → (Γ ~C) ρ₁ ρ₂ → (Γ ~C) ρ₀ ρ₂

-- types
postulate _~T : {ℓ : Level}{Γ : Set ℓ}{ℓ' : Level}(A : Γ → Set ℓ'){ρ₀ ρ₁ : Γ}(ρ₀₁ : (Γ ~C) ρ₀ ρ₁)(t₀ : A ρ₀)(t₁ : A ρ₁) → Prop (ℓ ⊔ ℓ')
postulate RT  : {ℓ : Level}{Γ : Set ℓ}{ℓ' : Level}(A : Γ → Set ℓ'){ρ : Γ}(t : A ρ) → (A ~T) (RC Γ ρ) t t
postulate ST  : {ℓ : Level}{Γ : Set ℓ}{ℓ' : Level}(A : Γ → Set ℓ'){ρ₀ ρ₁ : Γ}{ρ₀₁ : (Γ ~C) ρ₀ ρ₁}{t₀ : A ρ₀}{t₁ : A ρ₁}(t₀₁ : (A ~T) ρ₀₁ t₀ t₁) → (A ~T) (SC Γ ρ₀₁) t₁ t₀
postulate TT  : {ℓ : Level}{Γ : Set ℓ}{ℓ' : Level}(A : Γ → Set ℓ'){ρ₀ ρ₁ ρ₂ : Γ}{ρ₀₁ : (Γ ~C) ρ₀ ρ₁}{ρ₁₂ : (Γ ~C) ρ₁ ρ₂}{t₀ : A ρ₀}{t₁ : A ρ₁}{t₂ : A ρ₂}(t₀₁ : (A ~T) ρ₀₁ t₀ t₁)(t₁₂ : (A ~T) ρ₁₂ t₁ t₂) → (A ~T) (TC Γ ρ₀₁ ρ₁₂) t₀ t₂
postulate coe : {ℓ : Level}{Γ : Set ℓ}{ℓ' : Level}(A : Γ → Set ℓ'){ρ₀ ρ₁ : Γ}(ρ₀₁ : (Γ ~C) ρ₀ ρ₁)(t₀ : A ρ₀) → A ρ₁
postulate coh : {ℓ : Level}{Γ : Set ℓ}{ℓ' : Level}(A : Γ → Set ℓ'){ρ₀ ρ₁ : Γ}(ρ₀₁ : (Γ ~C) ρ₀ ρ₁)(t₀ : A ρ₀) → (A ~T) ρ₀₁ t₀ (coe A ρ₀₁ t₀)

-- substitutions
postulate _~s : {ℓ : Level}{Γ : Set ℓ}{ℓ' : Level}{Δ : Set ℓ'}(δ : Γ → Δ){ρ₀ ρ₁ : Γ}(ρ₀₁ : (Γ ~C) ρ₀ ρ₁) → (Δ ~C) (δ ρ₀) (δ ρ₁)

-- terms
postulate _~t : {ℓ : Level}{Γ : Set ℓ}{ℓ' : Level}{A : Γ → Set ℓ'}(t : (ρ : Γ) → A ρ){ρ₀ ρ₁ : Γ}(ρ₀₁ : (Γ ~C) ρ₀ ρ₁) → (A ~T) ρ₀₁ (t ρ₀) (t ρ₁)

-- abbreviations
coe* : {ℓ : Level}{Γ : Set ℓ}{ℓ' : Level}(A : Γ → Set ℓ'){ρ₀ ρ₁ : Γ}(ρ₀₁ : (Γ ~C) ρ₀ ρ₁)(t₁ : A ρ₁) → A ρ₀
coe* {Γ = Γ} A ρ₀₁ t₁ = coe A (SC Γ ρ₀₁) t₁
coh* : {ℓ : Level}{Γ : Set ℓ}{ℓ' : Level}(A : Γ → Set ℓ'){ρ₀ ρ₁ : Γ}(ρ₀₁ : (Γ ~C) ρ₀ ρ₁)(t₁ : A ρ₁) → (A ~T) ρ₀₁ (coe* A ρ₀₁ t₁) t₁
coh* {Γ = Γ} A ρ₀₁ t₁ = ST A (coh A (SC Γ ρ₀₁) t₁)

-- equalities for contexts
postulate
  -- there is no rule for the empty context
  Σ~C : {ℓ : Level}{Γ : Set ℓ}{ℓ' : Level}{A : Γ → Set ℓ'}{ρ₀ ρ₁ : Σ Γ A} →
    ((Σ Γ A) ~C) ρ₀ ρ₁ →β σ ((Γ ~C) (pr₀ ρ₀) (pr₀ ρ₁)) λ ρ₀₁ → (A ~T) ρ₀₁ (pr₁ ρ₀) (pr₁ ρ₁)  
  -- the following are not the image of something from CITs, probably
  -- they are not needed
  Π~C : {ℓ : Level}{Γ : Set ℓ}{ℓ' : Level}{A : Γ → Set ℓ'}{t₀ t₁ : Π Γ A} →
    ((Π Γ A) ~C) t₀ t₁ →β ((ρ₀ ρ₁ : Γ)(ρ₀₁ : (Γ ~C) ρ₀ ρ₁) → (A ~T) ρ₀₁ (t₀ ρ₀) (t₁ ρ₁))
  Bool~C : {b₀ b₁ : Bool} → (Bool ~C) b₀ b₁ →β (if b₀ then if b₁ then ⊤p else ⊥p else if b₁ then ⊥p else ⊤p)
  Prop~C : {ℓ : Level}{a₀ a₁ : Prop ℓ} → ((Prop ℓ) ~C) a₀ a₁ →β LiftP ((a₀ → a₁) × (a₁ → a₀))
  El~C   : {ℓ : Level}{a : Prop ℓ}{t₀ t₁ : El a} → ((El a) ~C) t₀ t₁ →β LiftP ⊤p

{-# REWRITE Σ~C #-}

-- equalities for types
postulate
  -- substituted types. is this needed? NOTE: Γ and Δ have the same
  -- universe level, otherwise we cannot typecheck this
  []~T : {ℓ : Level}{Δ : Set ℓ}{ℓ' : Level}{A : Δ → Set ℓ'}{Γ : Set ℓ}{δ : Γ → Δ}
    {ρ₀ ρ₁ : Γ}{ρ₀₁ : (Γ ~C) ρ₀ ρ₁}{t₀ : A (δ ρ₀)}{t₁ : A (δ ρ₁)} →
    ((λ ρ → A (δ ρ)) ~T) ρ₀₁ t₀ t₁ →β (A ~T) ((δ ~s) ρ₀₁) t₀ t₁
  -- other types
  Π~T : {ℓ : Level}{Γ : Set ℓ}{ℓ' : Level}{A : Γ → Set ℓ'}{ℓ'' : Level}{B : Σ Γ A → Set ℓ''}
    {ρ₀ ρ₁ : Γ}{ρ₀₁ : (Γ ~C) ρ₀ ρ₁}{t₀ : Π (A ρ₀) λ x → B (ρ₀ , x)}{t₁ : Π (A ρ₁) λ x → B (ρ₁ , x)} →
    ((λ ρ → Π (A ρ) λ x → B (ρ , x)) ~T) ρ₀₁ t₀ t₁ →β
    ((x₀ : A ρ₀)(x₁ : A ρ₁)(x₀₁ : (A ~T) ρ₀₁ x₀ x₁) → (B ~T) (ρ₀₁ , x₀₁) (t₀ x₀) (t₁ x₁))
  Σ~T : {ℓ : Level}{Γ : Set ℓ}{ℓ' : Level}{A : Γ → Set ℓ'}{ℓ'' : Level}{B : Σ Γ A → Set ℓ''}
    {ρ₀ ρ₁ : Γ}{ρ₀₁ : (Γ ~C) ρ₀ ρ₁}{t₀ : Σ (A ρ₀) λ x → B (ρ₀ , x)}{t₁ : Σ (A ρ₁) λ x → B (ρ₁ , x)} →
    ((λ ρ → Σ (A ρ) λ x → B (ρ , x)) ~T) ρ₀₁ t₀ t₁ →β
    σ ((A ~T) ρ₀₁ (pr₀ t₀) (pr₀ t₁)) λ x₀₁ → (B ~T) (ρ₀₁ , x₀₁) (pr₁ t₀) (pr₁ t₁)
  Bool~T : {ℓ : Level}{Γ : Set ℓ}{ρ₀ ρ₁ : Γ}{ρ₀₁ : (Γ ~C) ρ₀ ρ₁}{t₀ t₁ : Bool} →
    ((λ _ → Bool) ~T) {ρ₀}{ρ₁} ρ₀₁ t₀ t₁ →β (if t₀ then if t₁ then LiftP ⊤p else LiftP ⊥p else if t₁ then LiftP ⊥p else LiftP ⊤p)
  Prop~T : {ℓ : Level}{Γ : Set ℓ}{ρ₀ ρ₁ : Γ}{ρ₀₁ : (Γ ~C) ρ₀ ρ₁}{ℓ' : Level}{a₀ a₁ : Prop ℓ'} →
    ((λ _ → Prop ℓ') ~T) {ρ₀}{ρ₁} ρ₀₁ a₀ a₁ →β LiftP ((a₀ → a₁) × (a₁ → a₀))
  Prop~T' : {ℓ : Level}{Γ : Set ℓ}{ρ₀ ρ₁ : Γ}{ρ₀₁ : (Γ ~C) ρ₀ ρ₁}{a₀ a₁ : Prop} →
    ((λ _ → Prop) ~T) {ρ₀}{ρ₁} ρ₀₁ a₀ a₁ →β LiftP ((El a₀ → a₁) × (El a₁ → a₀))
  El~T : {ℓ : Level}{Γ : Set ℓ}{ρ₀ ρ₁ : Γ}{ρ₀₁ : (Γ ~C) ρ₀ ρ₁}{ℓ' : Level}{a : Γ → Prop ℓ'}
    {t₀ : El (a ρ₀)}{t₁ : El (a ρ₁)} →
    ((λ ρ → El (a ρ)) ~T) ρ₀₁ t₀ t₁ →β LiftP ⊤p

-- {-# REWRITE []~T #-}
-- {-# REWRITE Π~T #-}
-- {-# REWRITE Σ~T #-}
{-# REWRITE Bool~T #-}
-- {-# REWRITE Prop~T #-}
{-# REWRITE Prop~T' #-}
{-# REWRITE El~T #-}

postulate
  -- coercion rules
  coe[] : {ℓ : Level}{Δ : Set ℓ}{ℓ' : Level}{A : Δ → Set ℓ'}{Γ : Set ℓ}{δ : Γ → Δ}
    {ρ₀ ρ₁ : Γ}{ρ₀₁ : (Γ ~C) ρ₀ ρ₁}{t₀ : A (δ ρ₀)} →
    coe (λ ρ → A (δ ρ)) ρ₀₁ t₀ →β coe A ((δ ~s) ρ₀₁) t₀
  coeΠ : {ℓ : Level}{Γ : Set ℓ}{ℓ' : Level}{A : Γ → Set ℓ'}{ℓ'' : Level}{B : Σ Γ A → Set ℓ''}
    {ρ₀ ρ₁ : Γ}{ρ₀₁ : (Γ ~C) ρ₀ ρ₁}{t₀ : Π (A ρ₀) λ x → B (ρ₀ , x)} →
    coe (λ ρ → Π (A ρ) λ x → B (ρ , x)) ρ₀₁ t₀ →β
    λ x₁ → coe B (ρ₀₁ , coh* A ρ₀₁ x₁) (t₀ (coe* A ρ₀₁ x₁))
  coeΣ : {ℓ : Level}{Γ : Set ℓ}{ℓ' : Level}{A : Γ → Set ℓ'}{ℓ'' : Level}{B : Σ Γ A → Set ℓ''}
    {ρ₀ ρ₁ : Γ}{ρ₀₁ : (Γ ~C) ρ₀ ρ₁}{t₀ : Σ (A ρ₀) λ x → B (ρ₀ , x)} →
    coe (λ ρ → Σ (A ρ) λ x → B (ρ , x)) ρ₀₁ t₀ →β
    (coe A ρ₀₁ (pr₀ t₀) , coe B (ρ₀₁ , coh A ρ₀₁ (pr₀ t₀)) (pr₁ t₀))
  coeBool : {ℓ : Level}{Γ : Set ℓ}{ρ₀ ρ₁ : Γ}{ρ₀₁ : (Γ ~C) ρ₀ ρ₁}{t₀ : Bool} →
    coe (λ _ → Bool) {ρ₀}{ρ₁} ρ₀₁ t₀ →β t₀
  coeProp : {ℓ : Level}{Γ : Set ℓ}{ρ₀ ρ₁ : Γ}{ρ₀₁ : (Γ ~C) ρ₀ ρ₁}{ℓ' : Level}{a₀ : Prop ℓ'} →
    coe (λ _ → Prop ℓ') {ρ₀}{ρ₁} ρ₀₁ a₀ →β a₀
  -- can't typecheck because Prop~T is not a valid rewrite rule
  -- coeEl : {ℓ : Level}{Γ : Set ℓ}{ρ₀ ρ₁ : Γ}{ρ₀₁ : (Γ ~C) ρ₀ ρ₁}{ℓ' : Level}{a : Γ → Prop ℓ'}{t₀ : El (a ρ₀)} →
  --   coe (λ ρ → El (a ρ)) ρ₀₁ t₀ →β {!pr₀ ((a ~t) ρ₀₁) t₀!}
  coeEl' : {ℓ : Level}{Γ : Set ℓ}{ρ₀ ρ₁ : Γ}{ρ₀₁ : (Γ ~C) ρ₀ ρ₁}{ℓ' : Level}{a : Γ → Prop}{t₀ : El (a ρ₀)} →
    coe (λ ρ → El (a ρ)) ρ₀₁ t₀ →β liftp (pr₀ (unliftP ((a ~t) ρ₀₁)) t₀)

-- {-# REWRITE coe[] #-}
-- {-# REWRITE coeΠ #-}
-- {-# REWRITE coeΣ #-}
{-# REWRITE coeBool #-}
-- {-# REWRITE coeProp #-}
-- {-# REWRITE coeEl #-}
{-# REWRITE coeEl' #-}
