all: mpc2019.pdf

mpc2019.pdf : mpc2019.tex abbrevs.tex b.bib include/*.tex
	latexmk -pdf mpc2019.tex

clean:
	rm -f *.aux *.bbl *.blg *.fdb_latexmk *.fls *.log *.out *.nav *.snm *.toc *.vtc *.ptb *~ mpc2019.pdf
