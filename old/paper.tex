\documentclass[a4paper]{article}

\usepackage{cite}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{hyperref}
\usepackage{proof}
\usepackage{mathtools}
\usepackage[utf8x]{inputenc}
\usepackage[greek,english]{babel}
\usepackage{scalerel}
\usepackage{tikz}
\usetikzlibrary{arrows}

\input{abbrevs.tex}
\newcommand{\0}{\mathsf{0}}
\newcommand{\1}{\mathsf{1}}
\newcommand{\sra}{\scalebox{0.7}{ $\ra$ }}
\newcommand{\stimes}{\scalebox{0.7}{ $\times$ }}
\renewcommand{\stop}{\scalebox{0.7}{$\top$}}
\newcommand{\sbot}{\scalebox{0.7}{$\bot$}}

\begin{document}

\title{Setoid type theory}
\date{\today}
\maketitle

%--------------------------------------------------------------------

\begin{abstract}
Intensional type theory lacks extensional concepts such as functional
extensionality, univalence (equality of isomorphic types) and quotient
types. These principles are however necessary for the practical
formalisation of mathematics. This is why variants of intensional type
theory have been designed which support these concepts. These replace
equality (identity type, with eliminator J) by a heterogeneous
equality type. Observational type theory uses John Major equality
while cubical type theory adds an interval pretype and equality of a
type is a function from the interval to that type. In this paper we
present another alternative: defining equality using as a logical
relation based on Bernardy's work on parametricity for dependent
types. Our theory justifies functional and propositional
extensionality. We call it setoid type theory as it can be seen as the
internalisation of the setoid model.
\end{abstract}

%--------------------------------------------------------------------

Things that would be nice:
\begin{itemize}
\item a translation into intensional type theory
\item a proof that a theory with axioms funext and propuniv has (weak)
  canonicity
\item justification of quotient types
\item normalisation/opsem
\end{itemize}

%--------------------------------------------------------------------

\section{Basic syntax}

We consider $\alpha$-equivalent terms equal and weakening is implicit.

We have 4 sorts: contexts, types and terms and (parallel)
substitutions. For now we omit the rules for substitutions, they are
standard. We stratify types into separate (predicative) levels, hence
the index $i$ for the typing judgement.
\[
\vdash \Gamma \hspace{7em} \Gamma \vdash_i A \hspace{7em} \Gamma \vdash t : A \hspace{7em} \sigma : \Gamma \Ra \Delta
\]

Notational convention: contexts: $\Gamma,\Delta$, types: $A,B,C$,
terms: $t,u,v,w$, variables: $x, y, z, f$, substitutions: $\sigma,
\delta, \nu$.

Syntax for the substitution calculus:
\[
\begin{gathered}
  \infer{\Gamma\vdash x:A}{(x:A)\in\Gamma}
\end{gathered}
\hspace{2em}
\begin{gathered}
 \infer{\vdash\cdot}{\vspace{1em}}
\end{gathered}
\hspace{2em}
\begin{gathered}
 \infer{\vdash\Gamma,x:A}{\vdash\Gamma && \Gamma \vdash_i A}
\end{gathered}
\]
Dependent function space:
\[
\begin{gathered}
  \infer{\Gamma\vdash_i (x:A)\ra B}{\Gamma\vdash_i A && \Gamma,x:A\vdash_i B}
\end{gathered}
\hspace{2em}
\begin{gathered}
 \infer{\Gamma\vdash\lambda x.t : (x:A)\ra B}{\Gamma,x:A\vdash t : B}
\end{gathered}
\]
\[
\begin{gathered}
 \infer{\Gamma\vdash t\,u : B[x \mapsto u]}{\Gamma\vdash t : (x:A)\ra B && \Gamma \vdash u : A}
\end{gathered}
\hspace{2em}
\begin{gathered}
  (\lambda x.t)\,u = t[x \mapsto u]
\end{gathered}
\hspace{2em}
\begin{gathered}
  \lambda x.t\,x = t
\end{gathered}
\]
Dependent sum:
\[
\begin{gathered}
  \infer{\Gamma\vdash_i (x:A)\times B}{\Gamma\vdash_i A && \Gamma,x:A\vdash_i B}
\end{gathered}
\hspace{2em}
\begin{gathered}
 \infer{\Gamma\vdash(u, v) : (x:A)\times B}{\Gamma\vdash u : A && \Gamma\vdash v : B[x \mapsto u]}
\end{gathered}
\]
\[
\begin{gathered}
 \infer{\Gamma\vdash \proj_0\,t : A}{\Gamma\vdash t : (x:A)\times B}
\end{gathered}
\hspace{2em}
\begin{gathered}
 \infer{\Gamma\vdash \proj_1\,t : B[x \mapsto \proj_0\,t]}{\Gamma\vdash t : (x:A)\times B}
\end{gathered}
\]
\[
\begin{gathered}
  \proj_0\,(u, v) = u
\end{gathered}
\hspace{2em}
\begin{gathered}
  \proj_1\,(u, v) = v
\end{gathered}
\hspace{2em}
\begin{gathered}
  (\proj_0\,t, \proj_1\,t) = t
\end{gathered}
\]
Booleans:
\[
\begin{gathered}
  \infer{\Gamma\vdash_0\Bool}{}
\end{gathered}
\hspace{2em}
\begin{gathered}
  \infer{\Gamma\vdash\true:\Bool}{}
\end{gathered}
\hspace{2em}
\begin{gathered}
  \infer{\Gamma\vdash\false:\Bool}{}
\end{gathered}
\]
\[
  \infer{\Gamma\vdash\IF\,t\,\THEN\,u\,\ELSE\,v : C[x\mapsto t]}{\Gamma,x:\Bool\vdash_i C && \Gamma\vdash t : \Bool && \Gamma\vdash u : C[x \mapsto \true] && \Gamma\vdash v : C[x\mapsto \false]}
\]
\[
\begin{gathered}
  \IF\,\true\,\THEN\,u\,\ELSE\,v = u
\end{gathered}
\hspace{2em}
\begin{gathered}
  \IF\,\false\,\THEN\,u\,\ELSE\,v = v
\end{gathered}
\]
Unit and empty types:
\[
\begin{gathered}
  \infer{\Gamma\vdash_0\top}{\vspace{1em}}
\end{gathered}
\hspace{2em}
\begin{gathered}
  \infer{\Gamma\vdash\tt : \top}{\vspace{1em}}
\end{gathered}
\hspace{2em}
\begin{gathered}
  \infer{t = \tt}{\Gamma\vdash t : \top}
\end{gathered}
\hspace{2em}
\begin{gathered}
  \infer{\Gamma\vdash_0\bot}{\vspace{1em}}
\end{gathered}
\hspace{2em}
\begin{gathered}
  \infer{\Gamma\vdash \abort\,t:C}{\Gamma\vdash t : \bot}
\end{gathered}
\]

%--------------------------------------------------------------------

\section{A universe of propositions}

TODO: we need separate constructors for these.

Notational convention: elements of $\Prop$: $a, b, c$.

\[
\begin{gathered}
  \infer{\Gamma\vdash_{i+1} \Prop_i}{\vspace{1em}}
\end{gathered}
\hspace{2em}
\begin{gathered}
  \infer{\Gamma\vdash_i \underline{a}}{\Gamma\vdash a : \Prop_i}
\end{gathered}
\]
This universe is closed under dependent function space, dependent sum,
unit and empty types. Note that everything is written with a smaller
font and underlining the elements of $\Prop$ makes them normal size
types. Also note that the domain of the function space need not be a
proposition.
\[
\begin{gathered}
  \infer{\Gamma\vdash (x:A) \sra b : \Prop_i}{\Gamma\vdash_i A && \Gamma,x:A\vdash b : \Prop_i}
\end{gathered}
\hspace{2em}
\begin{gathered}
  \underline{(x:A)\sra b} = (x:A)\ra \underline{b}
\end{gathered}
\]
\[
\begin{gathered}
  \infer{\Gamma\vdash (x:a)\stimes b : \Prop_i}{\Gamma\vdash a:\Prop_i && \Gamma,x:A\vdash b : \Prop_i}
\end{gathered}
\hspace{2em}
\begin{gathered}
  \underline{(x:a)\stimes b} = (x:\underline{a})\times\underline{b}
\end{gathered}
\]
\[
\begin{gathered}
  \infer{\Gamma\vdash\stop:\Prop_0}{}
\end{gathered}
\hspace{2em}
\begin{gathered}
  \underline{\stop} = \top
\end{gathered}
\hspace{2em}
\begin{gathered}
  \infer{\Gamma\vdash\sbot:\Prop_0}{}
\end{gathered}
\hspace{2em}
\begin{gathered}
  \underline{\sbot} = \bot
\end{gathered}
\]


%--------------------------------------------------------------------

\section{Heterogeneous equality}

We define heterogeneous equality following
\cite{bernardy12parametricity} as a logical relation. A difference is
that the target of the relations is $\Prop$.

Heterogeneous equality is defined in a different context to the
original type. The context $\Gamma^=$ contains two copies each
variable in $\Gamma$ and witnesses of their (heterogeneous)
equalities. The substitutions $\0$ and $\1$ project out the
corresponding componenents.
\[
\begin{gathered}
  \infer{\vdash\Gamma^=}{\vdash\Gamma}
\end{gathered}
\hspace{2em}
\begin{gathered}
  \0_\Gamma, \1_\Gamma : \Gamma^= \Ra \Gamma
\end{gathered}
\hspace{2em}
\begin{gathered}
  \infer{\Gamma^= \vdash \sim_A : A[\0]\ra A[\1]\ra\Prop_i}{\Gamma\vdash_i A}
\end{gathered}
\]
Terms are congruences: they preserve equality which is witnessed by
the following rule.
\[
\begin{gathered}
  \infer{\Gamma^= \vdash t^= : \underline{t[\0]\sim_A t[\1]}}{\Gamma\vdash t : A}
\end{gathered}
\]

Now we show how to define the above operations. On contexts:
\begin{alignat*}{5}
  & \cdot^= && = \cdot \\
  & (\Gamma,x:A)^= && = \Gamma^=, x_0:A[\0],x_1:A[\1],x_2:\underline{x_0 \sim_A x_1}
\end{alignat*}
The substitutions $\0$ and $\1$ are given as follows.
\begin{alignat*}{5}
  & \0_\cdot && = \epsilon  && \1_\cdot && = \epsilon  \\
  & \0_{\Gamma,x:A} && = (\0_\Gamma, x \mapsto x_0) \hspace{7em} && \1_{\Gamma,x:A} && = (\1_\Gamma, x \mapsto x_1) 
\end{alignat*}
Note that $\0_\Gamma$ is implicitly weakened in $(\0_\Gamma, x \mapsto
x_0)$, it has is a substitution $(\Gamma,x:A)^= \Ra \Gamma$ and
forgets about the last three variables in $(\Gamma,x:A)^=$.

Types:
\begin{alignat*}{5}
  & t_0 \sim_{(x:A)\ra B} t_1 && = (x_0:A[\0])(x_1:A[\1])(x_2:\underline{x_0\sim_A x_1})\sra t_0\,x_0 \sim_B t_1\,x_1 \\
  & t_0 \sim_{(x:A)\times B} t_1 && = (x_2 : \proj_0\,t_0 \sim_A \proj_0 t_1) \stimes (\proj_1\,t_0 \sim_B\, \proj_1 t_1)[{\scriptstyle x_0\mapsto \proj_0\,t_0, x_1 \mapsto \proj_0\,t_1}] \\
  & t_0 \sim_\Bool t_1 && = \IF\,t_0\,\THEN\,(\IF\,t_1\,\THEN\,\stop\,\ELSE\,\sbot)\,\ELSE\,(\IF\,t_1\,\THEN\,\sbot\,\ELSE\,\stop) \\
  & t_0 \sim_\top t_1 && = \stop \\
  & t_0 \sim_\bot t_1 && = \stop \\
  & a_0 \sim_{\Prop_i} a_1 && = (\underline{a_0} \sra a_1) \stimes (\underline{a_1} \sra a_0) \\
  & t_0 \sim_{\underline{a}} t_1 && = \stop
\end{alignat*}
Equality for functions is functional extensionality, equality for
pairs is a pair of equalities. ($\sim_B$ needs to be substituted
because $B$ is in context $\Gamma,x:A$, hence $\sim_B$ will be in the
context $\Gamma^=,x_0:A[\0],x_1:A[\1],x_2:x_0 \sim_A x_1$. $x_2$ is
given by the first component of the pair but $x_0$ and $x_1$ need to
be specified.) Booleans are equal if they are both $\true$ or both
$\false$. Elements of the unit and empty type are always equal. Two
propositions are equal if they are logically equivalent, two proofs of
the same proposition are always equal.

Terms:
\begin{alignat*}{5}
  & x^= && = x_2 \\
  & (\lambda x.t)^= && = \lambda x_0\,x_1\,x_2.t^= \\
  & (t\,u)^= && = t^=\,u[\0]\,u[\1]\,u^= \\
  & (u, v)^= && = (u^=,v^=) \\
  & (\proj_0\,t)^= && = \proj_0\,t^= \\
  & (\proj_1\,t)^= && = \proj_1\,t^= \\
  & \true^= && = \tt \\
  & \false^= && = \tt \\
  & (\IF\,t\,\THEN\,u\,\ELSE\,v)^= && = \IF\,t[\0]\,\THEN\,\big(\IF\,t[\1]\,\THEN\,u^=\,\ELSE\,\abort\,(t^=)\big) \\
  & && \hspace{4em} \ELSE\,\big(\IF\,t[\1]\,\THEN\,\abort\,(t^=)\,\ELSE\,v^=\big) \\
  & \tt^= && = \tt \\
  & (\abort\,t)^= && = \abort\,\big(t[\0]\big)
\end{alignat*}
To define the congruence $\blank^=$ operation for terms of type
$\Prop$, we need to extend the interpretation of types. We need that
given $\Gamma \vdash A$ in addition to $\sim_A$ we also have the
following functions in context $\Gamma^=$.
\begin{alignat*}{10}
  & \coe_A^0 && : A[\0] \ra A[\1] \hspace{4em} && \coh_A^0 : (x_0:A[\0]) \ra \underline{x_0\sim_A \coe_A^0\,x_0} \\
  & \coe_A^1 && : A[\1] \ra A[\0] \hspace{4em} && \coh_A^1 : (x_1:A[\1]) \ra \underline{\coe_A^1\,x_1\sim_A x_1}
\end{alignat*}
We only show how to define the 0 components, the 1 components are
symmetric. Coercion:
\begin{alignat*}{10}
  & \coe_{(x:A)\ra B}^0\,t_0 && = \lambda x_1.\coe_B^0[{\scriptstyle x_0\mapsto \coe_A^1\,x_1,x_2\mapsto \coh_A^1\,x_1}]\,\big(t_0\,(\coe_A^1\,x_1)\big) \\
  & \coe_{(x:A)\times B}^0\,t_0 && = \big(\coe_A^0\,(\proj_0\,t_0), \coe_B^0[{\scriptstyle x_0\mapsto \proj_0\,t_0,x_1\mapsto \coe_A^0\,(\proj_0\,t_0), x_2\mapsto \coh_A^0\,(\proj_0\,t_0)}]\, \\
  & && \hspace{9em} (\proj_1\,t_0)\big) \\
  & \coe_\Bool^0\,t_0 && = t_0 \\
  & \coe_\top^0\,t_0 && = t_0 \\
  & \coe_\bot^0\,t_0 && = t_0 \\
  & \coe_{\Prop_i}^0\,a_0 && = a_0 \\
  & \coe_{\underline{a}}^0\,t_0 && = \proj_0\,(a^=)
\end{alignat*}
Coherence:
\begin{alignat*}{10}
  & \coh_{(x:A)\ra B}^0\,t_0 && = \text{see section \ref{sec:cohpi}} \\
  & \coh_{(x:A)\times B}^0\,t_0 && = \big(\coh_A^0\,(\proj_0\,t_0), \coh_B^0[{\scriptstyle x_0\mapsto \proj_0\,t_0,x_1\mapsto \coe_A^0\,(\proj_0\,t_0), x_2\mapsto \coh_A^0\,(\proj_0\,t_0)}]\, \\
  & && \hspace{9em} (\proj_1\,t_0)\big) \\
  & \coh_\Bool^0\,t_0 && = \IF\,t_0\,\THEN\,\tt\,\ELSE\,\tt \\
  & \coh_\top^0\,t_0 && = \tt \\
  & \coh_\bot^0\,t_0 && = \tt \\
  & \coh_{\Prop_i}^0\,a_0 && = (\lambda x.x, \lambda x.x) \\
  & \coh_{\underline{a}}^0\,t_0 && = \tt
\end{alignat*}
Coherence for $(x:A)\ra B$ needs homogeneous equality which we will
define in section \ref{sec:hom}.

Now we can define congruence for terms in $\Prop$ using coercion of
the corresponding types.
\begin{alignat*}{10}
  & \big((x:A)\sra b\big)^= && = (\coe_{(x:A)\ra B}^0, \coe_{(x:A)\ra B}^1) \\
  & \big((x:A)\stimes b\big)^= && = (\coe_{(x:A)\times B}^0, \coe_{(x:A)\times B}^1) \\
  & \stop^= && = (\coe_\top^0, \coe_\top^1) \\
  & \sbot^= && = (\coe_\bot^0, \coe_\bot^1)
\end{alignat*}

%--------------------------------------------------------------------

\section{Homogeneous equality}
\label{sec:hom}

We add a substitution from a context to its lifted version which
duplicates everything. We define homogeneous equality using this
substitution. At the same time, we add a new term former $\refl$.
\[
\begin{gathered}
  \infer{\R_\Gamma : \Gamma \Ra \Gamma^=}{\vspace{1em}}
\end{gathered}
\hspace{2em}
\begin{gathered}
  \infer{\Gamma \vdash {\equiv_A} \,=\, {\sim_A\hspace{-0.2em}[\R_\Gamma]} : A \ra A \ra \Prop_i}{\Gamma \vdash_i A}
\end{gathered}
\hspace{2em}
\begin{gathered}
  \infer{\Gamma \vdash \refl\,t : \underline{t \equiv_A t}}{\Gamma \vdash t : A}
\end{gathered}
\]
$\R_\Gamma$ is given by induction on $\Gamma$:
\begin{alignat*}{5}
  & \R_\cdot && = \epsilon \\
  & \R_{\Gamma,x:A} && = (\R_\Gamma, x_0 \mapsto x, x_1 \mapsto x, x_2 \mapsto \refl\,x)
\end{alignat*}
There is a computation rule for $\refl$ and a naturality rule for
$\R$.
\[
\begin{gathered}
  \Gamma \vdash \refl\,t = t^=[\R_\Gamma] : \underline{t \equiv_A t}
\end{gathered}
\hspace{2em}
\begin{gathered}
  \infer{\R_\Delta\sigma = \sigma^=\R_\Gamma}{\sigma : \Gamma \Ra \Delta}
\end{gathered}
\]
We also need to add the result of the operation $\blank^=$ on
$\refl$. This is trivial as equality is trivial in $\Prop$.
\[
(\refl\,t)^= = \tt : \underline{\refl\,t[\0] \sim_{\underline{t \equiv_A t}} \refl\,t[\1]}
\]

Now we can justify the transport operation.
\[
\infer{\Gamma \vdash \coe_P^0[\R_\Gamma, x_0\mapsto u, x_1\mapsto v, x_2\mapsto w]\,t : P[x\mapsto v]}{\Gamma,x:A\vdash P && \Gamma \vdash w : u \equiv_A v && \Gamma\vdash t : P[x\mapsto u]}
\]
This has a propositional compuation rule at least (given by
$\coh$). The computation rule is definitional in case we have the
following (this can be viewed as a variant of the identity extension
lemma).
\[
\infer{\Gamma \vdash \coe_A^0[\R_\Gamma]\, t = t : A}{\Gamma \vdash t : A}
\]
TODO: we should check this by induction on $A$.

To justify J, we also need that singletons are contractible which we
prove as follows.
\[
\infer{\Gamma \vdash (w, \tt) : \underline{(u, \refl\,u) \equiv_{(x:A)\times \underline{u \equiv_A x}} (v, w)}}{\Gamma \vdash w : \underline{u \equiv_A v}}
\]

%--------------------------------------------------------------------

\section{Cubical structure}
\label{sec:cubical}

In this section we explain how a geometric structure of cubes arises
in the syntax given above. We don't define anything new, but the
explanations here are necessary to understand the next section.

A context $\cdot,x:A$ can be viewed as a point. A context
$(\cdot,x:A)^=$ can be viewed as two points $x_0,x_1$ and a line $x_2$
between them; this is called a 1-dimensional cube. A two dimensional
cube is given by the context ${(\cdot,x:A)^=}^=$: it containts 4
points, $x_{00}, x_{01}, x_{10}, x_{11}$, 4 lines and a filler of the
cube. \\
\begin{tikzpicture}
\node (x)  at (0,0) {$x$};
\node (x0) at (1,0) {$x_0$};
\node (x1) at (3,0) {$x_1$};
\draw[->]
(x0) edge node[above] {$x_2\vphantom{x_{x_x}}$} (x1);
\node (x00) at (4,0) {$x_{00}$};
\node (x01) at (4,2) {$x_{01}$};
\node (x10) at (6,0) {$x_{10}$};
\node (x11) at (6,2) {$x_{11}$};
\node (xbb) at (5,1) {$x_{22}$};
\draw[->]
(x00) edge node[below] {$x_{20}$} (x10)
(x00) edge node[left]  {$x_{02}$}   (x01)
(x10) edge node[right] {$x_{12}$}   (x11)
(x01) edge node[above] {$x_{21}$} (x11);
\end{tikzpicture} \\
If $A$ is not in the empty context then the cubes are heterogeneous,
e.g. unfolding the types in ${(\Gamma,x:A)^=}^=$ gives the following.
\begin{alignat*}{10}
& {\Gamma^=}^= && ,  && x_{00} : A[\0_\Gamma\0_{\Gamma^=}] && , x_{01} : A[\0_\Gamma\1_{\Gamma^=}] && , x_{02} : \underline{x_{00}\sim_{A[\0_\Gamma]} x_{01}} \\
& && , && x_{10} : A[\1_\Gamma\0_{\Gamma^=}] && , x_{11} : A[\1_\Gamma\1_{\Gamma^=}] && , x_{12} : \underline{x_{10}\sim_{A[\1_\Gamma]} x_{11}} \\
& && , && x_{20} : \underline{x_{00}{\sim_{A}\hspace{-0.2em}[\0_{\Gamma^=}]} x_{10}} && , x_{21} : \underline{x_{01}{\sim_{A}\hspace{-0.2em}[\1_{\Gamma^=}]} x_{11}}\, && , x_{22} : \underline{x_{20}\sim_{\underline{x_0\sim_A x_1}} x_{21}}
\end{alignat*}
There are four different types of points, each depending on a
different copy of $\Gamma$ in ${\Gamma^=}^=$ (there are four
projections from ${\Gamma^=}^=$ to $\Gamma$, namely
$\0_\Gamma\0_{\Gamma^=}$, $\0_\Gamma\1_{\Gamma^=}$,
$\1_\Gamma\0_{\Gamma^=}$, $\1_\Gamma\1_{\Gamma^=}$). The types of
lines are all different as well, corresponding to the points they
connect. Note that $\sim_{A[\0_\Gamma]}$ is different from
$\sim_A\hspace{-0.2em}[\0_\Gamma]$: in the first, the operator $\sim$
is applied on $A[\0_\Gamma]$ while in the second, the operator $\sim$
is applied on $A$ and then the result is substituted by
$\0_\Gamma$. The filler of the cube $x_{22}$ is trivial, its type is
$\top$.

The operations $\coe$ and $\coh$ can be seen as Kan fillers for
1-dimensional cubes: given a point, they extend it to a line.

We can also derive Kan-fillers for 2-dimensional cubes. For a type
$\Gamma\vdash A$ we have
$\Gamma,x_0:A[\0_\Gamma],x_1:A[\1_\Gamma]\vdash \underline{x_0\sim_A
  x_1}$. And coercion for this type gives
\[
(\Gamma,x_0:A[\0_\Gamma],x_1:A[\1_\Gamma])^= \vdash \coe_{\underline{x_0\sim_A x_1}}^0 :
(\underline{x_0\sim_A x_1})[\0] \ra
(\underline{x_0\sim_A x_1})[\1],
\]
which can be unfolded as
\begin{alignat*}{10}
  & {\Gamma^=}^= && ,  && x_{00} : A[\0_\Gamma\0_{\Gamma^=}] && , x_{01} : A[\0_\Gamma\1_{\Gamma^=}] && , x_{02} : \underline{x_{00}\sim_{A[\0_\Gamma]} x_{01}} \\
  & && , && x_{10} : A[\1_\Gamma\0_{\Gamma^=}] && , x_{11} : A[\1_\Gamma\1_{\Gamma^=}] && , x_{12} : \underline{x_{10}\sim_{A[\1_\Gamma]} x_{11}} \\
  & \vdash && \coe_{\underline{x_0\sim_A x_1}}^0 && : \underline{x_{00}{\sim_{A}\hspace{-0.2em}[\0_{\Gamma^=}]} x_{10}} && \ra \underline{x_{01}{\sim_{A}\hspace{-0.2em}[\1_{\Gamma^=}]} x_{11}}.
\end{alignat*}
This means that given the left hand square below we can produce the
right hand square. \\
\begin{tikzpicture}
\node (x00) at (0,0) {$x_{00}$};
\node (x01) at (0,2) {$x_{01}$};
\node (x10) at (3,0) {$x_{10}$};
\node (x11) at (3,2) {$x_{11}$};
\node (x22) at (1.5,1) {};
\draw[->] (x00) edge node[below] {$x_{20}$} (x10);
\draw[->] (x00) edge node[left]  {$x_{02}$} (x01);
\draw[->] (x10) edge node[right] {$x_{12}$} (x11);
\node (x00) at (5,0) {$x_{00}$};
\node (x01) at (5,2) {$x_{01}$};
\node (x10) at (8,0) {$x_{10}$};
\node (x11) at (8,2) {$x_{11}$};
\node (x22) at (6.5,1) {$\tt$};
\draw[->] (x00) edge node[below] {$x_{20}$} (x10);
\draw[dashed,->] (x01) edge node[above] {$\coe_{\underline{x_0\sim_A x_1}}^0\,x_{20}$} (x11);
\draw[->] (x00) edge node[left]  {$x_{02}$} (x01);
\draw[->] (x10) edge node[right] {$x_{12}$} (x11);
\end{tikzpicture} \\
The components in these squares have the following types (we omit the
type of the filler, it is constant $\top$). All of these types are
given in context ${\Gamma^=}^=$. \\
\begin{tikzpicture}
\node (x00) at (0,0) {$A[\0_\Gamma\0_{\Gamma^=}]$};
\node (x01) at (0,2) {$A[\0_\Gamma\1_{\Gamma^=}]$};
\node (x10) at (3,0) {$A[\1_\Gamma\0_{\Gamma^=}]$};
\node (x11) at (3,2) {$A[\1_\Gamma\1_{\Gamma^=}]$};
\node (x22) at (1.5,1) {};
\draw[->] (x00) edge node[below] {$\sim_A\hspace{-0.2em}[\0_\Gamma]$} (x10);
\draw[->] (x01) edge node[above] {$\sim_A\hspace{-0.2em}[\1_\Gamma]$} (x11);
\draw[->] (x00) edge node[left]  {$\sim_{A[\0_\Gamma]}$} (x01);
\draw[->] (x10) edge node[right] {$\sim_{A[\1_\Gamma]}$} (x11);
\end{tikzpicture} \\
The substitution $\R_{\Gamma,x:A}$ takes us from a context of points
into a context of lines. If we start with a context of lines
$({\Gamma,x:A})^=$, there are two ways to use $\R$ to get to a context
of squares: $\R_{(\Gamma,x:A)^=}$ and $(\R_{\Gamma,x:A})^=$. The last
squares in these substitutions are depicted below as the left and
right squares, respectively. \\
\begin{tikzpicture}
\node (x00) at (0,0) {$x_0$};
\node (x01) at (0,2) {$x_0$};
\node (x10) at (2,0) {$x_1$};
\node (x11) at (2,2) {$x_1$};
\node (x22) at (1,1) {$\refl\,x_2$};
\draw[->] (x00) edge node[below] {$x_2$} (x10);
\draw[->] (x01) edge node[above] {$x_2$} (x11);
\draw[->] (x00) edge node[left]  {$\refl\,x_0$} (x01);
\draw[->] (x10) edge node[right] {$\refl\,x_1$} (x11);
\node (x00) at (5,0) {$x_0$};
\node (x01) at (5,2) {$x_1$};
\node (x10) at (7,0) {$x_0$};
\node (x11) at (7,2) {$x_1$};
\node (x22) at (6,1) {$\tt$};
\draw[->] (x00) edge node[below] {$\refl\,x_0$} (x10);
\draw[->] (x01) edge node[above] {$\refl\,x_1$} (x11);
\draw[->] (x00) edge node[left]  {$x_2$} (x01);
\draw[->] (x10) edge node[right] {$x_2$} (x11);
\end{tikzpicture} \\
These squares are degenerate in on direction, that is, in this
direction, the lines are homogeneous equalities. We depict the types
for the above squares below. The context for these types is $\Gamma^=$. \\
\begin{tikzpicture}
\node (x00) at (0,0) {$A[\0_\Gamma]$};
\node (x01) at (0,2) {$A[\0_\Gamma]$};
\node (x10) at (2,0) {$A[\1_\Gamma]$};
\node (x11) at (2,2) {$A[\1_\Gamma]$};
\node (x22) at (1,1) {};
\draw[->] (x00) edge node[below] {$\sim_A$} (x10);
\draw[->] (x01) edge node[above] {$\sim_A$} (x11);
\draw[->] (x00) edge node[left]  {$\equiv_{A[\0_\Gamma]}$} (x01);
\draw[->] (x10) edge node[right] {$\equiv_{A[\1_\Gamma]}$} (x11);
\node (x00) at (5,0) {$A[\0_\Gamma]$};
\node (x01) at (5,2) {$A[\1_\Gamma]$};
\node (x10) at (7,0) {$A[\0_\Gamma]$};
\node (x11) at (7,2) {$A[\1_\Gamma]$};
\node (x22) at (6,1) {};
\draw[->] (x00) edge node[below] {$\equiv_{A[\0_\Gamma]}$} (x10);
\draw[->] (x01) edge node[above] {$\equiv_{A[\1_\Gamma]}$} (x11);
\draw[->] (x00) edge node[left]  {$\sim_A$} (x01);
\draw[->] (x10) edge node[right] {$\sim_A$} (x11);
\end{tikzpicture} \\
We also have Kan fillers for these degenerate squares, we just need to
substitute $\coe_{\underline{x_0\sim_A x_1}}^0$ with $\R_{\Gamma^=}$
or $(\R_{\Gamma})^=$, respectively.

Note that $\R$ allows us to express equalities which are homogeneous
on some dependencies and heterogeneous on others. Given a type
$\Gamma,x:A\vdash B$ we have the fully heterogeneous equality, the
homogeneous equality and one which is homogeneous on the dependencies
in $\Gamma$ but heterogeneous on $A$.
\begin{alignat*}{5}
  & \Gamma^=,x_0:A[\0_\Gamma],x_1:A[\1_\Gamma],x_2:\underline{x_0\sim_A x_1} && \vdash \sim_B && : B[\0_{\Gamma,x:A}] && \ra B[\1_{\Gamma,x:A}] && \ra \Prop \\
  & \Gamma.x:A && \vdash \equiv_B && : B && \ra B && \ra \Prop \\
  & \Gamma.x_0:A,x_1:A,x_2:\underline{x_0\sim x_1} && \vdash {\sim_B\hspace{-0.2em}[\R_\Gamma]} && : B[{\scriptstyle x\mapsto x_0}] && \ra B[{\scriptstyle x\mapsto x_1}] && \ra \Prop
\end{alignat*}

By induction on $\Gamma$ we have $i_\Gamma\R_\Gamma = \id_\Gamma$ for
$i=\0,\1$. Similarly we can prove that $\0$ and $\1$ are natural:
$\sigma i_\Gamma = i_\Delta\sigma^=$ for $i\in\{\0,\1\}$ and $\sigma :
\Gamma \Ra \Delta$. As a special case we get that $j_\Gamma
i_{\Gamma^=} = i_\Gamma (j_\Gamma)^=$ for $i,j\in\{\0,\1\}$.

%--------------------------------------------------------------------

\section{Coherence for dependent function space}
\label{sec:cohpi}

This is the last piece which is missing from our theory.

Given $\Gamma,x:A \vdash B$, we need
\[
\Gamma^= \vdash \coh_{(x:A)\ra B}^0 : \big(f_0 : (x:A[\0_\Gamma])\ra B[\0_\Gamma]\big) \ra \underline{f_0 \sim_{(x:A)\ra B} \coe_{(x:A)\ra B}^0\,f_0}.
\]
After unfolding the type and adding arguments, we get
\[
\coh_{(x:A)\ra B}^0\,f_0\,x_0\,x_1\,x_2 : f_0\,x_0 \sim_B \coe_B^0[{\scriptstyle x_0\mapsto \coe_A^1\,x_1,x_2\mapsto \coh_A^1\,x_1}]\,\big(f_0\,(\coe_A^1\,x_1)\big)
\]
in context $(\Gamma,x:A)^=,f_0 : (x:A[\0_\Gamma])\ra B[\0_\Gamma]$.

First we fill the left hand side square below from top to bottom by a
term $p$. The type of the left hand side square is the right hand side
square. Both squares are in context $(\Gamma,x:A)^=$. \\
\begin{tikzpicture}
\node (x00) at (0,0) {$\coe_A^1\,x_1$};
\node (x01) at (0,2) {$x_1$};
\node (x10) at (2,0) {$x_0$};
\node (x11) at (2,2) {$x_1$};
\node (x22) at (1,1) {$\tt$};
\draw[dashed,->] (x00) edge node[below] {$p$} (x10);
\draw[->] (x01) edge node[above] {$\refl\,x_1$} (x11);
\draw[->] (x00) edge node[left]  {$\coh_A^1\,x_1$} (x01);
\draw[->] (x10) edge node[right] {$x_2$} (x11);
\node (x00) at (5,0) {$A[\0_\Gamma]$};
\node (x01) at (5,2) {$A[\1_\Gamma]$};
\node (x10) at (7,0) {$A[\0_\Gamma]$};
\node (x11) at (7,2) {$A[\1_\Gamma]$};
\node (x22) at (6,1) {};
\draw[->] (x00) edge node[below] {$\equiv_{A[\0_\Gamma]}$} (x10);
\draw[->] (x01) edge node[above] {$\equiv_{A[\1_\Gamma]}$} (x11);
\draw[->] (x00) edge node[left]  {$\sim_A$} (x01);
\draw[->] (x10) edge node[right] {$\sim_A$} (x11);
\end{tikzpicture} \\
The term $p$ is given as as follows.
\begin{alignat*}{10}
  & (\Gamma,x:A)^= \vdash \\
  & \hspace{2em} p = \coe_{\underline{x_0 \sim_A x_1}}^1\big[(\R_\Gamma)^= && , x_{00} \mapsto \coe_A^1\,x_1 &&, x_{01}\mapsto x_1 && , x_{02}\mapsto \coh_A^1\,x_1 && \\
  & && , x_{10}\mapsto x_0 && , x_{11}\mapsto x_1 && , x_{12}\mapsto x_2 && \big]\,(\refl\,x_1) \\
  & \rlap{$\hspace{1em} : \coe_A^1\,x_1 \equiv_{A[\0_\Gamma]} x_0$}
\end{alignat*}

Next we fill the following left square from bottom to top by the term
$q$. This square is in context $(\Gamma,x:A)^=,f_0 :
(x:A[\0_\Gamma])\ra B[\0_\Gamma]$.
\[
t = \coe_B^0[{\scriptstyle x_0\mapsto \coe_A^1\,x_1,x_2\mapsto \coh_A^1\,x_1}]\,\big(f_0\,(\coe_A^1\,x_1)\big)
\]
\begin{tikzpicture}
\node (x00) at (0,0) {$f_0\,(\coe_A^1\,x_1)$};
\node (x01) at (0,2) {$f_0\,x_0$};
\node (x10) at (8,0) {$t$};
\node (x11) at (8,2) {$t$};
\node (x22) at (4,1) {$\tt$};
\draw[->] (x00) edge node[below] {$\coh_B^0[{\scriptstyle x_0\mapsto \coe_A^1\,x_1,x_2\mapsto \coh_A^1\,x_1}]\,\big(f_0\,(\coe_A^1\,x_1)\big)$} (x10);
\draw[dashed,->] (x01) edge node[above] {$q$} (x11);
\draw[->] (x00) edge node[left] {$\refl\,f_0\,(\coe_A^1\,x_1)\,x_0\,p$} (x01);
\draw[->] (x10) edge node[right] {$\refl\,t$} (x11);
\end{tikzpicture} \\
The type of this square is given by the following square (in the same
context). Note that the $x$ components that the $B$ types depend on
are given by the first square (which has $p$ at the bottom) but with
the dimensions swapped. \\
\begin{tikzpicture}
\node (x00) at (0,0) {$B[\0_\Gamma,{\scriptstyle x\mapsto \coe_A^1\,x_1}]$};
\node (x01) at (0,2) {$B[\0_{\Gamma,x:A}]$};
\node (x10) at (6,0) {$B[1_{\Gamma,x:A}]$};
\node (x11) at (6,2) {$B[1_{\Gamma,x:A}]$};
\node (x22) at (3,1) {};
\draw[->] (x00) edge node[below] {$\sim_B\hspace{-0.2em}[{\scriptstyle x_0 \mapsto \coe_A^1\,x_1, x_2\mapsto \coh_A^1\,x_1}]$} (x10);
\draw[->] (x01) edge node[above] {$\sim_B$} (x11);
\draw[->] (x00) edge node {$\sim_{B[\0_\Gamma]}\hspace{-0.2em}[{\scriptstyle\R_{\Gamma^=}, x_0\mapsto \coe_A^1\,x_1, x_1\mapsto x_0, x_2\mapsto p}]$} (x01);
\draw[->] (x10) edge node[right] {$\equiv_{B[\1_{\Gamma,x:A}]}$} (x11);
\end{tikzpicture} \\
The term $q$ is given as follows.
\begin{alignat*}{10}
  & \rlap{$(\Gamma,x:A)^=,f_0 : ((x:A)\ra B)[\0_\Gamma] \vdash$} \\
  & \hspace{2em} q = \coe_{\underline{y_0 \sim_B y_1}}^0 && \Big[\R_{\Gamma^=} && , x_{00} \mapsto \coe_A^1\,x_1 &&, x_{01}\mapsto x_0 && , x_{02}\mapsto p && \\
  & && && , x_{10}\mapsto x_1 && , x_{11}\mapsto x_1 && , x_{12}\mapsto \refl\,x_1 && \\
  & && && , x_{20}\mapsto \coh_A^1\,x_1 && , x_{21}\mapsto x_2 && , x_{22}\mapsto \tt && \\
  & && && , y_{00}\mapsto f_0\,(\coe_A^1\,x_1) && , y_{01}\mapsto f_0\,x_0 && , y_{02}\mapsto \refl\,\big(f_0\,(\coe_A^1\,x_1)\big)\,x_0\,p \\
  & && && , y_{10}\mapsto t && , y_{11}\mapsto t && , y_{12}\mapsto \refl\,t \\
  & && \,\, \rlap{$\Big]\,\Big(\coh_B^0[{\scriptstyle x_0\mapsto \coe_A^1\,x_1,x_2\mapsto \coh_A^1\,x_1}]\,\big(f_0\,(\coe_A^1\,x_1)\big)\Big)$} \\
  & \rlap{$\hspace{1em} : f_0\,x_0 \sim_B \coe_B^0[{\scriptstyle x_0 \mapsto \coe_A^1\,x_1, x_2\mapsto \coh_A^1\,x_1}]\, \big(f_0\,(\coe_A^1\,x_1)\big)$}
\end{alignat*}

With the help of $q$ we define $\coh_{(x:A)\ra B}$ as follows.
\begin{alignat*}{10}
  & \Gamma^= \vdash \coh_{(x:A)\ra B}^0 = \lambda f_0\,x_0\,x_1\,x_2 . q \\
  & \hspace{2em} : \big(f_0 : (x:A)\ra B)[\0_\Gamma]\big) \ra \underline{f_0 \sim_{(x:A)\ra B} \coe_{(x:A)\ra B}^0\,f_0}.
\end{alignat*}

%--------------------------------------------------------------------

\section{Groupoid type theory}

In groupoid type theory, we have two universes, $\Set$ and $\Prop$,
the former includes everything in the latter. Equality in $\Set$ is
isomorphism.

%--------------------------------------------------------------------

\bibliographystyle{plain}
\bibliography{b}

%--------------------------------------------------------------------

\end{document}
