{-# OPTIONS --rewriting --prop #-}

open import Agda.Primitive


------------------------------------------------------------------------------
-- The relevant stuffs start at wort "start". First, there is a small library.
------------------------------------------------------------------------------


Π : {ℓ ℓ' : Level} (A : Set ℓ) (B : A → Set ℓ') → Set (ℓ ⊔ ℓ')
Π A B = (x : A) → B x

record Σ {ℓ ℓ'} (A : Set ℓ) (B : A → Set ℓ') : Set (ℓ ⊔ ℓ') where
  constructor _,Σ_
  field
    proj₁ : A
    proj₂ : B proj₁
infixl 5 _,Σ_

open Σ public
record ⊤ : Set where
  constructor tt

data ⊥ : Set where

⊥-elim : ∀{ℓ}{A : Set ℓ} → ⊥ → A
⊥-elim ()

¬ : Set → Set
¬ A = A → ⊥


data Bool : Set where
  true false : Bool

if_then_else_ : ∀{ℓ}{C : Bool → Set ℓ}(b : Bool)(c : C true)(d : C false) → C b
if true then c else d = c
if false then c else d = d

record ⊤p : Prop where
  constructor ttp

data ⊥p : Prop where

abort⊥p : ∀{ℓ}{A : Set ℓ} → ⊥p → A
abort⊥p ()

abort⊥pp : ∀{ℓ}{A : Prop ℓ} → ⊥p → A
abort⊥pp ()

abortp : ∀{ℓ}{A : Prop ℓ} → ⊥ → A
abortp ()

record Σp {ℓ ℓ'} (A : Prop ℓ) (B : A → Prop ℓ') : Prop (ℓ ⊔ ℓ') where
  constructor _,p_
  field
    proj₁p : A
    proj₂p : B proj₁p
infixl 5 _,p_
open Σp public

_×p_ : ∀{ℓ ℓ'} → Prop ℓ → Prop ℓ' → Prop (ℓ ⊔ ℓ')
A ×p B = Σp A λ _ → B
infixl 4 _×p_

data _≡p_ {ℓ}{A : Prop ℓ} (x : A) : A → Set ℓ where
  reflp : x ≡p x


record LiftP {ℓ ℓ'}(A : Prop ℓ) : Prop (ℓ ⊔ ℓ') where
  constructor liftP
  field
    unliftP : A
open LiftP public

record El {ℓ}(A : Prop ℓ) : Set ℓ where
  constructor liftp
  field
    unliftp : A
open El public

_,p'_ : ∀{ℓ ℓ'}{A : Prop ℓ}{B : A → Prop ℓ'}(x : El A)(y : El (B (unliftp x))) → El (Σp A B)
_,p'_ x y = liftp (unliftp x ,p unliftp y)

 
postulate _→β_ : {ℓ : Level}{A : Set ℓ} → A → A → Set
{-# BUILTIN REWRITE _→β_ #-}

postulate _→β'_ : {ℓ : Level}{A : Prop ℓ} → A → A → Set
-- {-# BUILTIN REWRITE _→β'_ #-} no need to rewrite it is convertible by proof irrelevance


------------------------------------------------------------------------------
-- start!
------------------------------------------------------------------------------


-- Here are our postulates on equality. Then, there is two reduction rules (_≡_ and subst for each type former) 

postulate _≡_  : {ℓ : Level}{A : Set ℓ} (x : A) → A → Prop ℓ
postulate refl : {ℓ : Level}{A : Set ℓ} (x : A) → El (x ≡ x)
-- postulate sym  : {ℓ : Level}{A : Set ℓ}{t₀ t₁ : A} → El (t₀ ≡ t₁) → El (t₁ ≡ t₀)
-- postulate trs  : {ℓ : Level}{A : Set ℓ}{t₀ t₁ t₂ : A} → El (t₀ ≡ t₁) → El (t₁ ≡ t₂) → El (t₀ ≡ t₂)

postulate subst : {ℓ ℓ' : Level}{A : Set ℓ}(B : A → Set ℓ'){x₀ x₁ : A}(p : El (x₀ ≡ x₁)) → B x₀ → B x₁

sym  : {ℓ : Level}{A : Set ℓ}{t₀ t₁ : A} → El (t₀ ≡ t₁) → El (t₁ ≡ t₀)
sym {ℓ}{A}{t₀}{t₁} p = subst (λ x → El (x ≡ t₀)) p (refl t₀)

trs  : {ℓ : Level}{A : Set ℓ}{t₀ t₁ t₂ : A} → El (t₀ ≡ t₁) → El (t₁ ≡ t₂) → El (t₀ ≡ t₂)
trs {ℓ}{A}{t₀}{t₁}{t₂} p q = subst (λ x → El (t₀ ≡ x)) q p

postulate _≡[_]≡_  : {ℓ ℓ' : Level}{A : Set ℓ}{B : A → Set ℓ'}{x₀ x₁ : A} → B x₀ → El (x₀ ≡ x₁) → B x₁ → Prop (ℓ ⊔ ℓ')
postulate ≡[refl]≡ : {ℓ : Level}{A : Set ℓ}{B : A → Set ℓ}{x : A}{u v : B x} →  (_≡[_]≡_ {B = B} u (refl x) v) →β (u ≡ v)  -- rewrite rule
postulate sym[]    : {ℓ ℓ' : Level}{A : Set ℓ}(B : A → Set ℓ'){x₀ x₁ : A}{t₀ : B x₀}{t₁ : B x₁}(p : El (x₀ ≡ x₁))
                   → El (t₀ ≡[ p ]≡ t₁) → El (t₁ ≡[ sym p ]≡ t₀)
postulate trs[]    : {ℓ ℓ' : Level}{A : Set ℓ}(B : A → Set ℓ'){x₀ x₁ x₂ : A}{t₀ : B x₀}{t₁ : B x₁}{t₂ : B x₂}(p : El (x₀ ≡ x₁))(q : El (x₁ ≡ x₂))
                   → El (t₀ ≡[ p ]≡ t₁) → El (t₁ ≡[ q ]≡ t₂) → El (t₀ ≡[ trs p q ]≡ t₂)

postulate coh   : {ℓ ℓ' : Level}{A : Set ℓ}(B : A → Set ℓ'){x₀ x₁ : A}(p : El (x₀ ≡ x₁))(t₀ : B x₀) → El (t₀ ≡[ p ]≡ subst B p t₀)


-- less implicit args
_∶_≡[_]≡_ : {ℓ ℓ' : Level}{A : Set ℓ}(B : A → Set ℓ'){x₀ x₁ : A} → B x₀ → El (x₀ ≡ x₁) → B x₁ → Prop (ℓ ⊔ ℓ')
_∶_≡[_]≡_ B t₀ p t₁ = t₀ ≡[ p ]≡ t₁

-- type easier to understand for the typechecker
≡[refl]≡' : {ℓ : Level}{A : Set ℓ}{B : A → Set ℓ}{x : A}{p : El (x ≡ x)}{u v : B x} → (_≡[_]≡_ {B = B} u p v) →β (u ≡ v)
≡[refl]≡' = ≡[refl]≡
{-# REWRITE ≡[refl]≡' #-}


-- First derivable lemmas

coe≡[]≡ : {ℓ : Level}{A : Set ℓ}(B : A → Set ℓ){x₀ x₁ : A}(p : El (x₀ ≡ x₁)){t₀ : B x₀}{t₁ : B x₁} → El (subst B p t₀ ≡ t₁) → El (B ∶ t₀ ≡[ p ]≡ t₁)
coe≡[]≡ {A} B p {t₀}{t₁} q = trs[] B p (refl _) (coh B p t₀) q

≡[]≡coe : {ℓ : Level}{A : Set ℓ}(B : A → Set ℓ){x₀ x₁ : A}(p : El (x₀ ≡ x₁)){t₀ : B x₀}{t₁ : B x₁} → El (B ∶ t₀ ≡[ p ]≡ t₁) → El (subst B p t₀ ≡ t₁)
≡[]≡coe {A} B p {t₀}{t₁} q = trs[] B (sym {A} p) p (sym[] B p (coh B p t₀)) q

coh* : {ℓ ℓ' : Level}{A : Set ℓ}(B : A → Set ℓ'){x₀ x₁ : A}(p : El (x₀ ≡ x₁))(t₁ : B x₁) → El (subst B (sym p) t₁ ≡[ p ]≡ t₁)
coh* B p t₁ = sym[] B (sym p) (coh B (sym p) t₁)

J : {A : Set}{x : A}(B : (y : A) → El (x ≡ y) → Set){y : A}(p : El (x ≡ y)) → B x (refl x) → B y p
J {A}{x} B p t = subst (λ y → (p : El (x ≡ y)) → B y p) p (λ p₁ → t) p

substR : {A  : Set} (B : A → Set) {x : A} (t : B x) → El (t ≡ subst B (refl x) t)
substR B t = coh B (refl _) t

uip : {A : Set} {x y : A} (p q : x ≡ y) → p ≡p q
uip p q = reflp



-- Σ types

postulate ≡Σ : {ℓ ℓ' : Level}{A : Set ℓ}{B : A → Set ℓ'}(u v : Σ A B)
               → (u ≡ v) →β (Σp (proj₁ u ≡ proj₁ v) λ p → proj₂ u ≡[ liftp p ]≡ proj₂ v)
{-# REWRITE ≡Σ #-}

postulate ≡Σ' : {ℓ : Level}{A : Set ℓ}{x₀ x₁ : A}{x₀₁ : El (x₀ ≡ x₁)}{B : A → Set ℓ}{C : (x : A) → B x → Set ℓ}{t₀ : Σ (B x₀) (C x₀)}{t₁ : Σ (B x₁) (C x₁)}
                → (_≡[_]≡_ {B = λ x → Σ (B x) (C x)} t₀ x₀₁ t₁)
                  →β Σp (B ∶ proj₁ t₀ ≡[ x₀₁ ]≡ proj₁ t₁) λ y₀₁ → (λ z → C (proj₁ z) (proj₂ z)) ∶ proj₂ t₀ ≡[ x₀₁ ,p' liftp y₀₁ ]≡ proj₂ t₁
{-# REWRITE ≡Σ' #-}

postulate substΣ : {ℓ ℓ' : Level}{A : Set ℓ}{B : A → Set ℓ'}{C : (x : A) → B x → Set}{x₀ x₁ : A}{p : El (x₀ ≡ x₁)}{t : Σ (B x₀) (C x₀)}
                   → (subst (λ x → Σ (B x) (C x)) p t)
                     →β (subst B p (proj₁ t) ,Σ subst {A = Σ A B} (λ z → C (proj₁ z) (proj₂ z)) (p ,p' coh B p (proj₁ t)) (proj₂ t))
{-# REWRITE substΣ #-}
 

-- Π types

postulate ≡Π : {ℓ : Level}{A : Set ℓ}{B : A → Set ℓ}(f g : (x : A) → B x) →  (f ≡ g) →β ((x₀ x₁ : A) → (p : El (x₀ ≡ x₁)) → B ∶ f x₀ ≡[ p ]≡ g x₁)
{-# REWRITE ≡Π #-}

postulate ≡Π' : {ℓ : Level}{A : Set ℓ}{x₀ x₁ : A}{x₀₁ : El (x₀ ≡ x₁)}{B : A → Set ℓ}{C : (x : A) → B x → Set ℓ}{f₀ : Π (B x₀) (C x₀)}{f₁ : Π (B x₁) (C x₁)}
                → (_≡[_]≡_ {B = λ x → Π (B x) (C x)} f₀ x₀₁ f₁)
                  →β ((y₀ : B x₀)(y₁ : B x₁)(y₀₁ : El (B ∶ y₀ ≡[ x₀₁ ]≡ y₁)) → (λ z → C (proj₁ z) (proj₂ z)) ∶ f₀ y₀ ≡[ x₀₁ ,p' y₀₁ ]≡ f₁ y₁)
{-# REWRITE ≡Π' #-}

postulate substΠ : {ℓ ℓ' : Level}{A : Set ℓ}{B : A → Set ℓ'}{C : (x : A) → B x → Set}{x₀ x₁ : A}{p : El (x₀ ≡ x₁)}{f : Π (B x₀) (C x₀)}
                   → (subst (λ x → Π (B x) (C x)) p f)
                     →β (λ x → subst {A = Σ A B} (λ z → C (proj₁ z) (proj₂ z)) (p ,p' coh* B p x) (f (subst B (sym {A = A} p) x)))
{-# REWRITE substΠ #-}


apD : {ℓ : Level}{A : Set ℓ}{B : A → Set ℓ}(f : Π A B){x₀ x₁ : A}(p : El (x₀ ≡ x₁)) → El (B ∶ f x₀ ≡[ p ]≡ f x₁)
apD f p = liftp (unliftp (refl f) _ _ p)

funext : {ℓ : Level}{A : Set ℓ}{B : A → Set ℓ}(f g : Π A B)(p : (x : A) → El (f x ≡ g x)) → El (f ≡ g)
funext {B = B} f g p = liftp (λ x₀ x₁ q → unliftp (trs[] B q (refl x₁) (apD f q) (p x₁)))

-- Bool

postulate ≡Bool : {b₀ b₁ : Bool} → (b₀ ≡ b₁) →β (if b₀ then if b₁ then ⊤p else ⊥p else if b₁ then ⊥p else ⊤p)
{-# REWRITE ≡Bool #-}

postulate ≡Bool' : {ℓ : Level}{A : Set ℓ}{x₀ x₁ : A}{x₀₁ : El (x₀ ≡ x₁)}{b₀ b₁ : Bool}
                   → (_≡[_]≡_ {B = λ (x : A) → Bool} b₀ x₀₁ b₁)
                     →β LiftP (b₀ ≡ b₁)
{-# REWRITE ≡Bool' #-}

postulate substBool : {A : Set}{x₀ x₁ : A}{p : El (x₀ ≡ x₁)}{b : Bool}
                      → (subst {A = A} (λ x → Bool) p b) →β b
{-# REWRITE substBool #-}

-- -- Prop

postulate ≡Prop : (A B : Prop) →  (A ≡ B) →β LiftP ((A → B) ×p (B → A))
{-# REWRITE ≡Prop #-}

postulate ≡Prop' : {ℓ : Level}{A : Set ℓ}{x₀ x₁ : A}{x₀₁ : El (x₀ ≡ x₁)}(A₀ A₁ : Prop) →  (_≡[_]≡_ {B = λ (x : A) → Prop} A₀ x₀₁ A₁) →β LiftP ((A₀ → A₁) ×p (A₁ → A₀))
{-# REWRITE ≡Prop' #-}

postulate substProp : {A : Set}{x₀ x₁ : A}{p : El (x₀ ≡ x₁)}{P : Prop} → (subst {A = A} (λ _ → Prop) p P) →β P
{-# REWRITE substProp #-}

-- -- El

postulate ≡El : (A : Prop)(u v : El A) →  (u ≡ v) →β ⊤p
{-# REWRITE ≡El #-}

-- There is a universe level problem
postulate substEl : {A : Set₁}{x₀ x₁ : A}{x₀₁ : El (x₀ ≡ x₁)}{P : A → Prop}{t : El (P x₀)}
                  → (subst {A = A} (λ x → El (P x)) x₀₁ t) →β liftp (proj₁p (unliftP (unliftp (refl {_} {A → Prop} P) x₀ x₁ x₀₁)) (unliftp t))
{-# REWRITE substEl #-}


-- Other admissible lemmas

ap : {A : Set} {B : Set} (f : A → B) {x y : A} (p : El (x ≡ y)) → El (f x ≡ f y)
ap f {x = x} p = subst (λ x → El (_ ≡ f x)) p (refl (f x))

ap₂ : {A A' : Set} {B : Set} (f : A → A' → B) {x y  : A} {x' y' : A'} (p : El (x ≡ y)) (q : El (x' ≡ y')) → El (f x x' ≡ f y y')
ap₂ f p q = subst (λ x' → El (_ ≡ f _ x')) q (subst (λ x → El (_ ≡ f x _)) p (refl (f _ _)))

transportD : {A : Set} {B : A → Set} (f : (a : A) → B a) {x y : A} (p : El (x ≡ y)) → El (subst B p (f x) ≡ f y)
transportD {A}{B} f {x} p = trs[] B (sym {A = A} p) p (coh* B (sym {A = A} p) (f x)) (apD f p)

substconst : {A : Set} (B : Set) {x y : A} (p : El (x ≡ y)) (b : B) → El ((subst (λ _ → B) {x} p) b ≡ b)
substconst B {x} p b = J {x = x} (λ x' p → El (subst _ {x} p b ≡ b)) p (sym {A = B} (substR (λ _ → B) {x} b))

subst₂ : {A  A' : Set} (B : A → A' → Set) {x y  : A} {x' y' : A'} (p : El (x ≡ y)) (p' : El (x' ≡ y')) → B x x' → B y y'
subst₂ B p q b = subst (λ x' → B _ x') q (subst (λ x → B x _) p b) 

ap10 : {A : Set} {B : Set} {f g : A → B} {x : A} (p : El (f ≡ g)) → El (f x ≡ g x)
ap10 {f = f} {g} {x} p = liftp (unliftp p x x (refl x))


-- neg : Bool -> Bool
-- neg true = false
-- neg false = true

-- neg≠b : (b : Bool) → neg b ≡ b → ⊥
-- neg≠b true ()
-- neg≠b false ()


-- neg≠b' : (b₁ b₂ : Bool) → (b₁ ≡ b₂ → ⊥p) → neg b₁ ≡ b₂
-- neg≠b' true true f = f ttp
-- neg≠b' true false f = ttp
-- neg≠b' false true f = ttp
-- neg≠b' false false f = f ttp


-- ≡Σ'' : {ℓ : Level}{A : Set ℓ}{B : A → Set ℓ}(u v : Σ A B)(p : proj₁ u ≡ proj₁ v)(q : subst B p (proj₂ u) ≡ proj₂ v) → u ≡ v
-- ≡Σ'' {B = B} u v p q = p ,p coe≡[]≡ B p q

-- pr1≡ : {ℓ ℓ' : Level}{A : Set ℓ}(B : A → Set ℓ'){u v : Σ A B}(p : u ≡ v) → proj₁ u ≡ proj₁ v
-- pr1≡ B p = proj₁p p

-- pr2≡ : {ℓ : Level}{A : Set ℓ}(B : A → Set ℓ){u v : Σ A B}(p : u ≡ v) → subst B (pr1≡ B p) (proj₂ u) ≡ proj₂ v
-- pr2≡ B p = ≡[]≡coe B (pr1≡ B p) (proj₂p p)



















-- postulate reflΣ : {ℓ ℓ' : Level}{A : Set ℓ}{B : A → Set ℓ'}(u : Σ A B) →  (refl u) →β'(refl (proj₁ u) ,p refl (proj₂ u))
-- postulate cohΣ : {ℓ ℓ' : Level}{A : Set ℓ}{B : A → Set ℓ'}{C : (x : A) → B x → Set}{x₀ x₁ : A}{p : x₀ ≡ x₁}{t : Σ (B x₀) (C x₀)}
               -- → (coh (λ x → Σ (B x) (C x)) p t) →β' {!!}
-- postulate ≡Π : {ℓ ℓ' : Level}{A : Set ℓ}{B : A → Set ℓ'}(f g : Π {ℓ}{ℓ'} A B) →  (f ≡ g) →β ((x₀ x₁ : A) → (p : x₀ ≡ x₁) → _≡[_]≡_ {B = B} (f x₀) p (g x₁))
