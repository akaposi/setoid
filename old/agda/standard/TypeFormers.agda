{-# OPTIONS --without-K #-}

module standard.TypeFormers where

open import HoTT
open import standard.Core

-- TODO: move this to some lib

caseBool : {B : Bool → Set}(b : Bool) → B true → B false → B b
caseBool true  t f = t
caseBool false t f = f

infixl 5 _$$_

-- function space

Π' : {Γ : Con}(A : Ty Γ)(B : Ty (Γ ▶ A)) → Ty Γ
(Π' A B) γ = ((x : fst (A γ)) → fst (B (γ , x))) , Π-level λ x → snd (B (γ , x))

lam : {Γ : Con}{A : Ty Γ}{B : Ty (Γ ▶ A)} → Tm (Γ ▶ A) B → Tm Γ (Π' {Γ} A B)
(lam t) γ  = λ x → t (γ , x)

app : {Γ : Con}{A : Ty Γ}{B : Ty (Γ ▶ A)} → Tm Γ (Π' {Γ} A B) → Tm (Γ ▶ A) B
(app t) (γ , x) = t γ x

Π[] : {Γ Δ : Con}{σ : Tms Γ Δ}{A : Ty Δ}{B : Ty (Δ ▶ A)}
    → _[_]T {Γ}{Δ}(Π' {Δ} A B) σ == (Π' {Γ} (_[_]T {Γ}{Δ} A σ) (_[_]T {Γ ▶ _[_]T {Γ}{Δ} A σ}{Δ ▶ A} B (_^_ {Γ}{Δ} σ A)))
Π[] = idp

lam[]
  : {Γ Δ : Con}{σ : Tms Γ Δ}{A : Ty Δ}{B : Ty (Δ ▶ A)}{t : Tm (Δ ▶ A) B}
  →  _[_]t {Γ}{Δ}{Π' {Δ} A B}(lam {Δ}{A}{B} t) σ
  == lam {Γ}{_[_]T {Γ}{Δ} A σ}{_[_]T {Γ ▶ _[_]T {Γ}{Δ} A σ}{Δ ▶ A} B (_^_ {Γ}{Δ} σ A)}
         (_[_]t {Γ ▶ _[_]T {Γ}{Δ} A σ}{Δ ▶ A}{B} t (_^_ {Γ}{Δ} σ A))
lam[] = idp

Πβ : {Γ : Con}{A : Ty Γ}{B : Ty (Γ ▶ A)}{t : Tm (Γ ▶ A) B} → app {Γ}{A}{B}(lam {Γ}{A}{B} t) == t
Πβ = idp

Πη : {Γ : Con}{A : Ty Γ}{B : Ty (Γ ▶ A)}{t : Tm Γ (Π' {Γ} A B)} → lam {Γ}{A}{B} (app {Γ}{A}{B} t) == t
Πη = idp

-- abbreviations

_$$_
  : {Γ : Con}{A : Ty Γ}{B : Ty (Γ ▶ A)}(t : Tm Γ (Π' {Γ} A B))(u : Tm Γ A)
  → Tm Γ (_[_]T {Γ}{Γ ▶ A} B (<_> {Γ}{A} u))
_$$_ {Γ}{A}{B} t u = _[_]t {Γ}{Γ ▶ A}{B}(app {Γ}{A}{B} t)(<_> {Γ}{A} u)

-- sigma

Σ' : {Γ : Con}(A : Ty Γ)(B : Ty (Γ ▶ A)) → Ty Γ
(Σ' A B) γ = (Σ (fst (A γ)) λ x → fst (B (γ , x))) , Σ-level (snd (A γ)) λ x → snd (B (γ , x))

_,'_
  : {Γ : Con}{A : Ty Γ}{B : Ty (Γ ▶ A)}(t : Tm Γ A)(u : Tm Γ (_[_]T {Γ}{Γ ▶ A} B (<_> {Γ}{A} t)))
  → Tm Γ (Σ' {Γ} A B)
(t ,' u) γ = t γ , u γ

proj₁ : {Γ : Con}{A : Ty Γ}{B : Ty (Γ ▶ A)}(w : Tm Γ (Σ' {Γ} A B)) → Tm Γ A
(proj₁ w) γ = fst (w γ)

proj₂
  : {Γ : Con}{A : Ty Γ}{B : Ty (Γ ▶ A)}(w : Tm Γ (Σ' {Γ} A B))
  → Tm Γ (_[_]T {Γ}{Γ ▶ A} B (<_> {Γ}{A} (proj₁ {Γ}{A}{B} w)))
(proj₂ w) γ = snd (w γ)

Σ[] : {Γ Δ : Con}{σ : Tms Γ Δ}{A : Ty Δ}{B : Ty (Δ ▶ A)}
    → _[_]T {Γ}{Δ}(Σ' {Δ} A B) σ == Σ' {Γ}(_[_]T {Γ}{Δ} A σ)(_[_]T {Γ ▶ _[_]T {Γ}{Δ} A σ}{Δ ▶ A} B (_^_ {Γ}{Δ} σ A))
Σ[] = idp

,Σ[]
  : {Γ Δ : Con}{σ : Tms Γ Δ}{A : Ty Δ}{B : Ty (Δ ▶ A)}{t : Tm Δ A}
    {u : Tm Δ (_[_]T {Δ}{Δ ▶ A} B (<_> {Δ}{A} t))}
  →  _[_]t {Γ}{Δ}{Σ' {Δ} A B}(_,'_ {Δ}{A}{B} t u) σ
  == _,'_ {Γ}{_[_]T {Γ}{Δ} A σ}{_[_]T {Γ ▶ _[_]T {Γ}{Δ} A σ}{Δ ▶ A} B (_^_ {Γ}{Δ} σ A)}
          (_[_]t {Γ}{Δ}{A} t σ) (_[_]t {Γ}{Δ}{_[_]T {Δ}{Δ ▶ A} B (<_> {Δ}{A} t)} u σ)
,Σ[] = idp

Σβ₁
  : {Γ : Con}{A : Ty Γ}{B : Ty (Γ ▶ A)}{t : Tm Γ A}{u : Tm Γ (_[_]T {Γ}{Γ ▶ A} B (<_> {Γ}{A} t))}
  → proj₁ {Γ}{A}{B}(_,'_ {Γ}{A}{B} t u) == t
Σβ₁ = idp

Σβ₂
  : {Γ : Con}{A : Ty Γ}{B : Ty (Γ ▶ A)}{t : Tm Γ A}{u : Tm Γ (_[_]T {Γ}{Γ ▶ A} B (<_> {Γ}{A} t))}
  → proj₂ {Γ}{A}{B}(_,'_ {Γ}{A}{B} t u) == u
Σβ₂ = idp

Ση
  : {Γ : Con}{A : Ty Γ}{B : Ty (Γ ▶ A)}{w : Tm Γ (Σ' {Γ} A B)}
  → _,'_ {Γ}{A}{B}(proj₁ {Γ}{A}{B} w) (proj₂ {Γ}{A}{B} w) == w
Ση = idp

-- boolean

Bool' : {Γ : Con} → Ty Γ
Bool' γ = Bool , Bool-level

true' : {Γ : Con} → Tm Γ (Bool' {Γ})
true' γ = true

false' : {Γ : Con} → Tm Γ (Bool' {Γ})
false' γ = false

ifthenelse
  : {Γ : Con}(C : Ty (Γ ▶ Bool' {Γ}))(t : Tm Γ (Bool' {Γ}))
    (u : Tm Γ (_[_]T {Γ}{Γ ▶ Bool' {Γ}} C (<_> {Γ}{Bool' {Γ}} (true' {Γ}))))
    (v : Tm Γ (_[_]T {Γ}{Γ ▶ Bool' {Γ}} C (<_> {Γ}{Bool' {Γ}} (false' {Γ}))))
  → Tm Γ (_[_]T {Γ}{Γ ▶ Bool' {Γ}} C (<_> {Γ}{Bool' {Γ}} t))
(ifthenelse C t u v) γ = caseBool {λ x → fst (C (γ , x))}(t γ)(u γ)(v γ)

Bool[] : {Γ Δ : Con}{σ : Tms Γ Δ} → _[_]T {Γ}{Δ}(Bool' {Δ}) σ == Bool' {Γ}
Bool[] = idp

true[] : {Γ Δ : Con}{σ : Tms Γ Δ} → _[_]t {Γ}{Δ}{Bool' {Δ}}(true' {Δ}) σ == true' {Γ}
true[] = idp

false[] : {Γ Δ : Con}{σ : Tms Γ Δ} → _[_]t {Γ}{Δ}{Bool' {Δ}}(false' {Δ}) σ == false' {Γ}
false[] = idp

ifthenelse[]
  : {Γ Δ : Con}{σ : Tms Γ Δ}{C : Ty (Δ ▶ Bool' {Δ})}{t : Tm Δ (Bool' {Δ})}
    {u : Tm Δ (_[_]T {Δ}{Δ ▶ Bool' {Δ}} C (<_> {Δ}{Bool' {Δ}} (true' {Δ})))}
    {v : Tm Δ (_[_]T {Δ}{Δ ▶ Bool' {Δ}} C (<_> {Δ}{Bool' {Δ}} (false' {Δ})))}
  →  _[_]t {Γ}{Δ}{_[_]T {Δ}{Δ ▶ Bool' {Δ}} C (<_> {Δ}{Bool' {Δ}} t)}(ifthenelse {Δ} C t u v) σ
  == ifthenelse {Γ}(_[_]T {Γ ▶ Bool' {Γ}}{Δ ▶ Bool' {Δ}} C (_^_ {Γ}{Δ} σ (Bool' {Δ})))
                (_[_]t {Γ}{Δ}{Bool' {Δ}} t σ)
                (_[_]t {Γ}{Δ}{_[_]T {Δ}{Δ ▶ Bool' {Δ}} C (<_> {Δ}{Bool' {Δ}} (true'  {Δ}))} u σ)
                (_[_]t {Γ}{Δ}{_[_]T {Δ}{Δ ▶ Bool' {Δ}} C (<_> {Δ}{Bool' {Δ}} (false' {Δ}))} v σ)
ifthenelse[] = idp

trueβ
  : {Γ : Con}{C : Ty (Γ ▶ Bool' {Γ})}
    {u : Tm Γ (_[_]T {Γ}{Γ ▶ Bool' {Γ}} C (<_> {Γ}{Bool' {Γ}} (true' {Γ})))}
    {v : Tm Γ (_[_]T {Γ}{Γ ▶ Bool' {Γ}} C (<_> {Γ}{Bool' {Γ}} (false' {Γ})))}
  → ifthenelse {Γ} C (true' {Γ}) u v == u
trueβ = idp

falseβ
  : {Γ : Con}{C : Ty (Γ ▶ Bool' {Γ})}
    {u : Tm Γ (_[_]T {Γ}{Γ ▶ Bool' {Γ}} C (<_> {Γ}{Bool' {Γ}} (true' {Γ})))}
    {v : Tm Γ (_[_]T {Γ}{Γ ▶ Bool' {Γ}} C (<_> {Γ}{Bool' {Γ}} (false' {Γ})))}
  → ifthenelse {Γ} C (false' {Γ}) u v == v
falseβ = idp

-- one element type

⊤' : {Γ : Con} → Ty Γ
⊤' γ = ⊤ , Unit-level

tt' : {Γ : Con} → Tm Γ (⊤' {Γ})
tt' γ = tt

⊤[] : {Γ Δ : Con}{σ : Tms Γ Δ} → _[_]T {Γ}{Δ} (⊤' {Δ}) σ == ⊤' {Γ}
⊤[] = idp

⊤η : {Γ : Con}{t : Tm Γ (⊤' {Γ})} → t == tt' {Γ}
⊤η = idp

-- empty type

⊥' : {Γ : Con} → Ty Γ
⊥' γ = ⊥ , Empty-level

abort : {Γ : Con}(C : Ty Γ) → Tm Γ (⊥' {Γ}) → Tm Γ C
(abort C t) γ = Empty-elim (t γ)

⊥[] : {Γ Δ : Con}{σ : Tms Γ Δ} → _[_]T {Γ}{Δ} (⊥' {Δ}) σ == ⊥' {Γ}
⊥[] = idp

abort[]
  : {Γ Δ : Con}{σ : Tms Γ Δ}{C : Ty Δ}{t : Tm Δ (⊥' {Δ})}
  → _[_]t {Γ}{Δ}{C}(abort {Δ} C t) σ == abort {Γ} (_[_]T {Γ}{Δ} C σ) (_[_]t {Γ}{Δ}{⊥' {Δ}} t σ)
abort[] = idp
