{-# OPTIONS --without-K #-}

-- the standard model of setoid type theory

module standard.Core where

open import Agda.Primitive

open import HoTT

-- open import Lib

infixl 65 _▶_
infixl 67 _[_]T
infixl 65 _,s_
infixl 66 _○_
infixl 68 _[_]t
infix  64 <_>
infixl 65 _^_

-- substitution calculus (CwF)

Con : Type (lsucc lzero)
Con = hSet lzero

Ty : (Γ : Con) → Type (lsucc lzero)
Ty (Γ ,  Γset) = Γ → hSet lzero

Tms : (Γ Δ : Con) → Type lzero
Tms (Γ , Γset) (Δ , Δset) = Γ → Δ

Tm : (Γ : Con)(A : Ty Γ) → Type lzero
Tm (Γ , Γset) A = (γ : Γ) → fst (A γ)

⊤set : is-set ⊤
⊤set = has-level-in λ { tt tt → has-level-in λ { idp idp → has-level-in (idp , λ { idp → idp }) } }

∙ : Con -- \.
∙ = ⊤ , ⊤set

_▶_ : (Γ : Con)(A : Ty Γ) → Con -- \T3
(Γ , Γset) ▶ A = (Σ Γ λ γ → fst (A γ)) , Σ-level Γset λ γ → snd (A γ)

_[_]T : {Γ Δ : Con}(A : Ty Δ)(σ : Tms Γ Δ) → Ty Γ
A [ σ ]T = A ∘ σ

id : {Γ : Con} → Tms Γ Γ
id γ = γ

_○_ : {Γ Θ Δ : Con}(σ : Tms Θ Δ)(ν : Tms Γ Θ) → Tms Γ Δ -- \ci2
σ ○ ν = σ ∘ ν

ε : {Γ : Con} → Tms Γ ∙
ε γ = tt

_,s_ : {Γ Δ : Con}{A : Ty Δ}(σ : Tms Γ Δ)(t : Tm Γ (_[_]T {Γ}{Δ} A σ)) → Tms Γ (Δ ▶ A)
(σ ,s t) γ = σ γ , t γ

π₁ : {Γ Δ : Con}{A : Ty Δ}(σ : Tms Γ (Δ ▶ A)) → Tms Γ Δ
(π₁ σ) γ = fst (σ γ)

_[_]t : {Γ Δ : Con}{A : Ty Δ}(t : Tm Δ A)(σ : Tms Γ Δ) → Tm Γ (_[_]T {Γ}{Δ} A σ)
(t [ σ ]t) γ = t (σ γ)

π₂ : {Γ Δ : Con}{A : Ty Δ}(σ : Tms Γ (Δ ▶ A)) → Tm Γ (_[_]T {Γ}{Δ} A (π₁ {Γ}{Δ}{A} σ))
(π₂ σ) γ = snd (σ γ)

[id]T : {Γ : Con}{A : Ty Γ} → _[_]T {Γ}{Γ} A (id {Γ}) == A
[id]T = idp

[][]T : {Γ Θ Δ : Con}{A : Ty Δ}{σ : Tms Θ Δ}{ν : Tms Γ Θ}
        → _[_]T {Γ}{Θ} (_[_]T {Θ}{Δ} A σ) ν == _[_]T {Γ}{Δ} A (_○_ {Γ}{Θ}{Δ} σ ν)
[][]T = idp

idl : {Γ Δ : Con}{σ : Tms Γ Δ} → _○_ {Γ}{Δ}{Δ} (id {Δ}) σ == σ
idl = idp

idr : {Γ Δ : Con}{σ : Tms Γ Δ} → _○_ {Γ}{Γ}{Δ} σ (id {Γ}) == σ
idr = idp

ass : {Γ Θ Ω Δ : Con}{σ : Tms Ω Δ}{ν : Tms Θ Ω}{δ : Tms Γ Θ}
      → _○_ {Γ}{Θ}{Δ}(_○_ {Θ}{Ω}{Δ} σ ν) δ == _○_ {Γ}{Ω}{Δ} σ (_○_ {Γ}{Θ}{Ω} ν δ)
ass = idp

π₁β : {Γ Δ : Con}{A : Ty Δ}{σ : Tms Γ Δ}{t : Tm Γ (_[_]T {Γ}{Δ} A σ)}
    → (π₁ {Γ}{Δ}{A}(_,s_ {Γ}{Δ}{A} σ t)) == σ
π₁β = idp

πη : {Γ Δ : Con}{A : Ty Δ}{σ : Tms Γ (Δ ▶ A)}
   → _,s_ {Γ}{Δ}{A}(π₁ {Γ}{Δ}{A} σ)(π₂ {Γ}{Δ}{A} σ) == σ
πη = idp

εη : {Γ : Con}{σ : Tms Γ ∙} → σ == ε {Γ}
εη = idp

π₂β : {Γ Δ : Con}{A : Ty Δ}{σ : Tms Γ Δ}{t : Tm Γ (_[_]T {Γ}{Δ} A σ)}
    → (π₂ {Γ}{Δ}{A}(_,s_ {Γ}{Δ}{A} σ t)) == t
π₂β = idp

-- abbreviations

wk : {Γ : Con}{A : Ty Γ} → Tms (Γ ▶ A) Γ
wk {Γ}{A} = π₁ {Γ ▶ A}{Γ}{A}(id {Γ ▶ A})

wk2 : {Γ : Con}{A : Ty Γ}{B : Ty (Γ ▶ A)} → Tms (Γ ▶ A ▶ B) Γ
wk2 {Γ}{A}{B} = _○_ {Γ ▶ A ▶ B}{Γ ▶ A}{Γ}(wk {Γ}{A})(wk {Γ ▶ A}{B})

wk3 : {Γ : Con}{A : Ty Γ}{B : Ty (Γ ▶ A)}{C : Ty (Γ ▶ A ▶ B)} → Tms (Γ ▶ A ▶ B ▶ C) Γ
wk3 {Γ}{A}{B}{C} = _○_ {Γ ▶ A ▶ B ▶ C}{Γ ▶ A}{Γ}(wk {Γ}{A})(wk2 {Γ ▶ A}{B}{C})

wk4 : {Γ : Con}{A : Ty Γ}{B : Ty (Γ ▶ A)}{C : Ty (Γ ▶ A ▶ B)}{D : Ty (Γ ▶ A ▶ B ▶ C)} → Tms (Γ ▶ A ▶ B ▶ C ▶ D) Γ
wk4 {Γ}{A}{B}{C}{D} = _○_ {Γ ▶ A ▶ B ▶ C ▶ D}{Γ ▶ A}{Γ}(wk {Γ}{A})(wk3 {Γ ▶ A}{B}{C}{D})

wk5 : {Γ : Con}{A : Ty Γ}{B : Ty (Γ ▶ A)}{C : Ty (Γ ▶ A ▶ B)}{D : Ty (Γ ▶ A ▶ B ▶ C)}{E : Ty (Γ ▶ A ▶ B ▶ C ▶ D)} → Tms (Γ ▶ A ▶ B ▶ C ▶ D ▶ E) Γ
wk5 {Γ}{A}{B}{C}{D}{E} = _○_ {Γ ▶ A ▶ B ▶ C ▶ D ▶ E}{Γ ▶ A}{Γ}(wk {Γ}{A})(wk4 {Γ ▶ A}{B}{C}{D}{E})

vz : {Γ : Con}{A : Ty Γ} → Tm (Γ ▶ A) (_[_]T {Γ ▶ A}{Γ} A (wk {Γ}{A}))
vz {Γ}{A} = π₂ {Γ ▶ A}{Γ}{A}(id {Γ ▶ A})

vs : {Γ : Con}{A B : Ty Γ}(t : Tm Γ A) → Tm (Γ ▶ B) (_[_]T {Γ ▶ B}{Γ} A (wk {Γ}{B}))
vs {Γ}{A}{B} t = _[_]t {Γ ▶ B}{Γ}{A} t (wk {Γ}{B})

v1 : {Γ : Con}{A : Ty Γ}{B : Ty (Γ ▶ A)} → Tm (Γ ▶ A ▶ B) (_[_]T {Γ ▶ A ▶ B}{Γ} A (wk2 {Γ}{A}{B}))
v1 {Γ}{A}{B} = vs {Γ ▶ A}{_[_]T {Γ ▶ A}{Γ} A (wk {Γ}{A}) }{B}(vz {Γ}{A})

v2
  : {Γ : Con}{A : Ty Γ}{B : Ty (Γ ▶ A)}{C : Ty (Γ ▶ A ▶ B)}
  → Tm (Γ ▶ A ▶ B ▶ C) (_[_]T {Γ ▶ A ▶ B ▶ C}{Γ} A (wk3 {Γ}{A}{B}{C}))
v2 {Γ}{A}{B}{C} = vs {Γ ▶ A ▶ B}{_[_]T {Γ ▶ A ▶ B}{Γ} A (wk2 {Γ}{A}{B})}{C}(v1 {Γ}{A}{B})

v3
  : {Γ : Con}{A : Ty Γ}{B : Ty (Γ ▶ A)}{C : Ty (Γ ▶ A ▶ B)}{D : Ty (Γ ▶ A ▶ B ▶ C)}
  → Tm (Γ ▶ A ▶ B ▶ C ▶ D) (_[_]T {Γ ▶ A ▶ B ▶ C ▶ D}{Γ} A (wk4 {Γ}{A}{B}{C}{D}))
v3 {Γ}{A}{B}{C}{D} = vs {Γ ▶ A ▶ B ▶ C}{_[_]T {Γ ▶ A ▶ B ▶ C}{Γ} A (wk3 {Γ}{A}{B}{C})}{D}(v2 {Γ}{A}{B}{C})

v4 : {Γ : Con}{A : Ty Γ}{B : Ty (Γ ▶ A)}{C : Ty (Γ ▶ A ▶ B)}{D : Ty (Γ ▶ A ▶ B ▶ C)}{E : Ty (Γ ▶ A ▶ B ▶ C ▶ D)}
   → Tm (Γ ▶ A ▶ B ▶ C ▶ D ▶ E) (_[_]T {Γ ▶ A ▶ B ▶ C ▶ D ▶ E}{Γ} A (wk5 {Γ}{A}{B}{C}{D}{E}))
v4 {Γ}{A}{B}{C}{D}{E} = vs {Γ ▶ A ▶ B ▶ C ▶ D}{_[_]T {Γ ▶ A ▶ B ▶ C ▶ D}{Γ} A (wk4 {Γ}{A}{B}{C}{D})}{E}(v3 {Γ}{A}{B}{C}{D})

<_> : {Γ : Con}{A : Ty Γ}(t : Tm Γ A) → Tms Γ (Γ ▶ A)
<_> {Γ}{A} t = _,s_ {Γ}{Γ}{A}(id {Γ}) t

_^_ : {Γ Δ : Con}(σ : Tms Γ Δ)(A : Ty Δ) → Tms (Γ ▶ _[_]T {Γ}{Δ} A σ) (Δ ▶ A)
_^_ {Γ}{Δ} σ A = _,s_ {Γ ▶ A[σ]}{Δ}{A}(_○_ {Γ ▶ A[σ]}{Γ}{Δ} σ (wk {Γ}{A[σ]}))(vz {Γ}{A[σ]})
  where
    A[σ] = _[_]T {Γ}{Δ} A σ
