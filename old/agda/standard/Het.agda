module standard.Het where

open import HoTT
open import standard.Core
open import standard.TypeFormers

-- heterogeneous equality

infix 69 _⁼C

{-
module het1 where

  _⁼C : (Γ : Con) → Con
  Γ ⁼C = Σ Γ λ γ₀ → Σ Γ λ γ₁ → γ₀ ≡ γ₁

  pr0 : (Γ : Con) → Tms (Γ ⁼C) Γ
  pr0 Γ (γ₀ Σ, γ₁ Σ, γ₌)= γ₀

  pr1 : (Γ : Con) → Tms (Γ ⁼C) Γ
  pr1 Γ (γ₀ Σ, γ₁ Σ, γ₌)= γ₁

  _⁼T : {Γ : Con}(A : Ty Γ) → Ty (Γ ⁼C , A [ pr0 Γ ]T , A [ pr1 Γ ]T [ wk ]T)
  (A ⁼T) (((γ₀ Σ, γ₁ Σ, γ₌) Σ, t₀) Σ, t₁) = tr A γ₌ t₀ ≡ t₁

  ∙⁼ : ∙ ⁼C ≡ ∙
  ∙⁼ = {!!}

  ,⁼ : {Γ : Con}{A : Ty Γ} → (Γ , A) ⁼C ≡ Γ ⁼C , A [ pr0 Γ ]T , A [ pr1 Γ ]T [ wk ]T , A ⁼T
  ,⁼ {Γ}{A} = {!!}

  _⁼s : {Γ Δ : Con}(σ : Tms Γ Δ) → Tms (Γ ⁼C) (Δ ⁼C)
  (σ ⁼s) (γ₀ Σ, γ₁ Σ, γ₌) = σ γ₀ Σ, σ γ₁ Σ, ap σ γ₌

  _⁼t : {Γ : Con}{A : Ty Γ}(t : Tm Γ A) → Tm (Γ ⁼C) (A ⁼T [ id ,s t [ pr0 Γ ]t ,s t [ pr1 Γ ]t  ]T)
  (t ⁼t) (γ₀ Σ, γ₁ Σ, γ₌) = apd t γ₌

  pr0∙ : pr0 ∙ ≡ ε
  pr0∙ = refl

  pr1∙ : pr1 ∙ ≡ ε
  pr1∙ = refl

  pr0, : {Γ : Con}{A : Ty Γ} → tr (λ z → Tms z (Γ , A)) ,⁼ (pr0 (Γ , A)) ≡ pr0 Γ ∘ wk ∘ wk ∘ wk ,s vs (vs vz)
  pr0, = {!!}

  pr1, : {Γ : Con}{A : Ty Γ} → tr (λ z → Tms z (Γ , A)) ,⁼ (pr1 (Γ , A)) ≡ pr1 Γ ∘ wk ∘ wk ∘ wk ,s vs vz
  pr1, = {!!}

-- another try
-}
_⁼C : (Γ : Con) → Con
Γ ⁼C = Γ

pr0 : (Γ : Con) → Tms (Γ ⁼C) Γ
pr0 Γ γ = γ

pr1 : (Γ : Con) → Tms (Γ ⁼C) Γ
pr1 Γ γ = γ

_⁼T
  : {Γ : Con}(A : Ty Γ)
  → Ty ( Γ ⁼C
       ▶ _[_]T {Γ ⁼C}{Γ} A (pr0 Γ)
       ▶ _[_]T {Γ ⁼C ▶ _[_]T {Γ ⁼C}{Γ} A (pr0 Γ)}{Γ} A (_○_ {Γ ⁼C ▶ _[_]T {Γ ⁼C}{Γ} A (pr0 Γ)}{Γ ⁼C}{Γ}(pr1 Γ)(wk {Γ ⁼C}{_[_]T {Γ ⁼C}{Γ} A (pr0 Γ)})))
(A ⁼T)((γ , t₀) , t₁) = (t₀ == t₁) , raise-level _ (has-level-apply (snd (A γ)) t₀ t₁)

∙⁼ : ∙ ⁼C == ∙
∙⁼ = idp
{-
module ,⁼ {Γ : Con}{A : Ty Γ} where
  Θ₀ Θ₁ : Con
  Θ₀ = (Γ , A) ⁼C
  Θ₁ = Γ ⁼C , A [ pr0 Γ ]T , A [ pr1 Γ ]T [ wk ]T , A ⁼T
  
  ⇒ : Θ₀ → Θ₁ -- \r2
  ⇒ (γ Σ, α) = ((γ Σ, α) Σ, α) Σ, refl

  ⇐ : Θ₁ → Θ₀ -- \r2
  ⇐ (((γ Σ, α) Σ, .α) Σ, refl) = γ Σ, α

  eq :  Θ₁ ≡ Θ₀
  eq = {!!}

  trTmsΓ- : {Δ : Con} → Tms Δ Θ₀ → Tms Δ Θ₁
  (trTmsΓ- σ) γ = ⇒ (σ γ)

  trTms-Γ : {Δ : Con} → Tms Θ₀ Δ → Tms Θ₁ Δ
  (trTms-Γ σ) γ = σ (⇐ γ)

_⁼s : {Γ Δ : Con}(σ : Tms Γ Δ) → Tms (Γ ⁼C) (Δ ⁼C)
(σ ⁼s) = σ

_⁼t : {Γ : Con}{A : Ty Γ}(t : Tm Γ A) → Tm (Γ ⁼C) (A ⁼T [ id ,s t [ pr0 Γ ]t ,s t [ pr1 Γ ]t ]T)
(t ⁼t) γ = refl

pr0∙ : pr0 ∙ ≡ ε
pr0∙ = refl

pr1∙ : pr1 ∙ ≡ ε
pr1∙ = refl

pr0, : {Γ : Con}{A : Ty Γ} → ,⁼.trTms-Γ (pr0 (Γ , A)) ≡ pr0 Γ ∘ wk3 ,s vs (vs vz)
(pr0, {Γ}{A}) = ext f
  where
    f : (γ : (Γ ⁼C) , A [ pr0 Γ ]T , A [ pr1 Γ ]T [ wk ]T , (A ⁼T)) → ,⁼.trTms-Γ (pr0 (Γ , A)) γ ≡ (pr0 Γ ∘ wk3 ,s vs (vs vz)) γ
    f (((γ Σ, α) Σ, .α) Σ, refl) = refl

pr1, : {Γ : Con}{A : Ty Γ} → ,⁼.trTms-Γ (pr1 (Γ , A)) ≡ pr1 Γ ∘ wk3 ,s vs vz
(pr1, {Γ}{A}) = ext f
  where
    f : (γ : (Γ ⁼C) , A [ pr0 Γ ]T , A [ pr1 Γ ]T [ wk ]T , (A ⁼T)) → ,⁼.trTms-Γ (pr0 (Γ , A)) γ ≡ (pr1 Γ ∘ wk3 ,s vs vz) γ
    f (((γ Σ, α) Σ, .α) Σ, refl) = refl

module Γ,A⁼BB {Γ : Con}{A : Ty Γ}{B : Ty (Γ , A)} where
  Θ₀ Θ₁ : Con
  Θ₀ = Γ ⁼C , A [ pr0 Γ ]T , A [ pr1 Γ ]T [ wk ]T , A ⁼T , B [ pr0 Γ ∘ wk3 ,s vs (vs vz) ]T , B [ (pr1 Γ ∘ wk3 ,s vs vz) ∘ wk ]T
  Θ₁ = ((Γ , A) ⁼C) , B [ pr0 (Γ , A) ]T , B [ pr1 (Γ , A) ]T [ wk ]T
  
  ⇒ : Θ₀ → Θ₁
  ⇒ (((((γ Σ, α) Σ, .α) Σ, refl) Σ, β₀) Σ, β₁) = (,⁼.⇐ (((γ Σ, α) Σ, α) Σ, refl) Σ, β₀) Σ, β₁

  ⇐ : Θ₁ → Θ₀
  ⇐ ((γ Σ, β₀) Σ, β₁) = (,⁼.⇒ γ Σ, β₀) Σ, β₁

  eq : Θ₀ ≡ Θ₁
  eq = {!!}

  trTmsΓ- : {Δ : Con} → Tms Δ Θ₀ → Tms Δ Θ₁
  (trTmsΓ- σ) γ = ⇒ (σ γ)

  trTms-Γ : {Δ : Con} → Tms Θ₀ Δ → Tms Θ₁ Δ
  (trTms-Γ σ) γ = σ (⇐ γ)

Π⁼ : {Γ : Con}(A : Ty Γ)(B : Ty (Γ , A))
   → (Π A B) ⁼T
   ≡ Π (A [ pr0 Γ ∘ wk ∘ wk ]T)
    (Π (A [ pr1 Γ ∘ wk ∘ wk ∘ wk ]T)
    (Π (A ⁼T [ wk4 ,s vs vz ,s vz ]T)
       (B ⁼T [ Γ,A⁼BB.trTmsΓ- (wk5 ,s v2 ,s v1 ,s vz ,s (v4 $ v2) ,s (v3 $ v1)) ]T)))
Π⁼ {Γ} A B = ext λ { ((γ Σ, f₀) Σ, f₁) → {!!} }
-}
