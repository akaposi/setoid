{-# OPTIONS --type-in-type #-}
module kiplcub where

open import Data.Unit
open import Data.Product

data Con : Set
⟦_⟧C : Con → Set

data Con where
  • : Con
  _,_ : (Γ : Con) → (⟦ Γ ⟧C → Set) → Con  

⟦ • ⟧C = ⊤
⟦ Γ , A ⟧C = Σ ⟦ Γ ⟧C A

data Ty : (Γ : Con) → (⟦ Γ ⟧C → Set) → Set
-- A : Ty Γ ⟦ A ⟧

data Ty where
  unit : ∀ {Γ} → Ty Γ (λ γ → ⊤)
  π : ∀ {Γ}{A} → Ty Γ A → ∀ {B} → Ty (Γ , A) B → Ty Γ (λ γ → (x : A γ) → B (γ , x)) 

data Var : (Γ : Con) → (⟦ Γ ⟧C → Set) → Set where
  zero : ∀ {Γ}{A} → Var (Γ , A) (λ {(γ , _) → A γ})
  suc  : ∀ {Γ}{A}{B} → Var Γ A → Var (Γ , B) (λ {(γ , _) → A γ})
-- a : Tm Γ ⟦ A ⟧

⟦_⟧v : {Γ : Con}{A : ⟦ Γ ⟧C → Set} → Var Γ A → (γ : ⟦ Γ ⟧C) → A γ
⟦ zero ⟧v (_ , a) = a
⟦ suc x ⟧v (γ , a) = ⟦ x ⟧v γ

data Tm : (Γ : Con) → (⟦ Γ ⟧C → Set) → Set
⟦_⟧t : {Γ : Con}{A : ⟦ Γ ⟧C → Set} → Tm Γ A → (γ : ⟦ Γ ⟧C) → A γ

data Tm where
  var : ∀ {Γ}{A} → Var Γ A → Tm Γ A
  tt  : ∀ {Γ} → Tm Γ (λ γ → ⊤)
  lam : ∀ {Γ}{A}{B} → Tm (Γ , A) B → Tm Γ (λ γ → (x : A γ) → B (γ , x)) 
  app : ∀ {Γ}{A : ⟦ Γ ⟧C → Set}{B : ⟦ Γ , A ⟧C → Set} → Tm Γ (λ γ → (x : A γ) → B (γ , x)) → (a : Tm Γ A) → Tm Γ (λ γ → B (γ , ⟦ a ⟧t γ))

⟦ var x ⟧t γ = ⟦ x ⟧v γ
⟦ tt ⟧t γ = tt
⟦ lam t ⟧t γ = λ x → ⟦ t ⟧t (γ , x)
⟦ app t u ⟧t γ = ⟦ t ⟧t γ (⟦ u ⟧t γ)

-- upto here like Conor's Kipling

data CON : Con → Set where
  • : CON •
  _,_ : {Γ : Con} → CON Γ → {A : ⟦ Γ ⟧C → Set} → Ty Γ A → CON (Γ , A)

eqC : {Γ : Con} → CON Γ → Con
pr0 : {Γ : Con}(ΓΓ : CON Γ) → ⟦ eqC ΓΓ ⟧C → ⟦ Γ ⟧C 
pr1 : {Γ : Con}(ΓΓ : CON Γ) → ⟦ eqC ΓΓ ⟧C → ⟦ Γ ⟧C 
eqT : {Γ : Con}(ΓΓ : CON Γ) → {A : ⟦ Γ ⟧C → Set} → Ty Γ A → (γ : ⟦ eqC ΓΓ ⟧C) → A (pr0 ΓΓ γ) → A (pr1 ΓΓ γ) → Set

eqC • = •
eqC (_,_ {Γ} ΓΓ {A} AA) = ((eqC ΓΓ , 
                           (λ γ → A (pr0 ΓΓ γ))) , 
                           (λ {(γ , _) → A (pr1 ΓΓ γ)})) ,
                           (λ {((γ , a) , b) → eqT ΓΓ AA γ a b})
pr0 • tt = tt
pr0 (ΓΓ , AA) (((γ , a0) , a1) , aa) = (pr0 ΓΓ γ) , a0
pr1 • tt = tt
pr1 (ΓΓ , AA) (((γ , a0) , a1) , aa) = (pr1 ΓΓ γ) , a1
eqT ΓΓ unit γ tt tt = ⊤
eqT ΓΓ (π {A = A} AA {B = B} BB) γ f0 f1 = (x0 : A (pr0 ΓΓ γ))(x1 : A (pr1 ΓΓ γ))(x01 : eqT ΓΓ AA γ x0 x1) 
             → eqT (ΓΓ , AA) BB (((γ , x0) , x1) , x01) (f0 x0) (f1 x1)

congv : {Γ : Con}(ΓΓ : CON Γ) → {A : ⟦ Γ ⟧C → Set}(AA : Ty Γ A)(x : Var Γ A)
       → (γ : ⟦ eqC ΓΓ ⟧C) → eqT ΓΓ AA γ (⟦ x ⟧v (pr0 ΓΓ γ)) (⟦ x ⟧v (pr1 ΓΓ γ))
congv (ΓΓ , BB) AA zero (((γ , a0) , a1) , a01) = {!!}
congv ΓΓ AA (suc x) γ = {!!}

congt : {Γ : Con}(ΓΓ : CON Γ) → {A : ⟦ Γ ⟧C → Set}(AA : Ty Γ A)(t : Tm Γ A)
       → (γ : ⟦ eqC ΓΓ ⟧C) → eqT ΓΓ AA γ (⟦ t ⟧t (pr0 ΓΓ γ)) (⟦ t ⟧t (pr1 ΓΓ γ))
congt ΓΓ AA (var x) γ = {!!}
congt ΓΓ AA tt γ = {!!}
congt ΓΓ AA (lam t) γ = {!!}
congt ΓΓ AA (app t₁ t) γ = {!!}

