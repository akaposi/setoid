{-# OPTIONS --rewriting --without-K #-}

module Setoid where

open import HoTT hiding (coe₀)

infixl 65 _▶_
infixl 67 _[_]T
infixl 65 _,s_
infix  66 _○_
infixl 68 _[_]t

i : ULevel
i = lsucc (lsucc lzero)

j : ULevel
j = lsucc lzero

record Con : Set i where
  field
    ⌜_⌝C : Set j
    _⁼C  : ⌜_⌝C → ⌜_⌝C → Set j
    RC   : (γ : ⌜_⌝C) → _⁼C γ γ
    
  infix 7 ⌜_⌝C
  
open Con

record Ty (Γ : Con) : Set i where
  field
    ⌜_⌝T  : ⌜ Γ ⌝C → Set j
    _⁼T   : {γ₀ γ₁ : ⌜ Γ ⌝C}(γ₌ : (Γ ⁼C) γ₀ γ₁)(α₀ : ⌜_⌝T γ₀)(α₁ : ⌜_⌝T γ₁) → Set j
    coe₀  : {γ₀ γ₁ : ⌜ Γ ⌝C}(γ₌ : (Γ ⁼C) γ₀ γ₁)(α₀ : ⌜_⌝T γ₀) → ⌜_⌝T γ₁
    coe₁  : {γ₀ γ₁ : ⌜ Γ ⌝C}(γ₌ : (Γ ⁼C) γ₀ γ₁)(α₁ : ⌜_⌝T γ₁) → ⌜_⌝T γ₀
    coh₀  : {γ₀ γ₁ : ⌜ Γ ⌝C}(γ₌ : (Γ ⁼C) γ₀ γ₁)(α₀ : ⌜_⌝T γ₀) → _⁼T γ₌ α₀ (coe₀ γ₌ α₀)
    coh₁  : {γ₀ γ₁ : ⌜ Γ ⌝C}(γ₌ : (Γ ⁼C) γ₀ γ₁)(α₁ : ⌜_⌝T γ₁) → _⁼T γ₌ (coe₁ γ₌ α₁) α₁
    RT    : {γ : ⌜ Γ ⌝C}(α : ⌜_⌝T γ) → _⁼T (RC Γ γ) α α
    
  infix 7 ⌜_⌝T

open Ty

Ty=
  : {Γ : Con}
    {⌜_⌝T  : ⌜ Γ ⌝C → Set j}
    {_⁼T : {γ₀ γ₁ : ⌜ Γ ⌝C}(γ₌ : (Γ ⁼C) γ₀ γ₁)(α₀ : ⌜_⌝T γ₀)(α₁ : ⌜_⌝T γ₁) → Set j}
    {coe₀₀ coe₀₁ : {γ₀ γ₁ : ⌜ Γ ⌝C}(γ₌ : (Γ ⁼C) γ₀ γ₁)(α₀ : ⌜_⌝T γ₀) → ⌜_⌝T γ₁}(coe₀₌ : _==_ {A = {γ₀ γ₁ : ⌜ Γ ⌝C}(γ₌ : (Γ ⁼C) γ₀ γ₁)(α₀ : ⌜_⌝T γ₀) → ⌜_⌝T γ₁} coe₀₀ coe₀₁)
    {coe₁₀ coe₁₁ : {γ₀ γ₁ : ⌜ Γ ⌝C}(γ₌ : (Γ ⁼C) γ₀ γ₁)(α₁ : ⌜_⌝T γ₁) → ⌜_⌝T γ₀}(coe₁₌ : _==_ {A = {γ₀ γ₁ : ⌜ Γ ⌝C}(γ₌ : (Γ ⁼C) γ₀ γ₁)(α₁ : ⌜_⌝T γ₁) → ⌜_⌝T γ₀} coe₁₀ coe₁₁)
    {coh₀₀ : {γ₀ γ₁ : ⌜ Γ ⌝C}(γ₌ : (Γ ⁼C) γ₀ γ₁)(α₀ : ⌜_⌝T γ₀) → _⁼T γ₌ α₀ (coe₀₀ γ₌ α₀)}
    {coh₀₁ : {γ₀ γ₁ : ⌜ Γ ⌝C}(γ₌ : (Γ ⁼C) γ₀ γ₁)(α₀ : ⌜_⌝T γ₀) → _⁼T γ₌ α₀ (coe₀₁ γ₌ α₀)}
    (coh₀₌ : _==_ {A = {γ₀ γ₁ : ⌜ Γ ⌝C}(γ₌ : (Γ ⁼C) γ₀ γ₁)(α₀ : ⌜_⌝T γ₀) → _⁼T γ₌ α₀ (coe₀₁ γ₌ α₀)}
                  (transport (λ z → {γ₀ γ₁ : ⌜ Γ ⌝C}(γ₌ : (Γ ⁼C) γ₀ γ₁)(α₀ : ⌜_⌝T γ₀) → _⁼T γ₌ α₀ (z γ₌ α₀)) coe₀₌ coh₀₀)
                  coh₀₁)
    {coh₁₀ : {γ₀ γ₁ : ⌜ Γ ⌝C}(γ₌ : (Γ ⁼C) γ₀ γ₁)(α₁ : ⌜_⌝T γ₁) → _⁼T γ₌ (coe₁₀ γ₌ α₁) α₁}
    {coh₁₁ : {γ₀ γ₁ : ⌜ Γ ⌝C}(γ₌ : (Γ ⁼C) γ₀ γ₁)(α₁ : ⌜_⌝T γ₁) → _⁼T γ₌ (coe₁₁ γ₌ α₁) α₁}
    (coh₁₌ : _==_ {A = {γ₀ γ₁ : ⌜ Γ ⌝C}(γ₌ : (Γ ⁼C) γ₀ γ₁)(α₁ : ⌜_⌝T γ₁) → _⁼T γ₌ (coe₁₁ γ₌ α₁) α₁}
                  (transport (λ z → {γ₀ γ₁ : ⌜ Γ ⌝C}(γ₌ : (Γ ⁼C) γ₀ γ₁)(α₁ : ⌜_⌝T γ₁) → _⁼T γ₌ (z γ₌ α₁) α₁) coe₁₌ coh₁₀)
                  coh₁₁)
    {RT₀ RT₁ : {γ : ⌜ Γ ⌝C}(α : ⌜_⌝T γ) → _⁼T (RC Γ γ) α α}(RT₌ : _==_ {A = {γ : ⌜ Γ ⌝C}(α : ⌜_⌝T γ) → _⁼T (RC Γ γ) α α} RT₀ RT₁)
  → _==_ {A = Ty Γ}
         (record { ⌜_⌝T = ⌜_⌝T ; _⁼T = _⁼T ; coe₀ = coe₀₀ ; coe₁ = coe₁₀ ; coh₀ = coh₀₀ ; coh₁ = coh₁₀ ; RT = RT₀ })
         (record { ⌜_⌝T = ⌜_⌝T ; _⁼T = _⁼T ; coe₀ = coe₀₁ ; coe₁ = coe₁₁ ; coh₀ = coh₀₁ ; coh₁ = coh₁₁ ; RT = RT₁ })
Ty= idp idp idp idp idp = idp

record Tms (Γ Δ : Con) : Set j where
  field
    ⌜_⌝s : ⌜ Γ ⌝C → ⌜ Δ ⌝C
    _⁼s  : {γ₀ γ₁ : ⌜ Γ ⌝C}(γ₌ : (Γ ⁼C) γ₀ γ₁) → (Δ ⁼C) (⌜_⌝s γ₀) (⌜_⌝s γ₁)
    Rs   : (γ : ⌜ Γ ⌝C) → RC Δ (⌜_⌝s γ) == _⁼s (RC Γ γ)
    
  infix 7 ⌜_⌝s

open Tms

RsT=
  : {Γ Δ : Con}
    {⌜_⌝s₀ ⌜_⌝s₁ : ⌜ Γ ⌝C → ⌜ Δ ⌝C}(⌜_⌝s₌ : ⌜_⌝s₀ == ⌜_⌝s₁)
    {_⁼s₀ : {γ₀ γ₁ : ⌜ Γ ⌝C}(γ₌ : (Γ ⁼C) γ₀ γ₁) → (Δ ⁼C) (⌜_⌝s₀ γ₀) (⌜_⌝s₀ γ₁)}
    {_⁼s₁ : {γ₀ γ₁ : ⌜ Γ ⌝C}(γ₌ : (Γ ⁼C) γ₀ γ₁) → (Δ ⁼C) (⌜_⌝s₁ γ₀) (⌜_⌝s₁ γ₁)}
    (_⁼s₌ : _==_ {A = {γ₀ γ₁ : ⌜ Γ ⌝C}(γ₌ : (Γ ⁼C) γ₀ γ₁) → (Δ ⁼C) (⌜_⌝s₁ γ₀) (⌜_⌝s₁ γ₁)}
                 (transport (λ z → {γ₀ γ₁ : ⌜ Γ ⌝C}(γ₌ : (Γ ⁼C) γ₀ γ₁) → (Δ ⁼C) (z γ₀) (z γ₁)) ⌜_⌝s₌ _⁼s₀)
                 _⁼s₁)
  → _==_ ((γ : ⌜ Γ ⌝C) → RC Δ (⌜_⌝s₀ γ) == _⁼s₀ (RC Γ γ))
         ((γ : ⌜ Γ ⌝C) → RC Δ (⌜_⌝s₁ γ) == _⁼s₁ (RC Γ γ))
RsT= idp idp = idp

Tms=
  : {Γ Δ : Con}
    {⌜_⌝s₀ ⌜_⌝s₁ : ⌜ Γ ⌝C → ⌜ Δ ⌝C}(⌜_⌝s₌ : ⌜_⌝s₀ == ⌜_⌝s₁)
    {_⁼s₀ : {γ₀ γ₁ : ⌜ Γ ⌝C}(γ₌ : (Γ ⁼C) γ₀ γ₁) → (Δ ⁼C) (⌜_⌝s₀ γ₀) (⌜_⌝s₀ γ₁)}
    {_⁼s₁ : {γ₀ γ₁ : ⌜ Γ ⌝C}(γ₌ : (Γ ⁼C) γ₀ γ₁) → (Δ ⁼C) (⌜_⌝s₁ γ₀) (⌜_⌝s₁ γ₁)}
    (_⁼s₌ : _==_ {A = {γ₀ γ₁ : ⌜ Γ ⌝C}(γ₌ : (Γ ⁼C) γ₀ γ₁) → (Δ ⁼C) (⌜_⌝s₁ γ₀) (⌜_⌝s₁ γ₁)}
                 (transport (λ z → {γ₀ γ₁ : ⌜ Γ ⌝C}(γ₌ : (Γ ⁼C) γ₀ γ₁) → (Δ ⁼C) (z γ₀) (z γ₁)) ⌜_⌝s₌ _⁼s₀)
                 _⁼s₁)
    {Rs₀ : (γ : ⌜ Γ ⌝C) → RC Δ (⌜_⌝s₀ γ) == _⁼s₀ (RC Γ γ)}
    {Rs₁ : (γ : ⌜ Γ ⌝C) → RC Δ (⌜_⌝s₁ γ) == _⁼s₁ (RC Γ γ)}
    (Rs₌ : _==_ {A = (γ : ⌜ Γ ⌝C) → RC Δ (⌜_⌝s₁ γ) == _⁼s₁ (RC Γ γ)}
                (coe (RsT= {Γ}{Δ} ⌜_⌝s₌ _⁼s₌) Rs₀)
                Rs₁)
  → _==_ {A = Tms Γ Δ}(record { ⌜_⌝s = ⌜_⌝s₀ ; _⁼s = _⁼s₀ ; Rs = Rs₀ })(record { ⌜_⌝s = ⌜_⌝s₁ ; _⁼s = _⁼s₁ ; Rs = Rs₁ })
Tms= idp idp idp = idp

Tms='
  : {Γ Δ : Con}
    {⌜_⌝s : ⌜ Γ ⌝C → ⌜ Δ ⌝C}
    {_⁼s : {γ₀ γ₁ : ⌜ Γ ⌝C}(γ₌ : (Γ ⁼C) γ₀ γ₁) → (Δ ⁼C) (⌜_⌝s γ₀) (⌜_⌝s γ₁)}
    {Rs₀ Rs₁ : (γ : ⌜ Γ ⌝C) → RC Δ (⌜_⌝s γ) == _⁼s (RC Γ γ)}(Rs₌ : Rs₀ == Rs₁)
  → _==_ {A = Tms Γ Δ}(record { ⌜_⌝s = ⌜_⌝s ; _⁼s = _⁼s ; Rs = Rs₀ })(record { ⌜_⌝s = ⌜_⌝s ; _⁼s = _⁼s ; Rs = Rs₁ })
Tms=' {Γ}{Δ} Rs₌ = Tms= {Γ}{Δ} idp idp Rs₌

record Tm (Γ : Con)(A : Ty Γ) : Set j where
  field
    ⌜_⌝t : (γ : ⌜ Γ ⌝C) → ⌜ A ⌝T γ
    _⁼t  : {γ₀ γ₁ : ⌜ Γ ⌝C}(γ₌ : (Γ ⁼C) γ₀ γ₁) → (A ⁼T) γ₌ (⌜_⌝t γ₀) (⌜_⌝t γ₁)
    Rt   : (γ : ⌜ Γ ⌝C) → RT A (⌜_⌝t γ) == _⁼t (RC Γ γ)

  infix 7 ⌜_⌝t

open Tm

Tm='
  : {Γ : Con}{A : Ty Γ}
    {⌜_⌝t : (γ : ⌜ Γ ⌝C) → ⌜ A ⌝T γ}
    {_⁼t : {γ₀ γ₁ : ⌜ Γ ⌝C}(γ₌ : (Γ ⁼C) γ₀ γ₁) → (A ⁼T) γ₌ (⌜_⌝t γ₀) (⌜_⌝t γ₁)}
    {Rt₀ Rt₁ : (γ : ⌜ Γ ⌝C) → RT A (⌜_⌝t γ) == _⁼t (RC Γ γ)}(Rt₌ : Rt₀ == Rt₁)
  → _==_ {A = Tm Γ A}(record { ⌜_⌝t = ⌜_⌝t ; _⁼t = _⁼t ; Rt = Rt₀ })(record { ⌜_⌝t = ⌜_⌝t ; _⁼t = _⁼t ; Rt = Rt₁ })
Tm=' idp = idp

-- TODO: replace ₌ by ₓ or something (only in the metatheory)

• : Con -- \bub
• = record
  { ⌜_⌝C = Lift ⊤
  ; _⁼C  = λ _ _ → Lift ⊤
  ; RC   = λ _ → lift tt
  }

_▶_ : (Γ : Con) → Ty Γ → Con
Γ ▶ A = record
  { ⌜_⌝C = Σ ⌜ Γ ⌝C ⌜ A ⌝T
  ; _⁼C  = λ { (γ₀ , α₀)(γ₁ , α₁) → Σ ((Γ ⁼C) γ₀ γ₁) λ γ₌ → (A ⁼T) γ₌ α₀ α₁ }
  ; RC   = λ { (γ , α) → RC Γ γ , RT A α }
  }

_[_]T : ∀{Γ Δ} → Ty Δ → Tms Γ Δ → Ty Γ
_[_]T {Γ}{Δ} A σ = record
  { ⌜_⌝T = λ γ → ⌜ A ⌝T (⌜ σ ⌝s γ)
  ; _⁼T = λ γ₌ α₀ α₁ → (A ⁼T) ((σ ⁼s) γ₌) α₀ α₁
  ; coe₀ = λ γ₌ α₀ → coe₀ A ((σ ⁼s) γ₌) α₀
  ; coe₁ = λ γ₌ α₁ → coe₁ A ((σ ⁼s) γ₌) α₁
  ; coh₀ = λ γ₌ α₀ → coh₀ A ((σ ⁼s) γ₌) α₀
  ; coh₁ = λ γ₌ α₁ → coh₁ A ((σ ⁼s) γ₌) α₁
  ; RT = λ {γ} α → transport (λ z → (A ⁼T) z α α)(Rs σ γ)(RT A α)
  }

id : ∀{Γ} → Tms Γ Γ
id {Γ} = record
  { ⌜_⌝s = λ γ → γ
  ; _⁼s  = λ γ₌ → γ₌
  ; Rs   = λ γ → idp
  }

_○_ : ∀{Γ Δ Σ} → Tms Δ Σ → Tms Γ Δ → Tms Γ Σ
_○_ {Γ}{Δ}{Σ} σ ν = record
  { ⌜_⌝s = λ γ → ⌜ σ ⌝s (⌜ ν ⌝s γ)
  ; _⁼s  = λ γ₌ → (σ ⁼s) ((ν ⁼s) γ₌)
  ; Rs   = λ γ → Rs σ (⌜ ν ⌝s γ) ∙ ap (σ ⁼s) (Rs ν γ)
  }

ε : ∀{Γ} → Tms Γ •
ε {Γ} =  record
  { ⌜_⌝s = λ γ → lift tt
  ; _⁼s  = λ γ₌ → lift tt
  ; Rs   = λ γ → idp
  }

_,s_ : ∀{Γ Δ A}(σ : Tms Γ Δ) → Tm Γ (A [ σ ]T) → Tms Γ (Δ ▶ A)
_,s_ {Γ}{Δ}{A} σ t =  record
  { ⌜_⌝s = λ γ → ⌜ σ ⌝s γ , ⌜ t ⌝t γ
  ; _⁼s  = λ γ₌ → (σ ⁼s) γ₌ , (t ⁼t) γ₌
  ; Rs   = λ γ → pair= (Rs σ γ) (from-transp (λ γ₌ → (A ⁼T) γ₌ (⌜ t ⌝t γ)(⌜ t ⌝t γ))(Rs σ γ)(Rt t γ))
  }

π₁ : ∀{Γ Δ}{A : Ty Δ} → Tms Γ (Δ ▶ A) → Tms Γ Δ
π₁ σ =  record
  { ⌜_⌝s = λ γ → fst (⌜ σ ⌝s γ)
  ; _⁼s  = λ γ₌ → fst ((σ ⁼s) γ₌)
  ; Rs   = λ γ → ap fst (Rs σ γ)
  }

_[_]t : ∀{Γ Δ}{A : Ty Δ} → Tm Δ A → (σ : Tms Γ Δ) → Tm Γ (A [ σ ]T)
_[_]t {Γ}{Δ}{A} t σ = record
  { ⌜_⌝t = λ γ → ⌜ t ⌝t (⌜ σ ⌝s γ)
  ; _⁼t  = λ γ₌ → (t ⁼t) ((σ ⁼s) γ₌)
  ; Rt   = λ γ → to-transp (transport! (λ z → PathOver (λ z → (A ⁼T) z (⌜ t ⌝t (⌜ σ ⌝s γ))(⌜ t ⌝t (⌜ σ ⌝s γ)))(Rs σ γ) z ((t ⁼t)((σ ⁼s)(RC Γ γ))))(Rt t (⌜ σ ⌝s γ))(apd (t ⁼t) (Rs σ γ)))
  }

π₂ : ∀{Γ Δ}{A : Ty Δ}(σ : Tms Γ (Δ ▶ A)) → Tm Γ (A [ π₁ {A = A} σ ]T)
π₂ {Γ}{Δ}{A} σ = record
  { ⌜_⌝t = λ γ → snd (⌜ σ ⌝s γ)
  ; _⁼t  = λ γ₌ → snd ((σ ⁼s) γ₌)
  ; Rt   = λ γ → to-transp (snd= (Rs σ γ))
  }

[id]T : ∀{Γ}{A : Ty Γ} → A [ id ]T == A
[id]T = idp
{-
[][]T : ∀{Γ Δ Σ}{A : Ty Σ}{σ : Tms Γ Δ}{δ : Tms Δ Σ}
        → A [ δ ]T [ σ ]T == A [ _○_ {Γ}{Δ}{Σ} δ σ ]T
[][]T {A = A}{σ}{δ}
  = Ty= idp idp idp idp (exti λ γ → ext λ α →
      tr-ap (λ z → (A ⁼T) z α α)(δ ⁼s)(Rs σ γ)
    ◾ tr-tr (λ z → (A ⁼T) z α α)(Rs δ (⌜ σ ⌝s γ))(ap (δ ⁼s) (Rs σ γ)))
-}
idl : ∀{Γ Δ}{δ : Tms Γ Δ} → (_○_ {Γ}{Δ}{Δ} id δ) == δ
idl {δ = δ} = Tms=' (λ= λ γ → ap-idf (Rs δ γ))

idr : ∀{Γ Δ}{δ : Tms Γ Δ} → (_○_ {Γ}{Γ}{Δ} δ id) == δ
idr {δ = δ} = Tms=' (λ= λ γ → ∙-unit-r (Rs δ γ))
{-
ass : ∀{Γ Δ Σ Ω}{σ : Tms Σ Ω}{δ : Tms Δ Σ}{ν : Tms Γ Δ}
      → _○_ {Γ}{Δ}{Ω}(_○_ {Δ}{Σ}{Ω} σ δ) ν == _○_ {Γ}{Σ}{Ω} σ (_○_ {Γ}{Δ}{Σ} δ ν)
ass {σ = σ}{δ}{ν} = Tms=' (λ= λ γ → ◾ass (Rs σ (⌜ δ ⌝s (⌜ ν ⌝s γ)))(ap (σ ⁼s) (Rs δ (⌜ ν ⌝s γ)))(ap (λ γ₌ → (σ ⁼s) ((δ ⁼s) γ₌)) (Rs ν γ))
                                  ◾ ap (λ z → Rs σ (⌜ δ ⌝s (⌜ ν ⌝s γ)) ◾ z)
                                       ( ap (λ z → ap (σ ⁼s) (Rs δ (⌜ ν ⌝s γ)) ◾ z)
                                            (ap-ap (σ ⁼s)(δ ⁼s) (Rs ν γ) ⁻¹) 
                                       ◾ ap-◾ (σ ⁼s)(Rs δ (⌜ ν ⌝s γ))(ap (δ ⁼s) (Rs ν γ)) ⁻¹))

,○ : ∀{Γ Δ Σ}{δ : Tms Γ Δ}{σ : Tms Σ Γ}{A : Ty Δ}{t : Tm Γ (A [ δ ]T)}
      → (_○_ {Σ}{Γ}{Δ ▶ A} (_,s_ {Γ}{Δ}{A} δ t) σ) ==
        (_,s_ {Σ}{Δ}{A}(_○_ {Σ}{Γ}{Δ} δ σ)(tr (Tm Σ) ([][]T {Σ}{Γ}{Δ}{A}{σ}{δ}) (_[_]t {Σ}{Γ}{A [ δ ]T} t σ)))
,○ {δ = δ}{σ}{A}{t}
  = Tms= (ext λ γ → Σ,= idp {!!})
         {!!}
         {!!}
-}
π₁β : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ Δ}{a : Tm Γ (A [ δ ]T)}
      → (π₁ {Γ}{Δ}{A}(_,s_ {Γ}{Δ}{A} δ a)) == δ
π₁β {A = A}{δ}{a} = Tms=' (λ= λ γ → fst=-β (Rs δ γ)(from-transp _ _ (Rt a γ)))

πη : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ (Δ ▶ A)}
      → _,s_ {Γ}{Δ}{A}(π₁ {Γ}{Δ}{A} δ)(π₂ {Γ}{Δ}{A} δ) == δ
πη {A = A}{δ} = Tms=' (λ= λ γ → ap (pair= (Rs (π₁ {A = A} δ) γ)) (to-transp-η (snd= (Rs δ γ))) ∙ ! (pair=-η (Rs δ γ)))

εη : ∀{Γ}{σ : Tms Γ •}
      → σ == ε
εη {Γ}{σ} = Tms=' (λ= λ γ → contr-has-all-paths {{has-level-apply (Lift-level (Unit-level {S ⟨-2⟩}))(lift tt)((σ ⁼s)(RC Γ γ))}}
                                                (Rs σ γ) idp)
{-
trA[Tms=']
  : ∀{Γ Δ}{A : Ty Δ}
    {⌜_⌝s : ⌜ Γ ⌝C → ⌜ Δ ⌝C}
    {_⁼s : {γ₀ γ₁ : ⌜ Γ ⌝C}(γ₌ : (Γ ⁼C) γ₀ γ₁) → (Δ ⁼C) (⌜_⌝s γ₀) (⌜_⌝s γ₁)}
    {Rs₀ Rs₁ : (γ : ⌜ Γ ⌝C) → RC Δ (⌜_⌝s γ) == _⁼s (RC Γ γ)}(Rs₌ : Rs₀ == Rs₁)
    {a : Tm Γ (A [ (record { ⌜_⌝s = ⌜_⌝s ; _⁼s = _⁼s ; Rs = Rs₀ }) ]T)}
  → tr (λ z → Tm Γ (A [ z ]T)) (Tms=' Rs₌) a
  == record { ⌜_⌝t = ⌜ a ⌝t
           ; _⁼t = a ⁼t
           ; Rt = λ γ → tr (λ Rs → RT (A [ record { ⌜_⌝s = ⌜_⌝s ; _⁼s = λ {_}{_} → _⁼s ; Rs = Rs } ]T) (⌜ a ⌝t γ) == (a ⁼t) (RC Γ γ))
                           Rs₌
                           (Rt a γ)
           }
trA[Tms='] idp = idp

π₂β : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ Δ}{a : Tm Γ (A [ δ ]T)}
      → tr (λ z → Tm Γ (A [ z ]T)) (π₁β {Γ}{Δ}{A}{δ}{a}) (π₂ {Γ}{Δ}{A}(_,s_ {Γ}{Δ}{A} δ a)) == a
π₂β {Γ}{Δ}{A}{δ}{a} = trA[Tms='] {Γ}{Δ}{A}{⌜ δ ⌝s}{δ ⁼s}(ext λ γ → Σ,=β₁ (Rs δ γ) (Rt a γ)){π₂ {Γ}{Δ}{A}(_,s_ {Γ}{Δ}{A} δ a)}
                    ◾ Tm=' (ext λ γ → {!!} ◾ Σ,=β₂ (Rs δ γ) (Rt a γ))

wk : ∀{Γ}{A : Ty Γ} → Tms (Γ ▶ A) Γ
wk {Γ}{A} = π₁ {Γ ▶ A}{Γ}{A} id

vz : ∀{Γ}{A : Ty Γ} → Tm (Γ ▶ A) (A [ wk {Γ}{A} ]T)
vz {Γ}{A} = π₂ {Γ ▶ A}{Γ}{A} id

vs : ∀{Γ}{A B : Ty Γ} → Tm Γ A → Tm (Γ ▶ B) (A [ wk {Γ}{B} ]T)
vs {Γ}{A}{B} x = _[_]t {Γ ▶ B}{Γ}{A} x (wk {Γ}{B})

<_> : ∀{Γ}{A : Ty Γ} → Tm Γ A → Tms Γ (Γ ▶ A)
<_> {Γ}{A} t = _,s_ {Γ}{Γ}{A} id (tr (Tm Γ) ([id]T {Γ}{A} ⁻¹) t)

infix 4 <_>

_^_ : {Γ Δ : Con}(σ : Tms Γ Δ)(A : Ty Δ) → Tms (Γ ▶ A [ σ ]T) (Δ ▶ A)
_^_ {Γ}{Δ} σ A =
  _,s_ {Γ ▶ A [ σ ]T}{Δ}{A}
       (_○_ {Γ ▶ A [ σ ]T}{Γ}{Δ} σ (wk {Γ}{A [ σ ]T}))
       (tr (Tm (Γ ▶ A [ σ ]T)) ([][]T {Γ ▶ A [ σ ]T}{Γ}{Δ}{A}{wk{Γ}{A [ σ ]T}}{σ}) vz)

infixl 5 _^_


{-

trA[δ₌] : ∀{Γ Δ}{A : Ty Δ}{δ₀ δ₁ : Tms Γ Δ}(δ₌ : δ₀ == δ₁)
         {a : Tm Γ (A [ δ₀ ]T)}
       → tr (λ z → Tm Γ (A [ z ]T)) δ₌ a
       == record { ⌜_⌝t = λ γ → tr (λ z → ⌜ A [ z ]T ⌝T γ) δ₌ (⌜ a ⌝t γ)
                ; _⁼t  = λ {γ₀}{γ₁} γ₌
                         → J (λ {δ₁} δ₌ → ((A [ δ₁ ]T) ⁼T) γ₌ (tr (λ z → ⌜ A [ z ]T ⌝T γ₀) δ₌ (⌜ a ⌝t γ₀))(tr (λ z → ⌜ A [ z ]T ⌝T γ₁) δ₌ (⌜ a ⌝t γ₁)))
                             ((a ⁼t) γ₌)
                             δ₌
                ; Rt   = λ γ
                         → J {x = δ₀}
                             (λ {δ₁} δ₌ → RT (A [ δ₁ ]T) (tr (λ z → ⌜ A [ z ]T ⌝T γ) δ₌ (⌜ a ⌝t γ)) == J (λ {δ₂} δ₌₁ → ((A [ δ₂ ]T) ⁼T) (RC Γ γ) (tr (λ z → ⌜ A [ z ]T ⌝T γ) δ₌₁ (⌜ a ⌝t γ)) (tr (λ z → ⌜ A [ z ]T ⌝T γ) δ₌₁ (⌜ a ⌝t γ))) ((a ⁼t) (RC Γ γ)) δ₌)
                             (Rt a γ)
                             δ₌
                }
trA[δ₌] idp = idp

-}
-}
