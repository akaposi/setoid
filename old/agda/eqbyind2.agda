{-# OPTIONS --rewriting --prop #-}

-- eqbyind2: en utilisant des telescopes

open import Agda.Primitive


------------------------------------------------------------------------------
-- The relevant stuffs start at wort "start". First, there is a small library.
------------------------------------------------------------------------------


Π : {ℓ ℓ' : Level} (A : Set ℓ) (B : A → Set ℓ') → Set (ℓ ⊔ ℓ')
Π A B = (x : A) → B x

record Σ {ℓ ℓ'} (A : Set ℓ) (B : A → Set ℓ') : Set (ℓ ⊔ ℓ') where
  constructor _,Σ_
  field
    proj₁ : A
    proj₂ : B proj₁
infixl 5 _,Σ_

open Σ public
record ⊤ : Set where
  constructor tt

data ⊥ : Set where

⊥-elim : ∀{ℓ}{A : Set ℓ} → ⊥ → A
⊥-elim ()

¬ : Set → Set
¬ A = A → ⊥


data Bool : Set where
  true false : Bool

if_then_else_ : ∀{ℓ}{C : Bool → Set ℓ}(b : Bool)(c : C true)(d : C false) → C b
if true then c else d = c
if false then c else d = d

data ℕ : Set where
  zero : ℕ
  suc  : ℕ → ℕ

one = suc zero

record ⊤p : Prop where
  constructor ttp

data ⊥p : Prop where

abort⊥p : ∀{ℓ}{A : Set ℓ} → ⊥p → A
abort⊥p ()

abort⊥pp : ∀{ℓ}{A : Prop ℓ} → ⊥p → A
abort⊥pp ()

abortp : ∀{ℓ}{A : Prop ℓ} → ⊥ → A
abortp ()

record Σp {ℓ ℓ'} (A : Prop ℓ) (B : A → Prop ℓ') : Prop (ℓ ⊔ ℓ') where
  constructor _,p_
  field
    proj₁p : A
    proj₂p : B proj₁p
infixl 5 _,p_
open Σp public

_×p_ : ∀{ℓ ℓ'} → Prop ℓ → Prop ℓ' → Prop (ℓ ⊔ ℓ')
A ×p B = Σp A λ _ → B
infixl 4 _×p_

data _≡p_ {ℓ}{A : Prop ℓ} (x : A) : A → Set ℓ where
  reflp : x ≡p x


record LiftP {ℓ ℓ'}(A : Prop ℓ) : Prop (ℓ ⊔ ℓ') where
  constructor liftP
  field
    unliftP : A
open LiftP public

record El {ℓ}(A : Prop ℓ) : Set ℓ where
  constructor liftp
  field
    unliftp : A
open El public

record Lift {ℓ ℓ'}(A : Set ℓ) : Set (ℓ ⊔ ℓ') where
  constructor lift
  field
    unlift : A
open Lift public

 
postulate _→β_ : {ℓ : Level}{A : Set ℓ} → A → A → Set
{-# BUILTIN REWRITE _→β_ #-}

postulate _→β'_ : {ℓ : Level}{A : Prop ℓ} → A → A → Set
-- {-# BUILTIN REWRITE _→β'_ #-} no need to rewrite it is convertible by proof irrelevance


------------------------------------------------------------------------------
-- start!
------------------------------------------------------------------------------



data Tel (ℓ : Level) : ℕ → Set (lsuc ℓ)
E  : {ℓ : Level}{n : ℕ} → Tel ℓ n → Set ℓ

data Tel ℓ where
  nil : Tel ℓ zero
  _,T_  : {n : ℕ}(T : Tel ℓ n) → (E T → Set ℓ) → Tel ℓ (suc n)

E nil = Lift ⊤
E (T ,T A) = Σ (E T) A

tl : ∀{ℓ}{n} → Tel ℓ (suc n) → Tel ℓ n
hd : ∀{ℓ}{n}(T : Tel ℓ (suc n)) → E (tl T) → Set ℓ
tl (T ,T _) = T
hd (T ,T A) = A

tlE : ∀{ℓ}{n}{T : Tel ℓ (suc n)}(x : E T) → E (tl T)
hdE : ∀{ℓ}{n}{T : Tel ℓ (suc n)}(x : E T) → hd T (tlE x)
tlE {T = T ,T _} = proj₁
hdE {T = T ,T _} = proj₂

ttT : {ℓ : Level} → Set ℓ → Tel ℓ one
ttT A = nil ,T (λ _ → A)


data TelP (ℓ : Level) : ℕ → Set (lsuc ℓ)
EP  : {ℓ : Level}{n : ℕ} → TelP ℓ n → Prop ℓ

data TelP ℓ where
  nilP : TelP ℓ zero
  _,TP_  : {n : ℕ}(T : TelP ℓ n) → (EP T → Prop ℓ) → TelP ℓ (suc n)

EP nilP = LiftP ⊤p
EP (T ,TP A) = Σp (EP T) A

tlP : ∀{ℓ}{n} → TelP ℓ (suc n) → TelP ℓ n
hdP : ∀{ℓ}{n}(T : TelP ℓ (suc n)) → EP (tlP T) → Prop ℓ
tlP (T ,TP _) = T
hdP (T ,TP A) = A

consP : ∀{ℓ}{n}{T : TelP ℓ (suc n)}(x : EP (tlP T))(y : hdP T x) → EP T
consP {ℓ} {n} {T ,TP A} x y = x ,p y

fstP : ∀{ℓ}{n}{T : TelP ℓ (suc n)}(x : EP T) → EP (tlP T)
fstP {ℓ}{n}{T ,TP A} x = proj₁p x

sndP : ∀{ℓ}{n}{T : TelP ℓ (suc n)}(x : EP T) → hdP T (fstP x)
sndP {ℓ}{n}{T ,TP A} x = proj₂p x


-- Here are our postulates on equality. Then, there are two reduction rules (_≡_ and subst) for each type former

postulate _≡_  : {ℓ : Level}{n : ℕ}{A : Tel ℓ n} → E A → E A → TelP ℓ n
postulate tl≡  : {ℓ : Level}{n : ℕ}{A : Tel ℓ (suc n)}{x₀ x₁ : E A} → tlP (x₀ ≡ x₁) →β (tlE x₀ ≡ tlE x₁)
{-# REWRITE tl≡ #-}

postulate refl : {ℓ : Level}{n : ℕ}{A : Tel ℓ n}(t : E A) → EP (t ≡ t)
postulate sym  : {ℓ : Level}{n : ℕ}{A : Tel ℓ n}{t₀ t₁ : E A} → EP (t₀ ≡ t₁) → EP (t₁ ≡ t₀)
postulate trs  : {ℓ : Level}{n : ℕ}{A : Tel ℓ n}{t₀ t₁ t₂ : E A} → EP (t₀ ≡ t₁) → EP (t₁ ≡ t₂) → EP (t₀ ≡ t₂)

-- less implicit args
_∶_≡_ : {ℓ : Level}{n : ℕ}(A : Tel ℓ n) → E A → E A → TelP ℓ n
_∶_≡_ A t₀ t₁ = t₀ ≡ t₁

-- notation
_∶_≡[_]≡_ : {ℓ : Level}{n : ℕ}{A : Tel ℓ n}(B : E A → Set ℓ){x₀ x₁ : E A}(y₀ : B x₀)(x₀₁ : EP (x₀ ≡ x₁))(y₁ : B x₁) → Prop ℓ
_∶_≡[_]≡_ B y₀ x₀₁ y₁ = hdP ((_ ,T B) ∶ (_ ,Σ y₀) ≡ (_ ,Σ y₁)) x₀₁


postulate subst : {ℓ : Level}{n : ℕ}{A : Tel ℓ n}(B : E A → Set ℓ){x₀ x₁ : E A}(x₀₁ : EP (x₀ ≡ x₁)) → B x₀ → B x₁

postulate coh   : {ℓ : Level}{n : ℕ}{A : Tel ℓ n}(B : E A → Set ℓ){x₀ x₁ : E A}(x₀₁ : EP (x₀ ≡ x₁))(t₀ : B x₀)
                → B ∶ t₀ ≡[ x₀₁ ]≡ subst B x₀₁ t₀

-- postulate ≡[refl]≡ : {ℓ : Level}{n : ℕ}{A : Set ℓ}{x : A}{Δ : A → Tel ℓ n}
-- {ℓ : Level}{A : Set ℓ}{B : A → Set ℓ}{x : A}{u v : B x} →  (_≡[_]≡'_ {B = B} u (refl' x) v) →β (u ≡' v)


-- sym'  : {ℓ : Level}{n : ℕ}{A : Tel ℓ n}{t₀ t₁ : E A} → EP (t₀ ≡ t₁) → EP (t₁ ≡ t₀)
-- sym' {ℓ}{n}{A}{t₀}{t₁} p = {!subst {A = A} (λ x → El (EP (x ≡ t₀))) p!}

_≡'_  : {ℓ : Level}{A : Set ℓ} (x : A) → A → Prop ℓ
_≡'_  {ℓ}{A} x y = hdP ((ttT A) ∶ (_ ,Σ x) ≡ (_ ,Σ y)) (refl _)

refl' : {ℓ : Level}{A : Set ℓ}(t : A) → t ≡' t
refl' {ℓ}{A} t = sndP (refl {A = ttT A} (_ ,Σ t))

subst' : {ℓ : Level}{A : Set ℓ}(B : A → Set ℓ){x₀ x₁ : A}(p : x₀ ≡' x₁) → B x₀ → B x₁
subst' {ℓ}{A} B p t₀ = subst {A = ttT A} (λ x → B (proj₂ x)) (consP _ p) t₀

_≡[_]≡'_ : {ℓ : Level}{A : Set ℓ}{B : A → Set ℓ}{x₀ x₁ : A} → B x₀ → x₀ ≡' x₁ → B x₁ → Prop ℓ
_≡[_]≡'_ {ℓ}{A}{B} t₀ p t₁ = _∶_≡[_]≡_ {A = ttT A} (λ x → B (proj₂ x)) t₀ (consP (refl _) p) t₁

coh' : {ℓ : Level}{A : Set ℓ}(B : A → Set ℓ){x₀ x₁ : A}(p : x₀ ≡' x₁)(t₀ : B x₀) → t₀ ≡[ p ]≡' subst' B p t₀
coh' {ℓ}{A} B p t₀ = coh {A = ttT A} (λ x → B (proj₂ x)) (consP (refl _) p) t₀ 

-- -- First derivable lemmas

-- coe≡[]≡ : {ℓ : Level}{A : Set ℓ}(B : A → Set ℓ){x₀ x₁ : A}(p : x₀ ≡ x₁){t₀ : B x₀}{t₁ : B x₁} → subst B p t₀ ≡' t₁ → B ∶ t₀ ≡[ p ]≡ t₁
-- coe≡[]≡ {A} B p {t₀}{t₁} q = trs[] B p (refl _) (coh B p t₀) {!!}

-- ≡[]≡coe : {ℓ : Level}{A : Set ℓ}(B : A → Set ℓ){x₀ x₁ : A}(p : x₀ ≡ x₁){t₀ : B x₀}{t₁ : B x₁} → B ∶ t₀ ≡[ p ]≡ t₁ → subst B p t₀ ≡ t₁
-- ≡[]≡coe {A} B p {t₀}{t₁} q = trs[] B (sym {A} p) p (sym[] B p (coh B p t₀)) q

coh* : ∀{ℓ}{n}{A : Tel ℓ n}(B : E A → Set ℓ){x₀ x₁ : E A}(x₀₁ : EP (x₀ ≡ x₁))(t₁ : B x₁) → B ∶ subst B (sym x₀₁) t₁ ≡[ x₀₁ ]≡ t₁
coh* {ℓ}{n}{A} B x₀₁ t₁ = let X = coh B (sym x₀₁) t₁
                              Y = sym {A = A ,T B}{_ ,Σ t₁}{_ ,Σ subst B (sym x₀₁) t₁} (consP (sym x₀₁) X)
                          in sndP Y

J : {A : Set}{x : A}(B : (y : A) → x ≡' y → Set){y : A}(p : x ≡' y) → B x (refl' x) → B y p
J {A}{x} B p t = subst' (λ y → (p : x ≡' y) → B y p) p (λ p₁ → t) p

substR : {A  : Set}(B : A → Set){x : A}(t : B x) → t ≡' subst' B (refl' x) t
substR {A} B {x} t = let X = coh' B (refl' _) t  in {!fst!}

-- uip : {A : Set} {x y : A} (p q : x ≡ y) → p ≡p q
-- uip p q = reflp



-- Σ types

postulate ≡Σ : ∀ {ℓ}{n}{A : Tel ℓ n}{B : E A → Set ℓ}{C : (x : E A) → B x → Set ℓ}{xf₀ xf₁ : Σ (E A) (λ x → Σ (B x) (C x))}
               → (_≡_ {A = A ,T (λ x → Σ (B x) (C x))} xf₀ xf₁)
                 →β let x₀ = proj₁ xf₀
                        x₁ = proj₁ xf₁
                        f₀ = proj₂ xf₀
                        f₁ = proj₂ xf₁ in
                    (x₀ ≡ x₁) ,TP λ x₀₁ → Σp (B ∶ proj₁ f₀ ≡[ x₀₁ ]≡ proj₁ f₁) λ y₀₁ → (λ z → C (proj₁ z) (proj₂ z)) ∶ proj₂ f₀ ≡[ consP x₀₁ y₀₁ ]≡ proj₂ f₁

{-# REWRITE ≡Σ #-}

postulate substΣ : ∀ {ℓ}{n}{A : Tel ℓ n}{B : E A → Set ℓ}{C : (x : E A) → B x → Set ℓ}{x₀ x₁ : E A}{x₀₁ : EP (x₀ ≡ x₁)}{f₀ : Σ (B x₀) (C x₀)}
                 → (subst (λ x → Σ (B x) (C x)) x₀₁ f₀)
                   →β ((subst B x₀₁ (proj₁ f₀)) ,Σ (subst (λ z → C (proj₁ z) (proj₂ z)) (consP x₀₁ (coh B x₀₁ (proj₁ f₀))) (proj₂ f₀)))
{-# REWRITE substΣ #-}
 

-- Π types

postulate ≡Π : ∀ {ℓ}{n}{A : Tel ℓ n}{B : E A → Set ℓ}{C : (x : E A) → B x → Set ℓ}{xf₀ xf₁ : Σ (E A) (λ x → Π (B x) (C x))}
               → (_≡_ {A = A ,T (λ x → Π (B x) (C x))} xf₀ xf₁)
                 →β let x₀ = proj₁ xf₀
                        x₁ = proj₁ xf₁
                        f₀ = proj₂ xf₀
                        f₁ = proj₂ xf₁ in
                    (x₀ ≡ x₁) ,TP λ x₀₁ → (y₀ : B x₀)(y₁ : B x₁)(y₀₁ : B ∶ y₀ ≡[ x₀₁ ]≡ y₁) → (λ x → C (proj₁ x) (proj₂ x)) ∶ f₀ y₀ ≡[ consP x₀₁ y₀₁ ]≡ f₁ y₁
{-# REWRITE ≡Π #-}

postulate substΠ : ∀ {ℓ}{n}{A : Tel ℓ n}{B : E A → Set ℓ}{C : (x : E A) → B x → Set ℓ}{x₀ x₁ : E A}{x₀₁ : EP (x₀ ≡ x₁)}{f₀ : Π (B x₀) (C x₀)}
                 → (subst (λ x → Π (B x) (C x)) x₀₁ f₀)
                   →β λ x → subst {A = A ,T B} (λ x → C _ (proj₂ x)) (consP x₀₁ (coh* B x₀₁ x)) (f₀ (subst B (sym x₀₁) x))
{-# REWRITE substΠ #-}


apD : {ℓ : Level}{A : Set ℓ}{B : A → Set ℓ}(f : Π A B){x₀ x₁ : A}(p : x₀ ≡' x₁) → _≡[_]≡'_ {B = B} (f x₀) p (f x₁)
apD f p = refl' f _ _ p

funext : {ℓ : Level}{A : Set ℓ}{B : A → Set ℓ}(f g : Π A B)(p : (x : A) → f x ≡' g x) → f ≡' g
funext {B = B} f g p = λ x₀ x₁ q → let X = apD f q
                                       Y = p x₁ in {!consP _ X!} --trs[] B q (refl x₁) (apD f q) (p x₁)

-- -- Bool

-- postulate ≡Bool : {b₀ b₁ : Bool} → (b₀ ≡ b₁) →β (if b₀ then if b₁ then ⊤p else ⊥p else if b₁ then ⊥p else ⊤p)
-- {-# REWRITE ≡Bool #-}

-- postulate ≡Bool' : {ℓ : Level}{A : Set ℓ}{x₀ x₁ : A}{x₀₁ : x₀ ≡ x₁}{b₀ b₁ : Bool} → ((λ (x : A) → Bool) ∶ b₀ ≡[ x₀₁ ]≡ b₁) →β (if b₀ then if b₁ then LiftP ⊤p else LiftP ⊥p else if b₁ then LiftP ⊥p else LiftP ⊤p)

-- postulate substBool : {A : Set}{x₀ x₁ : A}{p : x₀ ≡ x₁}{b : Bool} → (subst {A = A} (λ x → Bool) p b) →β b
-- {-# REWRITE substBool #-}

-- -- Prop

-- postulate ≡Prop : (A B : Prop) →  (A ≡ B) →β LiftP ((A → B) ×p (B → A))
-- {-# REWRITE ≡Prop #-}

-- postulate ≡Prop' : {ℓ : Level}{A : Set ℓ}{x₀ x₁ : A}{x₀₁ : x₀ ≡ x₁}(A₀ A₁ : Prop) →  ((λ (x : A) → Prop) ∶ A₀ ≡[ x₀₁ ]≡ A₁) →β LiftP ((A₀ → A₁) ×p (A₁ → A₀))

-- postulate substProp : {A : Set}{x₀ x₁ : A}{p : x₀ ≡ x₁}{P : Prop} → (subst {A = A} (λ _ → Prop) p P) →β P
-- {-# REWRITE substProp #-}

-- -- El

-- postulate ≡El : (A : Prop)(u v : El A) →  (u ≡ v) →β ⊤p
-- {-# REWRITE ≡El #-}

-- -- There is a universe level problem
-- postulate substEl : {A : Set₁}{x₀ x₁ : A}{p : x₀ ≡ x₁}{P : A → Prop}{t : El (P x₀)}
--                   → (subst {A = A} (λ x → El (P x)) p t) →β let X = refl {_} {A → Prop} P  x₀ x₁ p in
--                                                             let Y : P x₀ ≡ P x₁
--                                                                 Y = {!≡[]≡coe (λ x → P p X!} in
--                                                             {!El!}
-- -- {-# REWRITE substEl #-}


-- -- Other admissible lemmas

-- ap : {A : Set} {B : Set} (f : A → B) {x y : A} (p : x ≡ y) → El (f x ≡ f y)
-- ap f {x = x} p = subst (λ x → El (_ ≡ f x)) p (liftp (refl (f x)))

-- ap₂ : {A A' : Set} {B : Set} (f : A → A' → B) {x y  : A} {x' y' : A'} (p : x ≡ y) (q : x' ≡ y') → El (f x x' ≡ f y y')
-- ap₂ f p q = subst (λ x' → El (_ ≡ f _ x')) q (subst (λ x → El (_ ≡ f x _)) p (liftp (refl (f _ _))))

-- transportD : {A : Set} {B : A → Set} (f : (a : A) → B a) {x y : A} (p : x ≡ y) → subst B p (f x) ≡ f y
-- transportD {A}{B} f {x} p = trs[] B (sym {A = A} p) p (coh* B (sym {A = A} p) (f x)) (apD f p)

-- substconst : {A : Set} (B : Set) {x y : A} (p : x ≡ y) (b : B) → El ((subst (λ _ → B) {x} p) b ≡ b)
-- substconst B {x} p b = J {x = x} (λ x' p → El (subst _ {x} p b ≡ b)) p (liftp (sym {A = B} (substR (λ _ → B) {x} b)))

-- subst₂ : {A  A' : Set} (B : A → A' → Set) {x y  : A} {x' y' : A'} (p : x ≡ y) (p' : x' ≡ y') → B x x' → B y y'
-- subst₂ B p q b = subst (λ x' → B _ x') q (subst (λ x → B x _) p b) 

-- ap10 : {A : Set} {B : Set} {f g : A → B} {x : A} (p : f ≡ g) → f x ≡ g x
-- ap10 {f = f} {g} {x} p = p x x (refl x)


-- neg : Bool -> Bool
-- neg true = false
-- neg false = true

-- neg≠b : (b : Bool) → neg b ≡ b → ⊥
-- neg≠b true ()
-- neg≠b false ()


-- neg≠b' : (b₁ b₂ : Bool) → (b₁ ≡ b₂ → ⊥p) → neg b₁ ≡ b₂
-- neg≠b' true true f = f ttp
-- neg≠b' true false f = ttp
-- neg≠b' false true f = ttp
-- neg≠b' false false f = f ttp


-- ≡Σ'' : {ℓ : Level}{A : Set ℓ}{B : A → Set ℓ}(u v : Σ A B)(p : proj₁ u ≡ proj₁ v)(q : subst B p (proj₂ u) ≡ proj₂ v) → u ≡ v
-- ≡Σ'' {B = B} u v p q = p ,p coe≡[]≡ B p q

-- pr1≡ : {ℓ ℓ' : Level}{A : Set ℓ}(B : A → Set ℓ'){u v : Σ A B}(p : u ≡ v) → proj₁ u ≡ proj₁ v
-- pr1≡ B p = proj₁p p

-- pr2≡ : {ℓ : Level}{A : Set ℓ}(B : A → Set ℓ){u v : Σ A B}(p : u ≡ v) → subst B (pr1≡ B p) (proj₂ u) ≡ proj₂ v
-- pr2≡ B p = ≡[]≡coe B (pr1≡ B p) (proj₂p p)



















-- postulate reflΣ : {ℓ ℓ' : Level}{A : Set ℓ}{B : A → Set ℓ'}(u : Σ A B) →  (refl u) →β'(refl (proj₁ u) ,p refl (proj₂ u))
-- postulate cohΣ : {ℓ ℓ' : Level}{A : Set ℓ}{B : A → Set ℓ'}{C : (x : A) → B x → Set}{x₀ x₁ : A}{p : x₀ ≡ x₁}{t : Σ (B x₀) (C x₀)}
               -- → (coh (λ x → Σ (B x) (C x)) p t) →β' {!!}
-- postulate ≡Π : {ℓ ℓ' : Level}{A : Set ℓ}{B : A → Set ℓ'}(f g : Π {ℓ}{ℓ'} A B) →  (f ≡ g) →β ((x₀ x₁ : A) → (p : x₀ ≡ x₁) → _≡[_]≡_ {B = B} (f x₀) p (g x₁))
