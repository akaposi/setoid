\documentclass[a4paper]{article}

\usepackage{cite}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{hyperref}
\usepackage{proof}
\usepackage{mathtools}
\usepackage[utf8x]{inputenc}
\usepackage[greek,english]{babel}
\usepackage{scalerel}
\usepackage{tikz}
\usetikzlibrary{arrows}

\input{abbrevs.tex}

\begin{document}

\title{The reflexive graph syntactic translation}
\date{\today}
\maketitle

%--------------------------------------------------------------------

\begin{abstract}
Bernardy's parametricity translation \cite{bernardy12parametricity} is
a syntactic version of the graph model of type theory. Here we show
how the reflexive graph model of Atkey, Ghani and Johann \cite{atkey}
can be turned into a syntactic translation. An improvement of the
model is that we don't need proof irrelevance. The domain and codomain
of the translation are the same: a type theory with a universe closed
under $\Pi$ and $\Bool$.
\end{abstract}

%--------------------------------------------------------------------

\section{Syntax}

We use named variables, we consider $\alpha$-equivalent terms equal
and weakening is implicit.

There are 4 sorts: contexts, types and terms and (parallel)
substitutions.
\[
\vdash \Gamma \hspace{7em} \Gamma \vdash A \hspace{7em} \Gamma \vdash t : A \hspace{7em} \sigma : \Gamma \Ra \Delta
\]

Notational convention: contexts: $\Gamma,\Delta$, types: $A,B,C$,
terms: $t,u,v,w,a,b,c$, variables: $x, y, z, f$, substitutions:
$\sigma, \delta, \nu$.

Syntax for the substitution calculus:
\[
\begin{gathered}
 \infer{\vdash\cdot}{\vspace{1em}}
\end{gathered}
\hspace{2em}
\begin{gathered}
 \infer{\vdash\Gamma,x:A}{\vdash\Gamma && \Gamma \vdash A}
\end{gathered}
\hspace{2em}
\begin{gathered}
  \infer{\Gamma\vdash x:A}{(x:A)\in\Gamma}
\end{gathered}
\hspace{2em}
\begin{gathered}
 \infer{\Gamma\vdash A[\sigma]}{\Delta\vdash A && \sigma:\Gamma\Ra\Delta}
\end{gathered}
\]
\[
\begin{gathered}
 \infer{\Gamma\vdash t[\sigma]:A[\sigma]}{\Delta\vdash t:A && \sigma:\Gamma\Ra\Delta}
\end{gathered}
\hspace{2em}
\begin{gathered}
\infer{\epsilon:\Gamma\Ra\cdot}{\vdash\Gamma}
\end{gathered}
\hspace{2em}
\begin{gathered}
\infer{(\sigma,x\mapsto t):\Gamma\Ra(\Delta,x:A)}{\sigma:\Gamma\Ra\Delta && \Gamma\vdash t:A[\sigma]}
\end{gathered}
\]
$\Pi$ types:
\[
\begin{gathered}
  \infer{\Gamma\vdash (x:A)\ra B}{\Gamma\vdash A && \Gamma,x:A\vdash B}
\end{gathered}
\hspace{2em}
\begin{gathered}
 \infer{\Gamma\vdash\lambda x.t : (x:A)\ra B}{\Gamma,x:A\vdash t : B}
\end{gathered}
\]
\[
\begin{gathered}
 \infer{\Gamma\vdash t\,u : B[x \mapsto u]}{\Gamma\vdash t : (x:A)\ra B && \Gamma \vdash u : A}
\end{gathered}
\hspace{2em}
\begin{gathered}
  (\lambda x.t)\,u = t[x \mapsto u]
\end{gathered}
\hspace{2em}
\begin{gathered}
  \lambda x.t\,x = t
\end{gathered}
\]
$\Sigma$ types:
\[
\begin{gathered}
  \infer{\Gamma\vdash (x:A)\times B}{\Gamma\vdash A && \Gamma,x:A\vdash B}
\end{gathered}
\hspace{2em}
\begin{gathered}
 \infer{\Gamma\vdash(u, v) : (x:A)\times B}{\Gamma\vdash u : A && \Gamma\vdash v : B[x \mapsto u]}
\end{gathered}
\]
\[
\begin{gathered}
 \infer{\Gamma\vdash \proj_0\,t : A}{\Gamma\vdash t : (x:A)\times B}
\end{gathered}
\hspace{2em}
\begin{gathered}
 \infer{\Gamma\vdash \proj_1\,t : B[x \mapsto \proj_0\,t]}{\Gamma\vdash t : (x:A)\times B}
\end{gathered}
\]
\[
\begin{gathered}
  \proj_0\,(u, v) \equiv u
\end{gathered}
\hspace{2em}
\begin{gathered}
  \proj_1\,(u, v) \equiv v
\end{gathered}
\hspace{2em}
\begin{gathered}
  (\proj_0\,t, \proj_1\,t) \equiv t
\end{gathered}
\]
The restriction type.
\[
\infer{\Gamma\vdash A|_{x.u\equiv v}}{\Gamma\vdash A && \Gamma,x:A\vdash B && \Gamma,x:A\vdash u,v : B}
\]
\[
\infer{\Gamma\vdash\IN\,t:A|_{x.u\equiv v}}{\Gamma\vdash t:A && u[x\mapsto t]\equiv v[x\mapsto t]}
\hspace{2em}
\infer{\Gamma\vdash\OUT\,t:A}{\Gamma\vdash t:A|_{x.u\equiv v}}
\]
\[
\begin{gathered}
\infer{u[x\mapsto t]\equiv v[x\mapsto t]}{\Gamma\vdash t:A|_{x.u\equiv v}}
\end{gathered}
\hspace{2em}
\begin{gathered}
\IN\,(\OUT\,t) \equiv t
\end{gathered}
\hspace{2em}
\begin{gathered}
\OUT\,(\IN\,t) \equiv t
\end{gathered}
\]

%--------------------------------------------------------------------

\section{Specification of the translation}

$b$ can be $0$ or $1$.

\[
\begin{gathered}
  \infer{\begin{array}{l}
      \vdash\cul\Gamma\cur \\
      \vdash\Gamma^= \\
      b_\Gamma:\Gamma^=\Ra\cul\Gamma\cur \\
      \R_\Gamma:\cul\Gamma\cur\Ra\Gamma^= \\
      b_\Gamma\circ\R_\Gamma\equiv\id_{\cul\Gamma\cur}
  \end{array}}
        {\vdash\Gamma}
\end{gathered}
\hspace{12em}
\begin{gathered}
  \infer{\begin{array}{l}
      \cul\sigma\cur:\cul\Gamma\cur\Ra\cul\Delta\cur \\
      \sigma^=:\Gamma^=\Ra\Delta^= \\
      b_\Delta\circ\sigma^= \equiv \sigma\circ b_\Gamma  \\
      \R_\Delta\circ\sigma \equiv \sigma^=\circ\R_\Gamma \\
      \\
  \end{array}}
        {\sigma:\Gamma\Ra\Delta}
\end{gathered}
\]

\[
\begin{gathered}
  \infer{
    \begin{array}{l}
      \cul\Gamma\cur\vdash\cul A\cur \\
      \Gamma^=,x_0:\cul A\cur[0_\Gamma],x_1:\cul A\cur[1_\Gamma]\vdash A^=\,x_0\,x_1 \\
      \cul\Gamma\cur,x:\cul A\cur \vdash \R_A\,x:A^=[\R_\Gamma]\,x\,x
  \end{array}}
        {\Gamma\vdash A}
\end{gathered}
\hspace{2em}
\begin{gathered}
  \infer{
    \begin{array}{l}
      \cul\Gamma\cur\vdash\cul t\cur:\cul A\cur \\
      \Gamma^=\vdash t^= : A^=\,(\cul t\cur[0_\Gamma])\,(\cul t\cur[1_\Gamma]) \\
      \R_A\,\cul t\cur \equiv t^=[\R_\Gamma]
  \end{array}}
        {\Gamma\vdash t:A}
\end{gathered}
\]

%--------------------------------------------------------------------

\section{Implementation}

\begin{alignat*}{5}
  & \cul\cdot\cur && :\equiv \cdot \\
  & \cul\Gamma,x:A\cur && :\equiv \cul\Gamma\cur,x:\cul A\cur \\
  & \cul x\cur && :\equiv x \\
  & \cul(x:A)\times B\cur && :\equiv (x:\cul A\cur)\times\cul B\cur \\
  & \cul(t,u)\cur && :\equiv (\cul t\cur,\cul u\cur) \\
  & \cul\proj_1\,w\cur && :\equiv \proj_1\,\cul w\cur \\
  & \cul\proj_2\,w\cur && :\equiv \proj_2\,\cul w\cur
\end{alignat*}


\begin{alignat*}{5}
  & \cul(x:A)\ra B\cur :\equiv \\
  & \hspace{2em} (f:(x:\cul A\cur)\ra\cul B\cur)\times \\
  & \hspace{2em} ((x_0\,x_1:\cul A\cur)(x_=:A^=[\R_\Gamma]\,x_0\,x_1)\ra B^=[\R_\Gamma]\,(f\,x_0)\,(f\,x_1))|_{w.\lambda x.\R_B\,(\proj_1\,w\,x)\equiv \lambda x.\proj_2\,w\,x\,x\,(\R_A\,x)}
\end{alignat*}

%--------------------------------------------------------------------

\section{A universe}

\subsection{Syntax}

A universe
\[
\begin{gathered}
  \infer{\Gamma\vdash\U}{\vdash\Gamma}
\end{gathered}
\hspace{2em}
\begin{gathered}
 \infer{\Gamma\vdash\underline{a}}{\Gamma\vdash a:\U}
\end{gathered}
\]
which is closed under dependent function space
\[
\begin{gathered}
  \infer{\Gamma\vdash (x:a)\ra b : \U}{\Gamma\vdash a : \U && \Gamma,x:\underline{a}\vdash b:\U}
\end{gathered}
\hspace{2em}
\begin{gathered}
 \infer{\Gamma\vdash\lambda x.t : \underline{(x:a)\ra b}}{\Gamma,x:\underline{a}\vdash t : \underline{b}}
\end{gathered}
\]
\[
\begin{gathered}
 \infer{\Gamma\vdash t\,u : \underline{b}[x \mapsto u]}{\Gamma\vdash t : \underline{(x:a)\ra b} && \Gamma \vdash u : \underline{a}}
\end{gathered}
\hspace{2em}
\begin{gathered}
  (\lambda x.t)\,u \equiv t[x \mapsto u]
\end{gathered}
\hspace{2em}
\begin{gathered}
  \lambda x.t\,x \equiv t
\end{gathered}
\]
and booleans.
\[
\begin{gathered}
  \infer{\Gamma\vdash\bool:\U}{\vdash\Gamma}
\end{gathered}
\hspace{2em}
\begin{gathered}
  \infer{\Gamma\vdash\true:\underline{\bool}}{\vdash\Gamma}
\end{gathered}
\hspace{2em}
\begin{gathered}
  \infer{\Gamma\vdash\false:\underline{\bool}}{\vdash\Gamma}
\end{gathered}
\]
\[
  \infer{\Gamma\vdash\IF\,t\,\THEN\,u\,\ELSE\,v : C[x\mapsto t]}{\Gamma,x:\underline{\bool}\vdash C && \Gamma\vdash t : \underline{\bool} && \Gamma\vdash u : C[x \mapsto \true] && \Gamma\vdash v : C[x\mapsto \false]}
\]
\[
\begin{gathered}
  \IF\,\true\,\THEN\,u\,\ELSE\,v \equiv u
\end{gathered}
\hspace{2em}
\begin{gathered}
  \IF\,\false\,\THEN\,u\,\ELSE\,v \equiv v
\end{gathered}
\]
Restriction.
\[
\infer{\Gamma\vdash a|_{x.u\equiv v} : \U}{\Gamma\vdash a:\U && \Gamma,x:\underline{a}\vdash B && \Gamma,x:\underline{a}\vdash u,v : B}
\]
\[
\infer{\Gamma\vdash\IN\,t:\underline{a|_{x.u\equiv v}}}{\Gamma\vdash t:\underline{a} && u[x\mapsto t]\equiv v[x\mapsto t]}
\hspace{2em}
\infer{\Gamma\vdash\OUT\,t:\underline{a}}{\Gamma\vdash t:\underline{a|_{x.u\equiv v}}}
\]
\[
\begin{gathered}
\infer{u[x\mapsto t]\equiv v[x\mapsto t]}{\Gamma\vdash t:\underline{a|_{x.u\equiv v}}}
\end{gathered}
\hspace{2em}
\begin{gathered}
\IN\,(\OUT\,t) \equiv t
\end{gathered}
\hspace{2em}
\begin{gathered}
\OUT\,(\IN\,t) \equiv t
\end{gathered}
\]


\subsection{Implementation of the translation}

\[
\cul\U\cur :\equiv (a:\U)\times(a_=:a\ra a\ra \U)\times((x:a)\ra a_=\,x\,x)
\]

\[
\cul\underline{a}\cur :\equiv ?
\]

%--------------------------------------------------------------------

\bibliographystyle{plain}
\bibliography{b}

%--------------------------------------------------------------------

\end{document}
